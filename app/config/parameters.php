<?php

$container->setParameter('database_host', '127.0.0.1');
$container->setParameter('database_port', '3306');
$container->setParameter('database_name', 'engagement');
$container->setParameter('database_user', 'root');
$container->setParameter('database_password', null);
$container->setParameter('mailer_transport', 'smtp');
$container->setParameter('mailer_host', '127.0.0.1');
$container->setParameter('mailer_user', null);
$container->setParameter('mailer_password', null);
$container->setParameter('secret', 'a622f74e28bc3dda7260f9e9dbe3e8a781edc21');
$container->setParameter('elastic_host', getenv('ELASTIC_HOST'));
$container->setParameter('elastic_port', getenv('ELASTIC_PORT'));
$container->setParameter('elasticsearch.suffix', getenv('SYMFONY__ELASTICSEARCH__SUFFIX'));
