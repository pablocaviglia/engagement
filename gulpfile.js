var gulp = require("gulp"),
  del = require("del"),
  rename = require("gulp-rename"),
  less = require("gulp-less"),
  gutil = require("gulp-util"),
  minifyCss = require("gulp-minify-css"),
  coffee = require('gulp-coffee'),
  md5 = require('md5'),
  uglify = require('gulp-uglify'),
  fs = require('fs'),
  path = require('path'),
  replace = require('gulp-replace'),
  coffeelint = require('gulp-coffeelint'),
  htmlmin = require('gulp-htmlmin'),
  connect,
  modRewrite;

var BUILD_NUM,
  BUILD_NUM_MD5,
  PKG_VERSION;

gulp.task('start', function() {
  BUILD_NUM = (process.env.CI && process.env.CIRCLE_BUILD_NUM) ? process.env.CIRCLE_BUILD_NUM : Date.now();
  BUILD_NUM_MD5 = md5(BUILD_NUM);

  BUILD_NUM = BUILD_NUM.toString(); // Shorten the version for now because it falls back to date.
  BUILD_NUM = BUILD_NUM.substr(BUILD_NUM.length - 5);

  PKG_VERSION = require('./package.json').version;

  gutil.log(gutil.colors.green('Build started'));
  gutil.log('BUILD_NUM:', gutil.colors.yellow(BUILD_NUM));
  gutil.log('BUILD_NUM_MD5:', gutil.colors.yellow(BUILD_NUM_MD5));
  gutil.log('PKG_VERSION:', gutil.colors.yellow(PKG_VERSION));
});

gulp.task('clean', ['start'], function () {
  var delfolders = ["web/js", "web/css", 'src/EngagementBundle/Resources/views/Default/index.html.twig', "web/assets", "web/app/*.*", "web/app/.*"],
    appfolders = []; // app folders
  try {
    appfolders = fs.readdirSync('web/app/');
    appfolders.sort(function(a, b) { // Sort by date modified
      return fs.statSync('web/app/' + b).mtime.getTime() - fs.statSync('web/app/' + a).mtime.getTime();
    })
    appfolders = appfolders.filter(function(file) { // Filter by directory only (just in case)
      return fs.statSync(path.join('web/app', file)).isDirectory();
    });
  } catch (e) {
    appfolders = [];
  }
  appfolders.forEach(function (folder, i, afs) { // Find out what app folders to delete
    afs[i] = 'web/app/' + folder;
    if (i < 4) {
      gutil.log('Saving:', gutil.colors.yellow(folder));
    } else {
      gutil.log('Deleting:', gutil.colors.red(folder));
    }
  })
  appfolders = appfolders.slice(4); // Keep only the latest 4 apps plus the new one that will be built (5 total)
  delfolders = delfolders.concat(appfolders);
  return del(delfolders);
});

gulp.task('html', ['clean'], function () {
  return gulp.src(['src/EngagementBundle/Resources/views/Default/index.html'])
    .pipe(replace('BUILD_NUM_MD5', BUILD_NUM_MD5))
    .pipe(replace('BUILD_NUM', BUILD_NUM))
    .pipe(htmlmin({collapseWhitespace: true}))
    .pipe(rename('front-end.html'))
    .pipe(gulp.dest('web/'))
    .pipe(rename('index.html.twig'))
    .pipe(gulp.dest('src/EngagementBundle/Resources/views/Default/'))
});

gulp.task('lint', function () {
  return gulp.src(["src/EngagementBundle/Resources/public/coffee/**/*.coffee"])
    .pipe(coffeelint('coffeelint.json'))
    .pipe(coffeelint.reporter())
});

gulp.task("less", ["clean"], function () {
  return gulp.src("src/EngagementBundle/Resources/public/less/bootstrap.less")
    .pipe(less())
    .pipe(minifyCss())
    .pipe(rename("engagement.css"))
    .pipe(gulp.dest("web/app/" + BUILD_NUM_MD5 + "/css/"))
});

gulp.task("assets", ["clean"], function () {
  return gulp.src("src/EngagementBundle/Resources/public/assets/**")
    .pipe(gulp.dest("web/assets"))
});

gulp.task("coffee", ["clean"], function() {
  return gulp.src(["src/EngagementBundle/Resources/public/coffee/**/*.coffee"])
    .pipe(replace('cs!coffee/', '')) // Removes the CoffeeScript requirejs plugin that is used with Jasmine
    .pipe(coffee({bare: true}).on('error', gutil.log))
    .pipe(uglify({ mangle: false }))
    .pipe(gulp.dest("web/app/" + BUILD_NUM_MD5 + "/js"));
});

gulp.task("templates", ["clean"], function() {
  return gulp.src(["src/EngagementBundle/Resources/public/templates/**/*.html"])
    .pipe(replace('PKG_VERSION', PKG_VERSION))
    .pipe(replace('BUILD_NUM', BUILD_NUM))
    .pipe(gulp.dest("web/app/" + BUILD_NUM_MD5 + "/js/templates"));
});

gulp.task("connect", ["build"], function() {
  connect = connect || require("gulp-connect");
  modRewrite = require("connect-modrewrite");
  connect.server({
    port: 5000,
    root: "web",
    livereload: true,
    middleware: function () {
      var middlewares = [];
      middlewares.push((function () {
        var url = require("url");
        var proxy = require("proxy-middleware");
        // var options = url.parse("https://fast-wave-2047.herokuapp.com/api/v1/");
        var options = url.parse("http://gretzky.myuniphy.com/api/v1/");
        options.route = "/api/v1";
        return proxy(options);
      })());
      middlewares.push((function () {
        var url = require("url");
        var proxy = require("proxy-middleware");
        // var options = url.parse("https://fast-wave-2047.herokuapp.com/api/v1/");
        var options = url.parse("http://localhost:5002/ogh/");
        options.route = "/ogh";
        return proxy(options);
      })());
      middlewares.push(modRewrite([
        '^/([0-9]+)/groups        /front-end.html',
        '^/([0-9]+)/following     /front-end.html',
        '^/([0-9]+)/followers     /front-end.html',
        '^/([0-9]+)/posts         /front-end.html',
        '^/groups/([0-9]+)        /front-end.html',
        '^/groups                 /front-end.html',
        '^/search(.*)$            /front-end.html',
        '^/headlines              /front-end.html',
        '^/$                      /front-end.html'
      ]));
      return middlewares;
    }
  });
});

gulp.task("reload", ["build"], function() {
  connect = connect || require("gulp-connect");
  gulp.src("web/**/*.*").pipe(connect.reload());
  gutil.log(gutil.colors.green("Reloading"));
});

gulp.task("watch", ["connect"], function() {
  var watch = require("gulp-watch");
  gulp.watch(["src/EngagementBundle/Resources/public/**/*.*"], { debounceDelay: 3000 }, ["reload"]); // Rebuild on JS or LESS changes
});

gulp.task('version', ["clean"], function () {
  var src = require('stream').Readable({ objectMode: true })
  src._read = function () {
    this.push(new gutil.File({ cwd: "", base: "", path: "build.txt", contents: new Buffer("" + BUILD_NUM) }));
    this.push(null);
  }
  return src
    .pipe(gulp.dest('web/'))
});

// gulp or gulp server for local environment
gulp.task("build", ["version", "html", "templates", "lint", "coffee", "less", "assets"]);
gulp.task("server", ["watch"]);
gulp.task("default", ["server"]);