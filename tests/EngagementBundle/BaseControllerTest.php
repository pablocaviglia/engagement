<?php

namespace EngagementBundle\Tests;

use Liip\FunctionalTestBundle\Test\WebTestCase;

abstract class BaseControllerTest extends WebTestCase
{
    public function setUp()
    {
        $classes = array(
            'EngagementBundle\DataFixtures\ORM\LoadUserData',
            'EngagementBundle\DataFixtures\ORM\LoadPostData',
            'EngagementBundle\DataFixtures\ORM\LoadGroupData',
        );

        $this->fixtures = $this->loadFixtures($classes)->getReferenceRepository();
    }
}