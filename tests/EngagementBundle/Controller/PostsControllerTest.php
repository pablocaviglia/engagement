<?php

namespace Tests\AppBundle\Controller;

use EngagementBundle\Constant\ResultState;
use EngagementBundle\Tests\BaseControllerTest;

class PostsControllerTest extends BaseControllerTest
{

    public function testGetPost()
    {
        $client = static::createClient();
        $client->request('GET', "/api/v1/post/1");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertTrue(!is_null($json->id));
    }

    public function testEditPost()
    {

        $postId = "1";
        $postNewTitle = "modified title";
        $postNewContent = "modified content";
        $openGraphNewTitle = "modified opengraph title";

        $jsonStr = "{
                   \"id\":\"$postId\",
                   \"title\":\"$postNewTitle\",
                   \"content\":\"$postNewContent\",
                   \"open_graph_data\":{
                      \"ogTitle\":\"$openGraphNewTitle\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                      }
                   }
                }";

        //save post
        $client = static::createClient();
        $client->request('PUT', "/api/v1/post/$postId", array(), array(), array('CONTENT_TYPE' => 'application/json'), $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));

        //get post
        $client->request('GET', "/api/v1/post/$postId");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertEquals($postNewTitle, $json->title);
        $this->assertEquals($postNewContent, $json->content);
        $this->assertEquals($openGraphNewTitle, $json->open_graph_data->ogTitle);

    }

    public function testDeletePost()
    {

        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"1\"
                   },
                   \"open_graph_data\":{
                      \"ogTitle\":\"Ask Well: Should You Filter Your Water?\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                      }
                   }
                }";

        //create post
        $client = static::createClient();
        $user = $this->fixtures->getReference('second-user')->getId();
        $client->request('POST', "/api/v1/user/$user/post", array(), array(), array('CONTENT_TYPE' => 'application/json'), $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //get created post id
        $createdPostId = $json->id;

        //count posts
        $client->request('GET', "/api/v1/user/$user/posts/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(3, $json->count);

        //delete post
        $client->request('DELETE', '/api/v1/post/'.$createdPostId);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(ResultState::OK, $json->state);

        //count posts
        $client->request('GET', "/api/v1/user/$user/posts/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(2, $json->count);

    }

    public function testLogImpression() {
        $json_string = "{
                        \"userId\":\"2\"
                        }";
        $client = static::createClient();
        $client->request('POST', "/api/v1/post/1/impression", array(), array(), array('CONTENT_TYPE' => 'application/json'), $json_string);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }

    public function testLogImpressionFailures() {
        $json_string = "{
                        \"userId\":\"2\"
                        }";
        $client = static::createClient();
        $client->request('POST', "/api/v1/post/9999/impression", array(), array(), array('CONTENT_TYPE' => 'application/json'), $json_string);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());

        $json_string = "{
                        \"userId\":\"99999\"
                        }";
        $client = static::createClient();
        $client->request('POST', "/api/v1/post/1/impression", array(), array(), array('CONTENT_TYPE' => 'application/json'), $json_string);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
    }
}