<?php

namespace EngagementBundle\Tests\Controller;

use EngagementBundle\Tests\BaseControllerTest;

class DefaultControllerTest extends BaseControllerTest
{
    public function testIndex()
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/bob');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('Welcome to Symfony', $crawler->filter('#container h1')->text());
    }
}
