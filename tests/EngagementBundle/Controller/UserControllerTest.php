<?php

namespace Tests\AppBundle\Controller;

use DateTime;
use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\PostDataMapper;
use EngagementBundle\Exception\ErrorCode;
use EngagementBundle\Tests\BaseControllerTest;
use EngagementBundle\VO\V1\PostVO;
use EngagementBundle\Exception\InvalidUserException;

class UserControllerTest extends BaseControllerTest
{
    public function testGetUsers()
    {
        $client = static::createClient();

        //get users list
        $client->request('GET', '/api/v1/users');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        //validate headers
        $this->assertEquals(3, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(3, count($json));

        //get users count
        $client->request('GET', '/api/v1/users/count');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $jsonCount = json_decode($client->getResponse()->getContent());
        $this->assertEquals(3, $jsonCount->count);

        //request full user
        $userId = $json[0]->id;
        $client->request('GET', "/api/v1/user/$userId");
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));

        //transform json to VO
        $fullUserJson = json_decode($client->getResponse()->getContent());

        $this->assertEquals("Roger Clyne", $fullUserJson->full_name);
        $this->assertEquals("Roger", $fullUserJson->first);

    }

    public function testSavePost()
    {
        $user = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();

        $json = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"1\"
                   },
                   \"open_graph_data\":{
                      \"ogTitle\":\"Ask Well: Should You Filter Your Water?\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                      }
                   }
                }";

        $client->request('POST', "/api/v1/user/$user/post",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $json);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertEquals("Joe", $json->user->first);
        $this->assertEquals("Ask Well: Should You Filter Your Water?", $json->open_graph_data->ogTitle);
    }

    public function testSaveContentGroupPost()
    {
        $user = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();

        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"1\"
                   },
                   \"content_group\":{
                      \"id\":\"1\"
                   },
                   \"open_graph_data\":{
                      \"ogTitle\":\"Ask Well: Should You Filter Your Water?\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                      }
                   }
                }";

        $client->request('POST', "/api/v1/user/$user/post",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertEquals(1, $json->content_group->id);
    }

    public function testUserShow()
    {
        $user = $this->fixtures->getReference('post-user')->getId();
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$user");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals("Roger Clyne", $json->full_name);
        $this->assertEquals(1, $json->posts_qnty);
    }

    public function testUserFeed()
    {
        $user = $this->fixtures->getReference('post-user')->getId();

        $client = static::createClient();

        //get post-user feed
        $client->request('GET', "/api/v1/user/$user/feed");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(3, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(3, count($json));
        $this->assertEquals("Joe Hendrickson", $json[0]->user->full_name);

        //get post-user feed count
        $client->request('GET', "/api/v1/user/$user/feed/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(3, $json->count);

    }

    public function testUserFeedCreatedAtFilter()
    {
        $user = $this->fixtures->getReference('post-user')->getId();

        $client = static::createClient();

        //get post-user feed
        $client->request('GET', "/api/v1/user/$user/feed");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(3, count($json));
        $this->assertEquals("Joe Hendrickson", $json[0]->user->full_name);

        //get post-user feed count
        $client->request('GET', "/api/v1/user/$user/feed/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(3, $json->count);

        //tomorrow date
        $tomorrowDatetime = new DateTime('tomorrow');
        $tomorrowTimestamp = $tomorrowDatetime->getTimestamp() * 1000;

        //get post-user feed count filtering with tomorrow date
        $client->request('GET', "/api/v1/user/$user/feed/count?created_at=$tomorrowTimestamp");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(0, $json->count);

    }

    public function testFeedUserFollowing() {

        $user = $this->fixtures->getReference('post-user')->getId();
        $user2 = $this->fixtures->getReference('second-user')->getId();

        $client = static::createClient();

        //get post-user feed count
        $beforeUnfollowingUserFeedCount = $this->getUserFeedCount($user);
        $this->assertEquals(3, $beforeUnfollowingUserFeedCount);

        //count second-user posts
        $client->request('GET', "/api/v1/user/$user2/posts/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $followingUserPostCount = $json->count;
        $this->assertEquals(2, $followingUserPostCount);

        //make post-user stop following second-user
        $client->request('DELETE', "/api/v1/user/$user/following/$user2");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //the count must be $beforeUnfollowingUserFeedCount posts from 'post-user' feed ___MINUS___ $followingUserPostCount (because of the user unfollowing)
        $this->assertEquals($beforeUnfollowingUserFeedCount - $followingUserPostCount, $this->getUserFeedCount($user));

        //make post-user start following second-user again
        $client->request('POST', "/api/v1/user/$user/following/$user2");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //the user feed count must be the same than when test started
        $this->assertEquals($this->getUserFeedCount($user), $beforeUnfollowingUserFeedCount);

    }

    public function testFeedGroupFollowing() {

        $contentGroupId = 1;
        $user = $this->fixtures->getReference('post-user')->getId();
        $user2 = $this->fixtures->getReference('second-user')->getId();

        $client = static::createClient();

        //get post-user feed count
        $beforeFollowingGroupFeedCount = $this->getUserFeedCount($user);
        $this->assertEquals(3, $beforeFollowingGroupFeedCount);

        //makes 'second-user' write a group post
        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"$user2\"
                   },
                   \"content_group\":{
                      \"id\":\"$contentGroupId\"
                   }
                }";

        $client->request('POST', "/api/v1/user/$user2/post",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //makes 'post-user' start following group
        $client->request('POST', "/api/v1/user/$user/content_group/follow/$contentGroupId",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //get post-user feed count
        $afterFollowingGroupFeedCount = $this->getUserFeedCount($user);
        $this->assertEquals($beforeFollowingGroupFeedCount + 1, $afterFollowingGroupFeedCount);

    }

    public function testFeedUserAndGroupFollowing() {

        $contentGroupId = 2;
        $user = $this->fixtures->getReference('post-user')->getId();
        $user2 = $this->fixtures->getReference('second-user')->getId();

        $client = static::createClient();

        //get the feed count before 'second-user' post
        $userFeedCountBeforePosting = $this->getUserFeedCount($user);

        //makes 'second-user' write a post on a group
        //that 'post-user' is not following
        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"$user2\"
                   },
                   \"content_group\":{
                      \"id\":\"$contentGroupId\"
                   }
                }";

        $client->request('POST', "/api/v1/user/$user2/post",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //get the feed count after 'second-user' post
        $userFeedCountAfterPosting = $this->getUserFeedCount($user);

        //feed count must be the same before and after
        //a post is saved by second-user
        $this->assertEquals($userFeedCountBeforePosting, $userFeedCountAfterPosting);

        //makes 'second-user' write a post without a group
        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"$user2\"
                   }
                }";
        $client->request('POST', "/api/v1/user/$user2/post",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //get the feed count after 'second-user' post
        $userFeedCountAfterSecondPosting = $this->getUserFeedCount($user);

        //feed count must be the same before and after
        //a post is saved by second-user
        $this->assertEquals($userFeedCountBeforePosting, $userFeedCountAfterSecondPosting  - 1);

    }

    public function testFeedOrder()
    {
        $user = $this->fixtures->getReference('post-user')->getId();
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$user/feed");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertEquals(3, count($json));
        $this->assertTrue($json[0]->id > $json[1]->id);
    }

    public function testWhoToFollow()
    {
        $user = $this->fixtures->getReference('post-user')->getId();

        //test count
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$user/who_to_follow/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $json->count);

        //test retrieval
        $client->request('GET', "/api/v1/user/$user/who_to_follow");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(1, count($json));
        $this->assertEquals("Fancy Loving", $json[0]->full_name);

        $id = $json[0]->id;
        $client->request('POST', "/api/v1/user/$user/following/$id");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', "/api/v1/user/$user/who_to_follow");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(0, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(0, count($json));

        $client->request('DELETE', "/api/v1/user/$user/following/$id");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', "/api/v1/user/$user/who_to_follow");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(1, count($json));
        $this->assertEquals("Fancy Loving", $json[0]->full_name);
    }

    public function testFollowersQnty()
    {
        $user = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$user");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertEquals(1, $json->followers_qnty);
    }

    public function testFollowingsObject()
    {
        $user = $this->fixtures->getReference('post-user')->getId();

        //data retrieval
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$user/following");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(1, count($json));

        //count
        $client->request('GET', "/api/v1/user/$user/following/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $json->count);

    }

    public function testSaveUser()
    {
        $client = static::createClient();

        $json = "{
                   \"first\":\"Pablo\",
                   \"last\":\"Caviglia\"
                 }";

        $client->request('POST', "/api/v1/user",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $json);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $createdUserId = $json->id;

        $client->request('GET', "/api/v1/user/$createdUserId");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertEquals($createdUserId, $json->id);

    }

    function testGetInvalidUser() {

        //data retrieval
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/99999999");
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $this->assertTrue($json->exception->exception_class == InvalidUserException::class);
        $this->assertTrue($json->exception->error_code == ErrorCode::USER_INVALID_ID);

    }

    public function testFollowersEndpoint()
    {
        $userFollowing = $this->fixtures->getReference('post-user')->getId();
        $userToFollow = $this->fixtures->getReference('second-user')->getId();

        //unfollow user
        $client = static::createClient();
        $client->request('DELETE', "/api/v1/user/$userFollowing/following/$userToFollow");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $client->request('GET', "/api/v1/user/$userToFollow/followers");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(0, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(0, sizeof($json));

        //count
        $client->request('GET', "/api/v1/user/$userToFollow/followers/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $followersQnty = $json->count;

        //follow user
        $client->request('POST', "/api/v1/user/$userFollowing/following/$userToFollow");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        //count
        $client->request('GET', "/api/v1/user/$userToFollow/followers/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $newFollowersQnty = $json->count;

        $this->assertTrue($newFollowersQnty > $followersQnty);

        $client->request('GET', "/api/v1/user/$userToFollow/followers");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(1, sizeof($json));

    }

    function testTopPosters() {

        $userId = 1;

        //count posts
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$userId/posts/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $userPostsQnty = $json->count;

        //get top posters
        $client->request('GET', "/api/v1/post/top_posters/10");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        $valid = false;
        foreach($json as $topPoster) {
            if($topPoster->user->id == $userId) {
                if($userPostsQnty == $topPoster->qnty_posts) {
                    $valid = true;
                    break;
                }
            }
        }

        $this->assertTrue($valid, "Couldn't find top post user");
    }


    function getUserFeedCount($userId) {
        $client = static::createClient();
        $client->request('GET', "/api/v1/user/$userId/feed/count");
        $json = json_decode($client->getResponse()->getContent());
        $userFeedPostCount = $json->count;
        return  $userFeedPostCount;
    }
}