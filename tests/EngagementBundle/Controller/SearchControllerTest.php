<?php

namespace Tests\AppBundle\Controller;

use DateInterval;
use EngagementBundle\Tests\BaseControllerTest;

class SearchControllerTest extends BaseControllerTest
{

    //TODO FIX THIS ONE BY CHANGING ELASTICSEARCH BOOL QUERY
//    public function testBasicPostSearch() {
//        $client = static::createClient();
//
//        $client->request('GET', '/api/v1/search/?search=seed');
//        $this->assertEquals(200, $client->getResponse()->getStatusCode());
//        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
//        $jsonCount = json_decode($client->getResponse()->getContent());
//        $this->assertEquals(3, count($jsonCount));
//    }

    public function testCreatedAtPostSearch() {

        $client = static::createClient();

        //count without date
        $client->request('GET', '/api/v1/search/?search=seed');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $jsonCount = json_decode($client->getResponse()->getContent());
        $this->assertTrue(count($jsonCount) != 0);

        //current date plus 10 days
        $dateTime = new \DateTime();
        $dateTime->add(new DateInterval('P10D'));
        $dateTimeMs = $dateTime->getTimestamp() * 1000;

        //count with future date
        $client->request('GET', "/api/v1/search/?search=seed&created_at=$dateTimeMs");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $jsonCount = json_decode($client->getResponse()->getContent());
        $this->assertTrue(count($jsonCount) == 0);

    }

    public function testBasicSearchOneResult() {
        $client = static::createClient();

        $client->request('GET', '/api/v1/search/?search=body');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $jsonCount = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, count($jsonCount));
    }
}