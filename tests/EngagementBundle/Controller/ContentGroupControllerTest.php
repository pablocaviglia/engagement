<?php

namespace Tests\AppBundle\Controller;

use EngagementBundle\Constant\ResultState;
use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\Exception\ErrorCode;
use EngagementBundle\Tests\BaseControllerTest;
use EngagementBundle\VO\V1\PostVO;

class ContentGroupControllerTest extends BaseControllerTest
{
    public function testGetContentGroups()
    {
        $client = static::createClient();

        //get groups data
        $client->request('GET', '/api/v1/content_groups');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //count groups
        $this->assertEquals(2, count($json));
        $this->assertEquals("The first group", $json[0]->name);
        $this->assertEquals("Description of the group", $json[0]->description);

        //get groups count
        $client->request('GET', '/api/v1/content_groups/count');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertTrue($json->count != 0);

    }

    public function testSearchContentGroups()
    {
        $client = static::createClient();

        //get the first group
        $client->request('GET', '/api/v1/content_group/query', array('search'=>'the first'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, sizeof($json));

        //get the second group
        $client->request('GET', '/api/v1/content_group/query', array('search'=>'the second'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, sizeof($json));

        //get both groups
        $client->request('GET', '/api/v1/content_group/query', array('search'=>'the'));
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(2, sizeof($json));

    }

    public function testGetContentGroup()
    {
        $client = static::createClient();

        //get groups data
        $client->request('GET', '/api/v1/content_group/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //count groups
        $this->assertEquals(1, $json->id);
    }

    public function testContentGroupFeed()
    {

        $contentGroupId = 1;
        $user = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();

        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"1\"
                   },
                   \"content_group\":{
                      \"id\":\"$contentGroupId\"
                   },
                   \"open_graph_data\":{
                      \"ogTitle\":\"Ask Well: Should You Filter Your Water?\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                      }
                   }
                }";

        //save post
        $client->request('POST', "/api/v1/user/$user/post",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //get content group feed
        $client->request('GET', "/api/v1/content_group/$contentGroupId/feed");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, count($json));
        $this->assertEquals("Joe Hendrickson", $json[0]->user->full_name);

        //test count
        $client->request('GET', "/api/v1/content_group/$contentGroupId/feed/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $json->count);

    }

    public function testCreateContentGroup()
    {

        $userId = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();

        $jsonStr = "{
                      \"name\":\"content group name\",
                      \"description\":\"content group description\",
                      \"created_by\":{
                        \"id\":\"$userId\"
                       }
                    }";

        //save content group
        $client->request('POST', "/api/v1/content_group",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertTrue(!is_null($json->id));

        //resave same content group to validate
        //not duplicates on name field
        $client->request('POST', "/api/v1/content_group",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(ErrorCode::CONTENT_GROUP_INVALID_NAME, $json->exception->error_code);

    }

    public function testModifyContentGroup()
    {

        $userId = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();

        $contentGroupName = "new content name";
        $modContentGroupName = "MOD new content name ******";

        $jsonStr = "{
                      \"name\":\"$contentGroupName\",
                      \"description\":\"new content group description\",
                      \"created_by\":{
                        \"id\":\"$userId\"
                       }
                    }";

        //save content group
        $client->request('POST', "/api/v1/content_group",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $json = json_decode($client->getResponse()->getContent());
        $contentGroupId = $json->id;

        $jsonStrModified = "{
                      \"name\":\"$modContentGroupName\",
                      \"description\":\"new content group description\"
                    }";

        //modify created content group
        $client->request('PUT', "/api/v1/content_group/$contentGroupId",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStrModified);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $jsonModified = json_decode($client->getResponse()->getContent());

        //validate the value was changed on backend
        $this->assertEquals($jsonModified->name, $modContentGroupName);

    }

    public function testDeleteContentGroup()
    {

        $userId = $this->fixtures->getReference('second-user')->getId();
        $client = static::createClient();

        $contentGroupName = "new content name";

        $jsonStr = "{
                      \"name\":\"$contentGroupName\",
                      \"description\":\"new content group description\",
                      \"created_by\":{
                        \"id\":\"$userId\"
                       }
                    }";

        //save content group
        $client->request('POST', "/api/v1/content_group",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $json = json_decode($client->getResponse()->getContent());
        $contentGroupId = $json->id;

        //modify created content group
        $client->request('DELETE', "/api/v1/content_group/$contentGroupId",
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            array());
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //validate the group was deleted
        $this->assertEquals($json->state, ResultState::OK);

    }
}