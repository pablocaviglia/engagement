<?php

namespace Tests\AppBundle\Controller;

use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\Tests\BaseControllerTest;
use EngagementBundle\VO\V1\PostVO;

class ContentGroupFollowingControllerTest extends BaseControllerTest
{

    public function testFollowUnfollowContentGroup()
    {
        //start following content group
        $client = static::createClient();
        $client->request('POST', '/api/v1/user/1/content_group/follow/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //get content group followings of the user
        $client->request('GET', '/api/v1/user/1/content_group/following');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //test
        $this->assertEquals(1, $client->getResponse()->headers->get("X-Total-Count"));
        $this->assertEquals(1, sizeof($json));

        //unfollow the group
        //start following content group
        $client->request('DELETE', '/api/v1/user/1/content_group/unfollow/1');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //get content group followings of the user
        $client->request('GET', '/api/v1/user/1/content_group/following/count');
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //test
        $this->assertEquals(0, $json->count);

    }

    public function testContentGroupFeed()
    {

        $jsonStr = "{
                   \"title\":\"Water\",
                   \"content\":\"I better bringing in my own water!\",
                   \"user\":{
                      \"id\":\"1\"
                   },
                   \"content_group\":{
                      \"id\":\"1\"
                   },
                   \"open_graph_data\":{
                      \"ogTitle\":\"Ask Well: Should You Filter Your Water?\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                      }
                   }
                }";

        //create post
        $client = static::createClient();
        $user = $this->fixtures->getReference('second-user')->getId();
        $client->request('POST', "/api/v1/user/$user/post", array(), array(), array('CONTENT_TYPE' => 'application/json'), $jsonStr);
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());

        //get content group
        $client = static::createClient();
        $client->request('GET', "/api/v1/content_group/1/feed");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, count($json));
        $this->assertEquals("Joe Hendrickson", $json[0]->user->full_name);

        //test count
        $client->request('GET', "/api/v1/content_group/1/feed/count");
        $this->assertEquals(200, $client->getResponse()->getStatusCode());
        $this->assertContains('application/json', $client->getResponse()->headers->get('content_type'));
        $json = json_decode($client->getResponse()->getContent());
        $this->assertEquals(1, $json->count);

    }
}