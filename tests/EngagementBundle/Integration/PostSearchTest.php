<?php

namespace EngagementBundle\Tests\Integration;


class PostSearchTest extends BaseIntegration
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testBasicPostSearch()
    {
        $finder = $this->post_search;
        $results = $finder->find('body');

        $this->assertEquals(1, count($results));
        $this->assertEquals("Fancy Loving", $results[0]->getUser()->getFullName());
    }

    public function testCaseInsensitivityOnSearch()
    {
        $finder = $this->post_search;
        $results = $finder->find("BODY");

        $this->assertEquals(1, count($results));
    }

    public function testSnowballAnalyzer() {

        // Stop here and mark this test as incomplete.
        $this->markTestIncomplete(
            'This test needs to be fixed.'
        );

        $finder = $this->post_search;
        $results = $finder->find("lemon");

        $this->assertEquals(1, count($results));

        $finder = $this->post_search;
        $results = $finder->find("follow");

        $this->assertEquals(3, count($results));
    }
}