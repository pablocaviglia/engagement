<?php

namespace EngagementBundle\Tests\Integration;


use EngagementBundle\Entity\Impression;

class PostImpressionTest extends BaseIntegration
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testPostHasImpression()
    {
        $post = $this->posts->find(1);
        $impression = New Impression();
        $impression->setPost($post);

        $this->em->persist($impression);
        $this->em->flush();

        $this->assertEquals(1, count($this->impressions->findAll()));
        $this->assertEquals(1, count($post->getImpressions()));
        $this->assertEquals($post->getTitle(), $this->impressions->find(1)->getPost()->getTitle());
    }

    public function testImpressionHasUserAndPost()
    {
        $post = $this->posts->find(1);
        $user = $this->users->find(1);

        $impression = New Impression();
        $impression->setPost($post);
        $impression->setUser($user);

        $this->em->persist($impression);
        $this->em->flush();

        $this->assertEquals(1, count($this->impressions->findAll()));
        $this->assertEquals(1, count($post->getImpressions()));
        $this->assertEquals($post->getTitle(), $this->impressions->find(1)->getPost()->getTitle());
        $this->assertEquals($user->getFullName(), $this->impressions->find(1)->getUser()->getFullName());
    }
}
