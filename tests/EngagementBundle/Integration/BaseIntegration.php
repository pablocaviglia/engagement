<?php

namespace EngagementBundle\Tests\Integration;

use EngagementBundle\DataFixtures\ORM\LoadPostData;
use EngagementBundle\DataFixtures\ORM\LoadUserData;
use EngagementBundle\DataFixtures\ORM\LoadGroupData;
use EngagementBundle\Tests\BaseFixtureLoader;

class BaseIntegration extends BaseFixtureLoader
{
    protected $users;
    protected $posts;
    protected $contentgroups;
    protected $em;
    protected $serializer;
    protected $postDataMapper;
    protected $userDataMapper;
    protected $followingDataMapper;
    protected $followerDataMapper;
    protected $contentGroupDataMapper;
    protected $post_search;
    protected $impressions;

    public function setUp()
    {
        parent::setUp();

        // Base fixture for all tests
        $this->addFixture(new LoadUserData());
        $this->addFixture(new LoadPostData());
        $this->addFixture(new LoadGroupData());
        $this->executeFixtures();

        // Fixtures are now loaded in a clean DB. Yay!

        $kernel = static::createKernel();
        $kernel->boot();
        $this->users = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:User')
        ;

        $this->posts = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:Post')
        ;

        $this->contentgroups = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:ContentGroup')
        ;

        $this->impressions = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:Impression')
        ;

        $this->post_search = $kernel
            ->getContainer()
            ->get('fos_elastica.finder.app.post')
        ;

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');

        $this->serializer = $kernel->getContainer()->get('jms_serializer');

        //data mappers
        $this->postDataMapper = $kernel->getContainer()->get('engagement.post_mapper');
        $this->userDataMapper = $kernel->getContainer()->get('engagement.user_mapper');
        $this->followingDataMapper = $kernel->getContainer()->get('engagement.following_mapper');
        $this->followerDataMapper = $kernel->getContainer()->get('engagement.follower_mapper');
        $this->contentGroupDataMapper = $kernel->getContainer()->get('engagement.content_group_mapper');
    }
}