<?php

namespace EngagementBundle\Tests\Integration;

use EngagementBundle\Entity\Following;

class UserFollowingTest extends BaseIntegration
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
        $user_repo = $this->em->getRepository('EngagementBundle:User');
        $user = $user_repo->findOneBy(array('first'=> "Roger"));
        $user_to_follow = $user_repo->findOneBy(array('first'=> "Joe"));
        $following = new Following();
        $following->setUser($user);
        $following->setFollowingUser($user_to_follow);

        $this->em->persist($following);
        $this->em->flush();
    }

    public function testUserFollows()
    {
        $user = $this->users->find(1);
        $this->assertEquals(2, count($user->getFollowings()));
    }

    public function testArrayOfFollowing()
    {
        $user = $this->users->find(1);
        $following_user_ids = $user->getFollowingUserIds();
        $this->assertEquals(3, count($following_user_ids));
        $this->assertEquals(2, $following_user_ids[0]);
    }

    public function testUserFeed()
    {
        $user = $this->users->find(1);
        //retrieval
        $posts = $this->posts->getUserFeed($user, 20, 0, false);
        $this->assertEquals(3, count($posts));
        foreach ($posts as $post)
        {
            $this->assertArrayHasKey($post->getUser()->getId(), $user->getFollowingUserIds());
        }

        //count
        $count = $this->posts->getUserFeed($user, 20, 0, true);
        $this->assertEquals(3, $count);

    }

    public function testUsersToFollowFeed()
    {
        $user = $this->users->find(1);
        //retrieval
        $users_to_follow = $this->users->getUsersToFollow($user, 20, 0, false);
        $this->assertEquals(1, count($users_to_follow));

        //count
        $count = $this->users->getUsersToFollow($user, 20, 0, true);
        $this->assertEquals(1, $count);

    }

    public function testFollowers()
    {
        $user = $this->users->find(2);
        $user2 = $this->users->find(3);

        $following = new Following();
        $following->setUser($user);
        $following->setFollowingUser($user2);

        $this->em->persist($following);
        $this->em->flush();

        $followers = $user2->getFollowers();
        $this->assertEquals(1, count($followers));
        $this->assertEquals("Joe Hendrickson", $followers[0]->getUser()->getFullName());
        $this->assertEquals(2, $followers[0]->getUser()->getId());
    }

//    public function testFollowings()
//    {
//        $user = $this->users->find(1);
//
//        //retrieval
//        $usersFollowing = $this->users->getUserFollowings($user, 20, 0, false);
//        $this->assertEquals(1, count($usersFollowing));
//
//        //count
//        $count = $this->users->getUserFollowings($user, 20, 0, true);
//        $this->assertEquals(1, $count);
//    }
}