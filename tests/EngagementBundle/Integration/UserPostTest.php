<?php

namespace EngagementBundle\Tests\Integration;


class UserPostTest extends BaseIntegration
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testFixtureLoad()
    {
        $this->assertEquals(3, count($this->users->findAll()));
        $this->assertEquals(4, count($this->posts->findAll()));
    }

    public function testUserData()
    {
        $user = $this->users->find(1);
        $this->assertEquals("Roger", $user->getFirst());
        $this->assertEquals("Clyne", $user->getLast());
    }

    public function testUserHasPost()
    {
        $user = $this->users->find(1);
        $this->assertEquals(1, count($user->getPosts()));
    }

    public function testPostsHaveKeyData()
    {
        $posts = $this->posts->findAll();

        foreach ($posts as $post)
        {
            $this->assertNotNull($post->getCreatedAt());
            $this->assertNotNull($post->getUpdatedAt());
            $this->assertNotNull($post->getUser());
            $this->assertNotNull($post->getTitle());
            $this->assertNotNull($post->getContent());
            $this->assertNotNull($post->getOpenGraphData());
        }
    }
}
