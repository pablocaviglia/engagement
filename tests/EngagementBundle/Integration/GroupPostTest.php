<?php

namespace EngagementBundle\Tests\Integration;


use EngagementBundle\Entity\Post;

class GroupPostTest extends BaseIntegration
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testFixtureLoad()
    {
        $this->assertEquals(2, count($this->contentgroups->findAll()));
    }

    public function testGroupPost()
    {
        $user = $this->users->find(1);
        $contentgroup = $this->contentgroups->find(1);

        $post = new Post();
        $post->setUser($user);
        $post->setTitle("This is a group post");
        $post->setContent("This is the content area of a group post");
        $post->setContentGroup($contentgroup);

        $this->em->persist($post);
        $this->em->flush();

        $fetched_post = $contentgroup->getPosts();

        $this->assertEquals(1, count($fetched_post));
        $this->assertNotNull($fetched_post[0]->getId());
    }
}
