<?php


namespace EngagementBundle\DataMapper;


use EngagementBundle\Tests\Integration\BaseIntegration;

class AbstractDataMapperTest extends BaseIntegration
{

    public function setUp()
    {
        parent::setUp();
    }

    function testGetDataMappers() {
        $this->assertNotNull($this->postDataMapper->getUserDataMapper());
        $this->assertNotNull($this->postDataMapper->getContentGroupDataMapper());
        $this->assertNotNull($this->userDataMapper->getPostDataMapper());
        $this->assertNotNull($this->userDataMapper->getFollowingDataMapper());
        $this->assertNotNull($this->userDataMapper->getFollowerDataMapper());
        $this->assertNotNull($this->userDataMapper->getSerializer());
    }
}