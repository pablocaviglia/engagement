<?php

namespace EngagementBundle\Tests\DataMapper;

use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\FollowingDataMapper;
use EngagementBundle\Entity\Following;
use EngagementBundle\Tests\Integration\BaseIntegration;
use EngagementBundle\VO\V1\FollowingVO;

class FollowingDataMapperTest extends BaseIntegration
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testMappingProcess()
    {
        //get entity
        $entity = $this->users->find(1)->getFollowings()[0];

        //map entity to vo
        $tmpArray = array($entity);
        $voList = $this->followingDataMapper->mapListBaseEntityToVO_V1($tmpArray);
        $voList = $this->followingDataMapper->mapListFullEntityToVO_V1($tmpArray);
        $vo = $voList[0];

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //parse VO to json, and viceversa
        $json = DataMapperUtil::mapModelToJSON($this->serializer, $vo);
        $vo = DataMapperUtil::mapJSONToModel($json, $this->serializer, FollowingVO::class);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //map vo to entity
        $this->followingDataMapper->mapVO_V1ToEntity($vo, $entity);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

    }

    private function assertFieldsEquality(Following &$entity, FollowingVO &$vo) {
        $this->assertEquals($entity->getId(), $vo->getId());
        $this->assertEquals($entity->getCreatedAt()->getTimestamp(), $vo->getCreatedAt()->getTimestamp());
    }
}