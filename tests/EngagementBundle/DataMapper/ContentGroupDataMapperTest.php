<?php

namespace EngagementBundle\Tests\DataMapper;

use EngagementBundle\DataMapper\ContentGroupDataMapper;
use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\FollowingDataMapper;
use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Entity\Following;
use EngagementBundle\Tests\Integration\BaseIntegration;
use EngagementBundle\VO\V1\ContentGroupVO;
use EngagementBundle\VO\V1\FollowingVO;

class ContentGroupDataMapperTest extends BaseIntegration
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testMappingProcess()
    {
        //get entity
        $entity = $this->contentgroups->find(1);

        //map entity to vo
        $vo = $this->contentGroupDataMapper->mapFullEntityToVO_V1($entity);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //parse VO to json, and viceversa
        $json = DataMapperUtil::mapModelToJSON($this->serializer, $vo);
        $vo = DataMapperUtil::mapJSONToModel($json, $this->serializer, ContentGroupVO::class);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //map vo to entity
        $this->contentGroupDataMapper->mapVO_V1ToEntity($vo, $entity);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

    }

    private function assertFieldsEquality(ContentGroup &$entity, ContentGroupVO &$vo) {
        $this->assertEquals($entity->getId(), $vo->getId());
        $this->assertEquals($entity->getCreatedAt()->getTimestamp(), $vo->getCreatedAt()->getTimestamp());
        $this->assertEquals($entity->getUpdatedAt()->getTimestamp(), $vo->getUpdatedAt()->getTimestamp());
        $this->assertEquals($entity->getName(), $vo->getName());
        $this->assertEquals($entity->getDescription(), $vo->getDescription());
        $this->assertEquals($entity->getPhotoUrl(), $vo->getPhotoUrl());
        $this->assertEquals(sizeof($entity->getPosts()), sizeof($vo->getPosts()));
    }
}