<?php

namespace EngagementBundle\Tests\DataMapper;

use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\UserDataMapper;
use EngagementBundle\Entity\User;
use EngagementBundle\Tests\Integration\BaseIntegration;
use EngagementBundle\VO\V1\OpenGraphDataVO;
use EngagementBundle\VO\V1\UserVO;

class UserDataMapperTest extends BaseIntegration
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testMappingProcess()
    {
        //get entity
        $entity = $this->users->find(1);

        //map entity to vo
        $tmpArray = array($entity);
        $voList = $this->userDataMapper->mapListBaseEntityToVO_V1($tmpArray);
        $voList = $this->userDataMapper->mapListFullEntityToVO_V1($tmpArray);
        $vo = $voList[0];

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //parse VO to json, and viceversa
        $json = DataMapperUtil::mapModelToJSON($this->serializer, $vo);
        $vo = DataMapperUtil::mapJSONToModel($json, $this->serializer, UserVO::class);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //map vo to entity
        $this->userDataMapper->mapVO_V1ToEntity($vo, $entity);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

    }

    private function assertFieldsEquality(User &$entity, UserVO &$vo) {
        $this->assertEquals($entity->getId(), $vo->getId());
        $this->assertEquals($entity->getFirst(), $vo->getFirst());
        $this->assertEquals($entity->getLast(), $vo->getLast());
        $this->assertEquals($entity->getPhotoUrl(), $vo->getPhotoUrl());
    }
}