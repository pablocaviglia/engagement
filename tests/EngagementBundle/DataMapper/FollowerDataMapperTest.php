<?php

namespace EngagementBundle\Tests\DataMapper;

use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\FollowerDataMapper;
use EngagementBundle\Entity\Following;
use EngagementBundle\Tests\Integration\BaseIntegration;
use EngagementBundle\VO\V1\FollowerVO;

class FollowerDataMapperTest extends BaseIntegration
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testMappingProcess()
    {
        //get entity
        $entity = $this->users->find(2)->getFollowers()[0];

        //map entity to vo
        $tmpArray = array($entity);
        $voList = $this->followerDataMapper->mapListBaseEntityToVO_V1($tmpArray);
        $voList = $this->followerDataMapper->mapListFullEntityToVO_V1($tmpArray);
        $vo = $voList[0];

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //parse VO to json, and viceversa
        $json = DataMapperUtil::mapModelToJSON($this->serializer, $vo);
        $vo = DataMapperUtil::mapJSONToModel($json, $this->serializer, FollowerVO::class);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //map vo to entity
        $this->followerDataMapper->mapVO_V1ToEntity($vo, $entity);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

    }

    private function assertFieldsEquality(Following &$entity, FollowerVO &$vo) {
        $this->assertEquals($entity->getId(), $vo->getId());
        $this->assertEquals($entity->getCreatedAt()->getTimestamp(), $vo->getCreatedAt()->getTimestamp());
    }
}