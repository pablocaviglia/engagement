<?php

namespace EngagementBundle\Tests\DataMapper;

use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\PostDataMapper;
use EngagementBundle\Entity\Post;
use EngagementBundle\Tests\Integration\BaseIntegration;
use EngagementBundle\VO\V1\PostVO;

class PostDataMapperTest extends BaseIntegration
{

    public function setUp()
    {
        parent::setUp();
    }

    public function testMappingProcess()
    {
        //get entity
        $entity = $this->posts->find(1);

        //map entity to vo
        $tmpArray = array($entity);
        $voList = $this->postDataMapper->mapListBaseEntityToVO_V1($tmpArray);
        $voList = $this->postDataMapper->mapListFullEntityToVO_V1($tmpArray);
        $vo = $voList[0];

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //parse VO to json, and viceversa
        $json = DataMapperUtil::mapModelToJSON($this->serializer, $vo);
        $vo = DataMapperUtil::mapJSONToModel($json, $this->serializer, PostVO::class);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

        //map vo to entity
        $this->postDataMapper->mapVO_V1ToEntity($vo, $entity);

        //assert fields equality
        $this->assertFieldsEquality($entity, $vo);

    }

    private function assertFieldsEquality(Post &$entity, PostVO &$vo) {
        $this->assertEquals($entity->getId(), $vo->getId());
        $this->assertEquals($entity->getContent(), $vo->getContent());
        $this->assertEquals($entity->getTitle(), $vo->getTitle());
        $this->assertEquals($entity->getCreatedAt()->getTimestamp(), $vo->getCreatedAt()->getTimestamp());
        $this->assertEquals($entity->getUpdatedAt()->getTimestamp(), $vo->getUpdatedAt()->getTimestamp());
    }
}