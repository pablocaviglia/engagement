<?php


namespace EngagementBundle\Tests\Service;

use EngagementBundle\Entity\Post;
use EngagementBundle\Exception\EngagementException;
use EngagementBundle\Exception\InvalidPostException;

class PostServiceTest extends BaseServiceTest
{
    public function setUp()
    {
        parent::setUp();
    }

    function testUserPosts() {

        $postService = $this->getPostService();

        //data retrieval
        $result = $postService->getPostsByUserId(1, 10, 0, false);
        $this->assertEquals(1, sizeof($result));

        //count
        $result = $postService->getPostsByUserId(1, 10, 0, true);
        $this->assertEquals(1, $result);

    }

    function testEditPost() {

        $postService = $this->getPostService();

        //get post
        $postOriginal = $postService->getPost(1);

        //new post
        $postNewTitle = "new title";
        $postNewContent = "new content";
        $postNewOpenGraph = "\"open_graph_data\":{
                                \"ogTitle\":\"modified\",
                                \"ogType\":\"article\",
                                \"ogUrl\":\"http://ask-well-should-you-filter-your-water/\",
                                \"ogSiteName\":\"Well\",
                                \"ogDescription\":\"Although municipal tap of the limits may be too lenient, a research analyst says.\",
                                \"ogImage\":{
                                    \"url\":\"http://graphics8.well_water-facebookJumbo.jpg\"
                                }
                            }";

        $postNew = new Post();
        $postNew->setId($postOriginal->getId());
        $postNew->setTitle($postNewTitle);
        $postNew->setContent($postNewContent);
        $postNew->setOpenGraphData($postNewOpenGraph);

        //save post
        $postService->savePostContent($postNew, 1);

        //get modified post
        $postOriginal = $postService->getPost(1);

        //compare with new
        $this->assertEquals($postOriginal->getTitle(), $postNew->getTitle());
        $this->assertEquals($postOriginal->getContent(), $postNew->getContent());
        $this->assertEquals($postOriginal->getOpenGraphData(), $postNew->getOpenGraphData());
    }

    /**
     * @expectedException EngagementBundle\Exception\InvalidPostException
     */
    function testSaveInvalidPostId() {
        $postService = $this->getPostService();
        $post = $postService->getPost(1);

        $postService->savePostContent($post, 9999999);
    }

    /**
     * @expectedException EngagementBundle\Exception\InvalidPostException
     */
    function testSavePostNotFound() {
        $postService = $this->getPostService();
        $post = $postService->getPost(1);
        $post->setId(9999999);

        $postService->savePostContent($post, 9999999);
    }

    function testCreatePost() {

        $postService = $this->getPostService();

        //create post
        $post = new Post();
        $post->setTitle("Test post saving title");
        $post->setContent("Test post saving content");
        $result = $postService->addPost(1, $post);

        $this->assertTrue(!is_null($result->getId()));
    }

    function testCreateContentGroupPost() {

        $contentGroupService = $this->getContentGroupService();
        $postService = $this->getPostService();

        //get the content group
        $contentGroup = $contentGroupService->getContentGroup(1);

        //create post associated to the content group
        $post = new Post();
        $post->setTitle("Test post saving title");
        $post->setContent("Test post saving content");
        $post->setContentGroup($contentGroup);
        $result = $postService->addPost(1, $post);

        $this->assertTrue(!is_null($result->getId()));
    }

    function testTopPosters() {

        $postService = $this->getPostService();

        //get qnty of posts of user
        $userId = 1;
        $userPostsQnty = $postService->getPostsByUserId($userId, 1, 0, true);

        $result = $postService->getTopPosters(100);

        $valid = false;
        foreach($result as $topPoster) {
            $user = $topPoster[0];
            $qntyPosts = $topPoster['qntyPosts'];
            if($user->getId() == $userId) {
                if($userPostsQnty == $qntyPosts) {
                    $valid = true;
                    break;
                }
            }
        }

        $this->assertTrue($valid, "Couldn't find top post user");
    }

    function testBasicSearch() {
        $postService = $this->getPostService();
        $posts = $postService->getSearchResults('seed', 10, 0);
        $this->assertEquals(3, count($posts));
    }

    function testGetPostById() {
        $postService = $this->getPostService();

        $post = $postService->getPost(1);
        $this->assertEquals(1, count($post));
        $this->assertEquals(1, $post->getId());
    }

    function testPagination() {
        $postService = $this->getPostService();

        $id = null;

        $posts = $postService->getSearchResults('seed', 1, 0);
        $this->assertEquals(1, count($posts));
        $this->assertEquals(1, $posts[0]->getId());
        $id = $posts[0]->getId();

        $posts = $postService->getSearchResults('seed', 1, 1);
        $this->assertEquals(1, count($posts));
        $this->assertNotEquals($id, $posts[0]->getId());
        $id = $posts[0]->getId();

        $posts = $postService->getSearchResults('seed', 1, 2);
        $this->assertEquals(1, count($posts));
        $this->assertNotEquals($id, $posts[0]->getId());
    }

    function testLogImpression() {
        $postService = $this->getPostService();
        $userService = $this->getUserService();

        $post = $postService->getPost(1);

        $impression = $postService->logImpression(2, $post);
        $this->assertEquals(1, $impression->getId());
    }
}