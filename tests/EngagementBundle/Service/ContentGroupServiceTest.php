<?php

namespace EngagementBundle\Tests\Service;

use EngagementBundle\Constant\CriteriaField;
use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Entity\Post;
use EngagementBundle\Exception\EngagementException;

class ContentGroupServiceTest extends BaseServiceTest
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testGetContentGroups()
    {
        $contentGroupService = $this->getContentGroupService();

        $groups = $contentGroupService->getContentGroups(10, 0, 0, array());
        $this->assertEquals(2, count($groups));
        $this->assertEquals("The first group", $groups[0]->getName());
    }

    public function testSearchContentGroups()
    {
        $criteria = array();

        //get first group
        $criteria[CriteriaField::CONTENT_GROUP_NAME_LIKE] = 'the first';
        $contentGroupService = parent::getContentGroupService();
        $result  = $contentGroupService->getContentGroups(10, 0, false, $criteria);
        $this->assertEquals(1, count($result));

        //get second group
        $criteria[CriteriaField::CONTENT_GROUP_NAME_LIKE] = 'the second';
        $contentGroupService = parent::getContentGroupService();
        $result  = $contentGroupService->getContentGroups(10, 0, false, $criteria);
        $this->assertEquals(1, count($result));

        //get both groups
        $criteria[CriteriaField::CONTENT_GROUP_NAME_LIKE] = 'the';
        $contentGroupService = parent::getContentGroupService();
        $result  = $contentGroupService->getContentGroups(10, 0, false, $criteria);
        $this->assertEquals(2, count($result));

    }

    public function testContentGroupById() {
        $contentGroupService = $this->getContentGroupService();

        $contentGroup = $contentGroupService->getContentGroup(1);
        $this->assertEquals(1, count($contentGroup));
        $this->assertEquals(1, $contentGroup->getId());
    }

    function testContentGroupFeed() {

        $postService = $this->getPostService();
        $contentGroupService = $this->getContentGroupService();

        $contentGroup = $contentGroupService->getContentGroup(1);

        $post = new Post();
        $post->setTitle("post title");
        $post->setContent("post content");
        $post->setContentGroup($contentGroup);

        //save content group post
        $postService->addPost(1, $post);

        //data retrieval
        $result = $postService->getContentGroupFeed(1, 10, 0, false);
        $this->assertEquals(1, sizeof($result));

        //count
        $result = $postService->getContentGroupFeed(1, 10, 0, true);
        $this->assertEquals(1, $result);

    }

    function testCreateContentGroup() {

        $contentGroupService = $this->getContentGroupService();
        $userService = $this->getUserService();

        $user = $userService->getUser(1);

        $contentGroup = new ContentGroup();
        $contentGroup->setName("content group name");
        $contentGroup->setDescription("content group description");
        $contentGroup->setCreatedBy($user);

        //save content group post
        $contentGroup = $contentGroupService->createContentGroup($contentGroup);

        //data retrieval
        $result = $contentGroupService->getContentGroup($contentGroup->getId());
        $this->assertTrue(!is_null($result->getId()));

    }

    function testModifyContentGroup() {

        $contentGroupService = $this->getContentGroupService();
        $userService = $this->getUserService();

        $user = $userService->getUser(1);

        $contentGroup = new ContentGroup();
        $contentGroup->setName("content group name");
        $contentGroup->setDescription("content group description");
        $contentGroup->setCreatedBy($user);

        //save content group post
        $contentGroup = $contentGroupService->createContentGroup($contentGroup);

        //data retrieval
        $modName = "MOD **** content group name ***";
        $result = $contentGroupService->getContentGroup($contentGroup->getId());
        $result->setName($modName);

        //save modified group
        $contentGroupService->modifyContentGroup($result);

        //data retrieval
        $result = $contentGroupService->getContentGroup($contentGroup->getId());

        //validate
        $this->assertEquals($modName, $result->getName());

    }

    function testDeleteContentGroup() {

        $contentGroupService = $this->getContentGroupService();
        $userService = $this->getUserService();

        $user = $userService->getUser(1);

        $contentGroup = new ContentGroup();
        $contentGroup->setName("content group name");
        $contentGroup->setDescription("content group description");
        $contentGroup->setCreatedBy($user);

        //save content group post
        $contentGroup = $contentGroupService->createContentGroup($contentGroup);

        //delete group
        $contentGroupService->deleteContentGroup($contentGroup->getId());

        try {
            //data retrieval
            $contentGroupService->getContentGroup($contentGroup->getId());
            $this->assertTrue(false);
        }
        catch(EngagementException $exception) {
            $this->assertTrue(true);
        }
    }
}