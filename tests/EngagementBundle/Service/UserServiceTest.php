<?php


namespace EngagementBundle\Tests\Service;

use EngagementBundle\Exception\InvalidUserException;

class UserServiceTest extends BaseServiceTest
{
    public function setUp()
    {
        parent::setUp();
    }

    function testUsers() {

        $userService = $this->getUserService();

        //data retrieval
        $result = $userService->findBy(10, 0, false);
        $this->assertEquals(3, sizeof($result));

        //count
        $result = $userService->findBy(10, 0, true);
        $this->assertEquals(3, $result);

    }

    function testUserFollowings() {

        $userService = $this->getUserService();

        //data retrieval
        $result = $userService->getUserFollowings(1, 10, 0, false);
        $this->assertEquals(1, sizeof($result));

        //count
        $result = $userService->getUserFollowings(1, 10, 0, true);
        $this->assertEquals(1, $result);

    }

    function testUsersToFollow() {

        $userService = $this->getUserService();

        //data retrieval
        $result = $userService->getUsersToFollow(1, 10, 0, false);
        $this->assertEquals(1, sizeof($result));

        //count
        $result = $userService->getUsersToFollow(1, 10, 0, true);
        $this->assertEquals(1, $result);

    }

    function testUserFeed() {

        $postService = $this->getPostService();

        //data retrieval
        $result = $postService->getUserFeed(1, 10, 0, false);
        $this->assertEquals(3, sizeof($result));

        //count
        $result = $postService->getUserFeed(1, 10, 0, true);
        $this->assertEquals(3, $result);

    }

    function testGetInvalidUser() {

        $userService = $this->getUserService();

        try {
            //data retrieval
            $userService->getUser(999999);
        }
        catch(InvalidUserException $e) {
            $this->assertTrue(true);
            return;
        }
        $this->assertTrue(false, "Exception not thrown for invalid user");
    }
}