<?php


namespace EngagementBundle\Service;


use EngagementBundle\Tests\Service\BaseServiceTest;

class ContentGroupFollowingServiceTest extends BaseServiceTest
{
    public function setUp()
    {
        parent::setUp();
    }

    public function testFollowUnfollowContentGroup()
    {
        $contentGroupFollowingService = $this->getContentGroupFollowingService();

        //start following content group
        $contentGroupFollowingService->addFollow(1, 1);

        //get content group followings of the user
        $contentGroupFollowings = $this->getContentGroupFollowingService()->findFollowingsByUserId(1, array(), 10, 0, 0);

        //test
        $this->assertEquals(1, sizeof($contentGroupFollowings));

        //unfollow the group
        $contentGroupFollowingService->removeFollow(1, 1);

        //get content group followings of the user
        $contentGroupFollowingsSize = $this->getContentGroupFollowingService()->findFollowingsByUserId(1, array(), 10, 0, 1);

        //test
        $this->assertEquals(0, $contentGroupFollowingsSize);
    }
}