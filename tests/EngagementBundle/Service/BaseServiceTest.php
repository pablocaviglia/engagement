<?php

namespace EngagementBundle\Tests\Service;

use EngagementBundle\Tests\Integration\BaseIntegration;

class BaseServiceTest extends BaseIntegration
{
    private $container;

    public function setUp()
    {
        self::bootKernel();
        $this->container = self::$kernel->getContainer();
        parent::setUp();
    }

    public function getContentGroupService()
    {
        return $this->container->get('engagement.content_group_service');
    }

    public function getUserService()
    {
        return $this->container->get('engagement.user_service');
    }

    public function getPostService()
    {
        return $this->container->get('engagement.post_service');
    }

    public function getContentGroupFollowingService()
    {
        return $this->container->get('engagement.content_group_following_service');
    }

    public function testContainer()
    {
        $this->assertNotNull($this->container);
    }
}