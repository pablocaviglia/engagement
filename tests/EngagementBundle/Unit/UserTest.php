<?php

namespace EngagementBundle\Tests\Unit;


use EngagementBundle\Entity\User;
use EngagementBundle\Tests\Unit\BaseUnit;

class UserTest extends BaseUnit
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testUserCreation()
    {
        $user = new User();
        $user->setFirst("Test Gen 1");
        $user->setLast("Last Name");

        $this->em->persist($user);
        $this->em->flush();

        $this->assertEquals("Test Gen 1", $user->getFirst());
    }

    public function testFullName()
    {
        $user_repo = $this->em->getRepository('EngagementBundle:User');
        $user = $user_repo->findOneBy(array('first'=> "Roger"));
        $this->assertEquals($user->getFirst() . " " . $user->getLast(), $user->getFullName());
    }
}
