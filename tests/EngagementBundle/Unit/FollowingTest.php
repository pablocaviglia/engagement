<?php

namespace EngagementBundle\Tests\Unit;

use EngagementBundle\Entity\Following;

class FollowingTest extends BaseUnit
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testFollowUser()
    {
        $user_repo = $this->em->getRepository('EngagementBundle:User');
        $user = $user_repo->findOneBy(array('first'=> "Roger"));
        $user_to_follow = $user_repo->findOneBy(array('first'=> "Joe"));
        $following = new Following();
        $following->setUser($user);
        $following->setFollowingUser($user_to_follow);

        $this->em->persist($following);
        $this->em->flush();

        $this->assertEquals(2, $following->getId());
        $this->assertEquals($user->getId(), $following->getUser()->getId());
        $this->assertEquals($user_to_follow->getId(), $following->getFollowingUser()->getId());
    }

    public function testKeyFollowingData()
    {
        $followings = $this->followings->findAll();

        foreach ($followings as $following)
        {
            $this->assertNotNull($following->getCreatedAt());
        }
    }
}