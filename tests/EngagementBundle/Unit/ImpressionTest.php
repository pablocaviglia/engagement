<?php

namespace EngagementBundle\Tests\Unit;


use EngagementBundle\Entity\Impression;
use EngagementBundle\Tests\Unit\BaseUnit;

class ImpressionTest extends BaseUnit
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testImpressionCreation()
    {
        $impression = new Impression();

        $this->em->persist($impression);
        $this->em->flush();

        $impressions = $this->impressions->findAll();
        $this->assertEquals(1, count($impressions));
        $this->assertNotNull($this->impressions->find(1)->getCreatedAt());
        $this->assertNull($this->impressions->find(1)->getPost());
    }
}
