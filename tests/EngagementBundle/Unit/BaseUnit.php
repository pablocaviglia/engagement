<?php

namespace EngagementBundle\Tests\Unit;

use EngagementBundle\DataFixtures\ORM\LoadGroupData;
use EngagementBundle\DataFixtures\ORM\LoadPostData;
use EngagementBundle\DataFixtures\ORM\LoadUserData;
use EngagementBundle\Tests\BaseFixtureLoader;

class BaseUnit extends BaseFixtureLoader
{
    protected $users;
    protected $posts;
    protected $em;
    protected $followings;
    protected $contentGroups;
    protected $impressions;

    public function setUp()
    {
        parent::setUp();

        // Base fixture for all tests
        $this->addFixture(new LoadUserData());
        $this->addFixture(new LoadPostData());
        $this->addFixture(new LoadGroupData());
        $this->executeFixtures();

        // Fixtures are now loaded in a clean DB. Yay!

        $kernel = static::createKernel();
        $kernel->boot();
        $this->users = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:User')
        ;

        $this->posts = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:Post')
        ;

        $this->followings = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:Following');

        $this->contentGroups = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:ContentGroup');

        $this->impressions = $kernel
            ->getContainer()
            ->get('doctrine.orm.entity_manager')
            ->getRepository('EngagementBundle:Impression');

        $this->em = $kernel->getContainer()->get('doctrine.orm.entity_manager');
    }
}