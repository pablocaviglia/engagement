<?php

namespace EngagementBundle\Tests\Unit;

class GroupContentTest extends BaseUnit
{
    public function setUp()
    {
        //$users and $posts are inherited and populated with the repository
        parent::setUp();
    }

    public function testKeyGroupData()
    {
        $user = $this->users->find(1);
        $group = $this->contentGroups->find(1);

        $this->assertNotNull($group->getName());
        $this->assertNotNull($group->getDescription());
        $this->assertNotNull($group->getCreatedAt());
        $this->assertNotNull($group->getUpdatedAt());
    }
}