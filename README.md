# engagement

A Symfony project created on December 23, 2015, 10:20 am.

Remember to run `composer update` to install all of the project dependencies.

In addition this dev environment needs a local mysql instance running on port 3306.

Create dev database `php bin/console doctrine:database:create`

Update schema `php bin/console doctrine:schema:update --force`

Load Fixtures `php bin/console doctrine:fixtures:load`

Create Entities `php bin/console doctrine:generate:entity`

Update Entities `php bin/console doctrine:generate:entities`

Clear Dev Cache `php bin/console cache:clear --env=dev`
Clear Test Cache `php bin/console cache:clear --env=test`

Start Dev Server `php bin/console server:start`

Create Test Database `php bin/console doctrine:schema:update --env=test --force`

# Before Running PHP Unit

Set the following

`export SYMFONY__ELASTICSEARCH__SUFFIX=<your_unique_dev_name>`

`export ELASTIC_HOST=localhost`

`export ELASTIC_PORT=9200`

`php app/console fos:elastica:populate --env=dev|test`

Run tests `./vendor/bin/phpunit`

# Information about how to enhance auto generated documentation can be found here:
#   http://phpdoc.org/docs/latest/welcome.html
Generate documentation `php bin/console documentation:create`

# Get full API documentation
http://{PROJECT_URL}/api/doc

# Generate full API documentation
php bin/console api:doc:dump --format html > apidoc.html

# engagement UI
The steps below will allow you to run the web UI locally at http://localhost:5000.

1. `git clone https://github.com/brentkastner/engagement`. You probably already have this step done!

2. Install Node.js and npm (Node Package Manager) via https://nodejs.org/en/.

3. Verify that both Node.js and npm are installed:
`node -v` and `npm -v` should both return a version.

4. Install the UI dependencies (it may need sudo depending on how you installed Node.js and npm): `npm install` and then `npm install -g gulp`.

5. Type `gulp` to build and run the project.

6. Open a browser and go to http://localhost:5000.

The API calls will proxy to https://fast-wave-2047.herokuapp.com/api/v1/. If you need to run the UI against a local Symfony project, you can modify gulpfile.js in the root directory.

1. Open `gulpfile.js`.
2. Find the `connect` task.
3. The API Url is within a `url.parse` method (you could also search `gulpfile.js` for `url.parse`).
4. Modify the URL to point to your localhost, for example, `"http://localhost:8000"`.
