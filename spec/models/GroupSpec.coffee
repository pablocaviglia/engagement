define (require) ->

  App = require 'cs!coffee/App'
  Group = require 'cs!src/EngagementBundle/Resources/public/coffee/models/Group'
  sinon = require 'sinon'

  require('jasmine-sinon')

  Backbone.$.ajaxSettings.xhr = ->
    return new XMLHttpRequest()

  describe 'Group', ->

    # All Specs  ---------------------------------------------------

    App.user = new (Backbone.Model) (full_name: 'Roger Clyne', id: '1', first: 'Roger', last: 'Clyne')
    App.user.groups = new (Backbone.Collection) (id: '2', name: 'X-Ray', description: 'Discussion about x-rays', created_at: '2016-01-19T17:32:47+0000', updated_at: '2016-02-23T15:51:57+0000')

    # Spec Specific ------------------------------------------------

    beforeEach( ->
      @server = sinon.fakeServer.create()
      @spy = sinon.spy(Backbone.$, 'ajax')
      return
    )

    afterEach( ->
      @server.restore()
      Backbone.$.ajax.restore()
      return
    )

    it 'set the attributes after a fetch', ->

      group = new Group(
        id: 38
        #created_by:
          #id: App.user.get('id')
        #name: 'Test Group'
        #desciption: 'Just a group for testing'
      )

      @server.respondWith('GET', '/api/v1/content_group/38',
        [200, { "Content-Type": "application/json" }, JSON.stringify(
          id: "38"
          name: "Test Group"
          description: "Just a group for testing"
          created_by:
            full_name: "Roger Clyne"
            id: "1",
            first: "Roger",
            last:" Clyne"
          created_at: "2016-03-03T22:19:58+0000"
          updated_at: "2016-03-03T22:19:58+0000"
          )
        ]
      )

      group.fetch()

      @server.respond()

      expect(@spy).toHaveBeenCalledOnce()
      expect(group.get('name')).toEqual('Test Group') # We want the name to be provided by the server

      return

    it 'should set the attributes after a save', ->

      group = new Group( # Do not add an Id!
        created_by:
          id: App.user.get('id')
        name: 'Test Group'
        desciption: 'Just a group for testing'
      )

      expect(group.url()).toEqual('/api/v1/content_group') # As of now the Url should not have an Id

      @server.respondWith('POST', '/api/v1/content_group',
        [200, { "Content-Type": "application/json" }, JSON.stringify(
          id: "38"
          name: "Test Group"
          description: "Just a group for testing"
          created_by:
            full_name: "Roger Clyne"
            id: "1",
            first: "Roger",
            last:" Clyne"
          created_at: "2016-03-03T22:19:58+0000"
          updated_at: "2016-03-03T22:19:58+0000"
          )
        ]
      )

      group.save()

      @server.respond()

      expect(@spy).toHaveBeenCalledOnce()
      expect(group.get('created_by').full_name).toEqual('Roger Clyne') # We want the server to add the created_by
      expect(group.url()).toEqual('/api/v1/content_group/38') # We want an update Url with the correct Id

      return

    return
