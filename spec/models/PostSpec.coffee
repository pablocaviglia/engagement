define (require) ->

  App = require 'cs!coffee/App'
  Post = require 'cs!src/EngagementBundle/Resources/public/coffee/models/Post'
  PostsPage = require 'cs!src/EngagementBundle/Resources/public/coffee/views/posts/PostsPage'
  sinon = require 'sinon'

  require('jasmine-sinon')

  Backbone.$.ajaxSettings.xhr = ->
    return new XMLHttpRequest()

  describe 'Post', ->

    # All Specs  ---------------------------------------------------

    App.user = new (Backbone.Model) (full_name: 'Roger Clyne', id: '1', first: 'Roger', last: 'Clyne')
    App.user.groups = new (Backbone.Collection) (id: '2', name: 'X-Ray', description: 'Discussion about x-rays', created_at: '2016-01-19T17:32:47+0000', updated_at: '2016-02-23T15:51:57+0000')

    # Spec Specific ------------------------------------------------

    beforeEach( ->
      @server = sinon.fakeServer.create()
      @spy = sinon.spy(Backbone.$, 'ajax')
      return
    )

    afterEach( ->
      @server.restore()
      Backbone.$.ajax.restore()
      return
    )

    # View Specific ------------------------------------------------

    it 'should make the correct server request', ->

      post = new Post(
        user:
          id: App.user.id
        title: 'Sinon'
        content: 'Test'
      )
      
      post.save()
      expect(@spy.getCall(0).args[0].url).toEqual('/api/v1/user/1/post')
      expect(@spy).toHaveBeenCalledOnce()

      return

    return
