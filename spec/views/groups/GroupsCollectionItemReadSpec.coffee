define (require) ->

  App = require 'cs!coffee/App'
  GroupsCollectionItemRead = require 'cs!src/EngagementBundle/Resources/public/coffee/views/groups/GroupsCollectionItemRead'

  describe 'GroupsCollectionItemRead', ->

    # All Specs  ---------------------------------------------------

    App.user = new (Backbone.Model) (full_name: 'Roger Clyne', id: '1', first: 'Roger', last: 'Clyne')
    App.user.groups = new (Backbone.Collection) (id: '2', name: 'X-Ray', description: 'Discussion about x-rays', created_at: '2016-01-19T17:32:47+0000', updated_at: '2016-02-23T15:51:57+0000')

    # Spec Specific ------------------------------------------------

    model = new (Backbone.Model) (
      id: '2'
      name: 'X-Ray'
      description: 'Discussion about x-rays'
      created_at: '2016-01-19T17:32:47+0000'
      updated_at: '2016-02-23T15:51:57+0000'
      created_by:
        full_name: 'Roger Clyne'
        id: '1'
        first: 'Roger'
        last: 'Clyne'
    )
    view = null

    beforeEach ->
      view = new GroupsCollectionItemRead(model: model)
      return

    # All Views ----------------------------------------------------

    it 'should show each of its defined regions in the template', ->
      if view.regions
        view.render()
        Object.keys(view.regions).forEach (key, index) ->
          expect(view.$el.find(view.regions[key]).length).toEqual(1)
          return
      return

    # View Specific ------------------------------------------------

    it 'should display an edit/delete dropdown if the author is the app user', ->
      model.set(
        created_by:
          full_name: 'Roger Clyne'
          id: '1'
          first: 'Roger'
          last: 'Clyne'
      )
      view.render()
      expect(view.$el.find('div.dropdown').length).toEqual(1)
      return

    it 'should not display an edit/delete dropdown if the author is not the app user', ->
      model.set(
        created_by:
          full_name: 'Dean Stockbridge'
          id: '4'
          first: 'Dean'
          last: 'Stockbridge'
      )
      view.render()
      expect(view.$el.find('div.dropdown').length).toEqual(0)
      return

    it 'should display a following button if the user is part of the group', ->
      App.user.groups.add(model)
      view.render()
      expect(view.$el.find('button:contains("Following")').text().indexOf('Following…')).toBeGreaterThan(-1)
      return

    it 'should display a follow button if the user is not part of the group', ->
      App.user.groups.remove(model)
      view.render()
      expect(view.$el.find('button:contains("Follow")').text().indexOf('Follow…')).toBeGreaterThan(-1)
      return

  return
