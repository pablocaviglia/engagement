define (require) ->

  App = require 'cs!coffee/App'
  GroupsCollectionItem = require 'cs!src/EngagementBundle/Resources/public/coffee/views/groups/GroupsCollectionItem'

  describe 'GroupsCollectionItem', ->

    # All Specs  ---------------------------------------------------

    App.user = new (Backbone.Model) (full_name: 'Roger Clyne', id: '1', first: 'Roger', last: 'Clyne')
    App.user.groups = new (Backbone.Collection) (id: '2', name: 'X-Ray', description: 'Discussion about x-rays', created_at: '2016-01-19T17:32:47+0000', updated_at: '2016-02-23T15:51:57+0000')

    # Spec Specific ------------------------------------------------

    model = new (Backbone.Model) (
      id: '2'
      name: 'X-Ray'
      description: 'Discussion about x-rays'
      created_at: '2016-01-19T17:32:47+0000'
      updated_at: '2016-02-23T15:51:57+0000'
      created_by:
        full_name: 'Roger Clyne'
        id: '1'
        first: 'Roger'
        last: 'Clyne'
    )
    view = null

    beforeEach ->
      view = new GroupsCollectionItem(model: model)
      return

    # All Views ----------------------------------------------------

    it 'should show each of its defined regions in the template', ->
      if view.regions
        view.render()
        Object.keys(view.regions).forEach (key, index) ->
          expect(view.$el.find(view.regions[key]).length).toEqual(1)
          return
      return

    # View Specific ------------------------------------------------

  return
