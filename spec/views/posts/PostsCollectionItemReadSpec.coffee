define (require) ->

  App = require 'cs!coffee/App'
  PostsCollectionItemRead = require 'cs!src/EngagementBundle/Resources/public/coffee/views/posts/PostsCollectionItemRead'

  describe 'PostsCollectionItemRead', ->

    # All Specs  ---------------------------------------------------

    App.user = new (Backbone.Model) (full_name: 'Roger Clyne', id: '1', first: 'Roger', last: 'Clyne')
    App.user.groups = new (Backbone.Collection) (id: '2', name: 'X-Ray', description: 'Discussion about x-rays', created_at: '2016-01-19T17:32:47+0000', updated_at: '2016-02-23T15:51:57+0000')

    # Spec Specific ------------------------------------------------

    model = new (Backbone.Model) (
      id: '393'
      title: 'Testing a post'
      content: 'Way to go! Running #jasmine tests! :thumbsup:'
      created_at: '2016-02-29T19:36:24+0000'
      updated_at: '2016-02-29T19:36:24+0000'
      content_group:
        id: '3'
        name: 'Pharmaceuticals'
        description: 'Pharmaceutical industry developments'
        created_at: '2016-01-19T17:53:41+0000'
        updated_at: '2016-02-23T15:53:42+0000'
      user:
        full_name: 'Roger Clyne'
        id: '1'
        first: 'Roger'
        last: 'Clyne'
    )
    view = null

    beforeEach ->
      view = new PostsCollectionItemRead(model: model)
      return

    # All Views ----------------------------------------------------

    it 'should show each of its defined regions in the template', ->
      if view.regions
        view.render()
        Object.keys(view.regions).forEach (key, index) ->
          expect(view.$el.find(view.regions[key]).length).toEqual(1)
          return
      return

    ###
    it 'should have a method corresponding to each event in its events object as well as a selector in the template to match', ->
      if view.events
        view.render()
        Object.keys(view.events).forEach (key, index) ->
          console.log(key.split(' ')[1])
          expect(view.$el.find(key.split(' ')[1]).length).toEqual(1)
          return
      return
    ###

    # View Specific ------------------------------------------------

    it 'should display an edit/delete dropdown if the author is the app user', ->
      model.set(
        user:
          full_name: 'Roger Clyne'
          id: '1'
          first: 'Roger'
          last: 'Clyne'
      )
      view.render()
      expect(view.$el.find('div.dropdown').length).toEqual(1)
      return

    it 'should not display an edit/delete dropdown if the author is not the app user', ->
      model.set(
        user:
          full_name: 'Dean Stockbridge'
          id: '4'
          first: 'Dean'
          last: 'Stockbridge'
      )
      view.render()
      expect(view.$el.find('div.dropdown').length).toEqual(0)
      return

    it 'should emojify content', ->
      view.render()
      expect(view.$el.find('[data-target="content"] img.emoji').attr('src').indexOf('thumbsup.png')).toBeGreaterThan(-1)
      return

    it 'should build hashtags around content', ->
      view.render()
      expect(view.$el.html().indexOf('#jasmine</a>')).toBeGreaterThan(-1)
      return

    it 'should show an open graph preview if open graph data is present', ->
      model.set(
        open_graph_data:
          ogTitle: 'Staying Healthy While Traveling the Globe'
          ogType: 'article'
          ogUrl: 'http://well.blogs.nytimes.com/2016/02/22/staying-healthy-while-traveling-the-globe/'
          ogSiteName: 'Well'
          ogDescription: 'Drink bottled water, take Pepto-Bismol preventively and avoid hungry lions.'
          ogImage:
            url: '/assets/images/open_graph.jpg'
      )
      view.render()
      expect(view.$el.find('a.og').length).toEqual(1)
      return

    it 'should truncate content when the content is too long', ->
      model.set(
        content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam quis efficitur purus. Sed aliquet nunc eget sapien ultricies, non consequat odio efficitur. Mauris sagittis cursus dui sed hendrerit. Donec eu eros quis eros lobortis ultricies. Ut eu libero id velit blandit mattis. Etiam finibus, lorem non pharetra fermentum, enim metus aliquet risus, et ornare enim felis sit amet nibh. Nunc volutpat magna leo, sed sagittis dolor placerat quis. Quisque sit amet mi tortor. Integer at urna odio. Morbi sollicitudin, massa vel euismod vehicula, augue sem consectetur lacus, quis efficitur dolor ligula consectetur libero. Cras vestibulum tortor malesuada, laoreet tellus eu, consectetur libero. Phasellus et aliquam nulla. Aenean at neque felis. Cras gravida sem quis dignissim molestie. Donec eleifend nibh at lorem euismod, at tempus arcu porta. Mauris hendrerit libero metus, non fringilla lorem blandit eget. Maecenas turpis magna, egestas a elit eu, viverra ornare dolor. Maecenas varius porttitor augue sit amet tempor. Suspendisse potenti. Etiam nulla massa, egestas ut nisi vitae, hendrerit ultrices libero. Integer interdum lorem a malesuada aliquet. Cras mauris enim, mattis a nisi vitae, pulvinar faucibus arcu. Phasellus non lectus nibh. Nulla in ultricies nunc. Morbi eu libero egestas, semper augue sed, cursus diam. In ac consequat lorem. Pellentesque eget pellentesque sem, in dictum lacus. Pellentesque lacinia sit amet neque rutrum viverra. Vivamus et eros porttitor, dapibus purus non, maximus nibh. Cras convallis nunc magna, nec dictum dui semper id. Proin et ante quis justo convallis sollicitudin. Duis eros est, pulvinar vitae pharetra maximus, porttitor ac erat. Maecenas blandit consequat tellus vitae pellentesque. Nulla facilisi. Duis molestie volutpat viverra. Suspendisse tincidunt condimentum egestas. Pellentesque elementum elit erat, sed ultrices metus porttitor et.'
      )
      view.render()
      expect(view.$el.html().indexOf('>read more')).toBeGreaterThan(-1)
      return

  return
