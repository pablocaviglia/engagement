define (require) ->

  App = require 'cs!coffee/App'
  PostsPage = require 'cs!src/EngagementBundle/Resources/public/coffee/views/posts/PostsPage'

  describe 'PostsCollectionItem', ->

    # All Specs  ---------------------------------------------------

    App.user = new (Backbone.Model) (full_name: 'Roger Clyne', id: '1', first: 'Roger', last: 'Clyne')
    App.user.groups = new (Backbone.Collection) (id: '2', name: 'X-Ray', description: 'Discussion about x-rays', created_at: '2016-01-19T17:32:47+0000', updated_at: '2016-02-23T15:51:57+0000')

    # Spec Specific ------------------------------------------------

    collection = new Backbone.Collection
    model1 = new (Backbone.Model) (
      id: '393'
      title: 'Testing a post'
      content: 'Way to go! Running #jasmine tests! :thumbsup:'
      created_at: '2016-02-29T19:36:24+0000'
      updated_at: '2016-02-29T19:36:24+0000'
      content_group:
        id: '3'
        name: 'Pharmaceuticals'
        description: 'Pharmaceutical industry developments'
        created_at: '2016-01-19T17:53:41+0000'
        updated_at: '2016-02-23T15:53:42+0000'
      user:
        full_name: 'Roger Clyne'
        id: '1'
        first: 'Roger'
        last: 'Clyne'
    )
    model2 = new (Backbone.Model) (
      id: '394'
      title: 'Testing another post'
      content: 'OK'
      created_at: '2016-02-28T19:36:24+0000'
      updated_at: '2016-02-28T19:36:24+0000'
      content_group:
        id: '3'
        name: 'Pharmaceuticals'
        description: 'Pharmaceutical industry developments'
        created_at: '2016-01-19T17:53:41+0000'
        updated_at: '2016-02-23T15:53:42+0000'
      user:
        full_name: 'Dean Stockbridge'
        id: '4'
        first: 'Dean'
        last: 'Stockbridge'
    )
    Backbone.Collection.extend(
      fetch: ->
        console.log('got here')
        @add([model1, model2])
    )
    view = null

    beforeEach ->
      view = new PostsPage(userId: '1')
      return

    # All Views ----------------------------------------------------

    it 'should show each of its defined regions in the template', ->
      if view.regions
        view.render()
        Object.keys(view.regions).forEach (key, index) ->
          expect(view.$el.find(view.regions[key]).length).toEqual(1)
          return
      return

    # View Specific ------------------------------------------------

  return
