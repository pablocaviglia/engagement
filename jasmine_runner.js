var Jasmine = require('jasmine');
var jasmine = new Jasmine();
var util = require('util');
var requirejs = require('requirejs');
var glob = require('glob');

jasmine.onComplete(function(passed) {
  if(passed) {
    console.log('All specs have passed.');
  } else {
    console.log('At least one spec has failed.');
  }
});

jasmine.configureDefaultReporter({
  print: function() {
    process.stdout.write(util.format.apply(this, arguments));
  },
  showColors: true,
  jasmineCorePath: this.jasmineCorePath
});

// Engagement Paths
// Eventually see if we can pull thise from config.coffee. Maybe?
appUrl = './src/EngagementBundle/Resources/public/';
requirejs.config({
  baseUrl: '',
  nodeRequire: require,
  paths: {
    'underscore': [appUrl + 'assets/libs/backbone/underscore-min'],
    'backbone': [appUrl + 'assets/libs/backbone/backbone-min'],
    'marionette': [appUrl + 'assets/libs/backbone/backbone.marionette.min'],
    'handlebars': [appUrl + 'assets/libs/handlebars/handlebars-v4.0.5'],
    'marionette': appUrl + 'assets/libs/backbone/backbone.marionette.min',
    'backgrid': appUrl + 'assets/libs/backbone/backgrid.min',
    'backgrid-paginator': appUrl + 'assets/libs/backbone/backgrid-paginator',
    'backbone-paginator': appUrl + 'assets/libs/backbone/backbone.paginator.min',
    'backbone.paginator': 'backbone-paginator',
    'jquery': appUrl + 'assets/libs/jquery/jquery-1.11.3.min',
    'jquery-truncate': appUrl + 'assets/libs/jquery/jquery.truncate',
    'jquery-textcomplete': appUrl + 'assets/libs/jquery/jquery.textcomplete',
    'text': appUrl + 'assets/libs/require/text',
    'handlebars': appUrl + 'assets/libs/handlebars/handlebars-v4.0.5',
    'bootstrap-affix': appUrl + 'assets/libs/bootstrap/affix',
    'bootstrap-alert': appUrl + 'assets/libs/bootstrap/alert',
    'bootstrap-button': appUrl + 'assets/libs/bootstrap/button',
    'bootstrap-carousel': appUrl + 'assets/libs/bootstrap/carousel',
    'bootstrap-collapse': appUrl + 'assets/libs/bootstrap/collapse',
    'bootstrap-dropdown': appUrl + 'assets/libs/bootstrap/dropdown',
    'bootstrap-modal': appUrl + 'assets/libs/bootstrap/modal',
    'bootstrap-popover': appUrl + 'assets/libs/bootstrap/popover',
    'bootstrap-scrollspy': appUrl + 'assets/libs/bootstrap/scrollspy',
    'bootstrap-tab': appUrl + 'assets/libs/bootstrap/tab',
    'bootstrap-tooltip': appUrl + 'assets/libs/bootstrap/tooltip',
    'bootstrap-transition': appUrl + 'assets/libs/bootstrap/transition',
    'bootstrap-notify': appUrl + 'assets/libs/bootstrap/bootstrap-notify.min',
    'bootstrap-typeahead': appUrl + 'assets/libs/bootstrap/bootstrap3-typeahead.min',
    'moment': appUrl + 'assets/libs/moment/moment.min',
    'md5': appUrl + 'assets/libs/md5/md5.min',
    'masonry': appUrl + 'assets/libs/masonry/masonry.pkgd-4.0.0.min',
    'autolinker': appUrl + 'assets/libs/autolinker/Autolinker.min',
    'showdown': appUrl + 'assets/libs/showdown/showdown.min',
    'emojify': appUrl + 'assets/libs/emojify/emojify.min',

    // Jasmine helpers
    'templates': './src/EngagementBundle/Resources/public/templates',
    'coffee': './src/EngagementBundle/Resources/public/coffee',
    'cs'            : appUrl + 'assets/libs/require/cs',
    'CoffeeScript'  : appUrl + 'assets/libs/coffeescript/CoffeeScript',
    'sinon'         : appUrl + 'assets/libs/sinon/sinon-1.17.3'//,
    // 'jasmine-sinon' : appUrl + 'assets/libs/sinon/jasmine-sinon'
  }
});

global.define = require('requirejs');

var jsdom = require("jsdom");
var window = jsdom.jsdom().defaultView;
global.XMLHttpRequest = window.XMLHttpRequest = require("xmlhttprequest").XMLHttpRequest;

 
jsdom.jQueryify(window, [appUrl + 'assets/libs/jquery/jquery-1.11.3.min.js'], function () {

  // global.XMLHttpRequest = window.XMLHttpRequest;
  global.window = window;
  global.document = window.document;
  global.Image = window.Image;
  global.addEventListener = window.addEventListener
  global.jQuery = global.$ = window.$;

  // jQuery.support.cors = true;
  jQuery.ajaxSettings.xhr = function() {
    return new XMLHttpRequest();
  };

  // Create a file array from a matching glob
  // glob('spec/**/*Spec.coffee', function (er, files) {

  glob('spec/**/*Spec.coffee', function (er, files) {

    // Append cs! to the front of each file so the files will be run through the CoffeeScript plugin
    files.forEach(function(part, index, fileArray) {
      fileArray[index] = 'cs!' + fileArray[index].replace('.coffee', '');
    });

    requirejs(files, function () {

      jasmine.execute();

    });

  });

});