<?php


namespace EngagementBundle\Filter;


use EngagementBundle\Constant\Platform;
use EngagementBundle\Controller\BaseController;
use EngagementBundle\Entity\Analytic\RESTAnalytic;
use EngagementBundle\Service\AnalyticService;
use EngagementBundle\Util\Str;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class RESTFilter
{

    const FILTER_ATTR_TIME_INIT = 'REST_FILTER_ATTR_TIME_INIT';

    private $analyticService;

    public function __construct(AnalyticService $analyticService)
    {
        $this->analyticService = $analyticService;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if ($this->doApplyFilter($event)) {

            $request = $event->getRequest();

            //set init execution time
            $currentMicrotime = microtime(true);
            $request->attributes->set(RESTFilter::FILTER_ATTR_TIME_INIT, $currentMicrotime);
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        $endExecutionMicrotime = microtime(true);
        $request = $event->getRequest();
        $response = $event->getResponse();
        $initExecutionTime = $request->attributes->get(RESTFilter::FILTER_ATTR_TIME_INIT);

        if(isset($initExecutionTime)) {

            //get requested route
            $route = $request->attributes->get('_route');

            //calculate endpoint execution time
            $executionTime = $endExecutionMicrotime-$initExecutionTime;

            //create analytic object
            $restAnalytic = new RESTAnalytic();

            //get data from headers
            $platform = $request->headers->get("X-Engagement-Platform");
            $browserType = $request->headers->get("X-Engagement-Browser");
            $userId = $request->headers->get("X-Engagement-User-Id");
            $deviceModel = $request->headers->get("X-Engagement-Device-Model");
            $deviceDebugMode = $request->headers->get("X-Engagement-Debug");

            $restAnalytic->setPlatform($platform);
            $restAnalytic->setBrowserType($browserType);
            $restAnalytic->setUserId($userId);
            $restAnalytic->setDeviceModel($deviceModel);
            $restAnalytic->setDeviceDebugMode($deviceDebugMode);

            //other fields
            $restAnalytic->setRoute($route);
            $restAnalytic->setCreatedAt(new \DateTime());
            $restAnalytic->setExecutionTime($executionTime);

            $httpStatusCode = $response->getStatusCode();
            if($httpStatusCode >= 400 && $response->getStatusCode() <= 599) {
                $restAnalytic->setHttpErrorCode($httpStatusCode);
                if($httpStatusCode >= 500 && $response->getStatusCode() <= 599) {
                    $json = json_decode($response->getContent());
                    $restAnalytic->setErrorManaged($json->managed);
                    if(array_key_exists('exception', $json) && array_key_exists('error_code', $json->exception)) {
                        $restAnalytic->setErrorCode($json->exception->error_code);
                    }
                }
                else if($httpStatusCode >= 400 && $response->getStatusCode() <= 499) {
                    $restAnalytic->setErrorManaged(false);
                }
            }

            $this->analyticService->save($restAnalytic);
        }
    }

    function doApplyFilter($event) {

        if(method_exists($event, 'getController')) {

            $controller = $event->getController();

            /*
             * $controller passed can be either a class or a Closure.
             * This is not usual in Symfony but it may happen.
             * If it is a class, it comes in array format
             */
            if (!is_array($controller)) {
                return;
            }

            //validate it's a rest api call
            $pathInfo = $event->getRequest()->getPathInfo();

            if(Str::startsWith($pathInfo, "/api/") && !Str::startsWith($pathInfo, "/api/doc")) {
                return true;
            }
        }
        return false;
    }
}