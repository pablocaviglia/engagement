<?php


namespace EngagementBundle\Util;


use DateTime;

class DateUtil
{

    static function formatES(DateTime $dateTime) {
        $dateDate = date_format($dateTime, "Y-m-d");
        $dateTime = date_format($dateTime, "H:i:s");
        $dateFull = $dateDate."T".$dateTime;
        return $dateFull;
    }
}