<?php


namespace EngagementBundle\Service;

use EngagementBundle\Entity\Following;
use EngagementBundle\Repository\FollowingRepository;

class FollowingService
{

    private $userService;
    private $followingRepository;

    public function __construct(FollowingRepository $followingRepository, UserService $userService)
    {
        $this->followingRepository = $followingRepository;
        $this->userService = $userService;
    }

    function findBy($criteria, $order, $limit, $offset)
    {
        $followings = $this->followingRepository->findBy($criteria, $order, $limit, $offset);
        return $followings;
    }

    function findFollowingsByUserId($userId, $orderBy, $limit, $offset, $count)
    {
        $followings = $this->followingRepository->findFollowingsByUserId($userId, $orderBy, $limit, $offset, $count);
        return $followings;
    }

    function findFollowersByUserId($userId, $order, $limit, $offset, $count)
    {
        $followings = $this->followingRepository->findFollowersByUserId($userId, $order, $limit, $offset, $count);
        return $followings;
    }

    function addFollow($userId, $userIdToFollow)
    {
        $following = $this->followingRepository->findByUserAndUserToFollow($userId, $userIdToFollow);
        $user = $this->userService->getUser($userId);
        $userToFollow = $this->userService->getUser($userIdToFollow);

        if(is_null($following) && !is_null($user) && !is_null($userToFollow)) {

            $following = new Following();
            $following->setCreatedAt(new \DateTime());
            $following->setUser($user);
            $following->setFollowingUser($userToFollow);

            $this->followingRepository->save($following);
        }
    }

    function removeFollow($userId, $userIdToUnfollow) {
        $this->followingRepository->removeFollow($userId, $userIdToUnfollow);
    }
}