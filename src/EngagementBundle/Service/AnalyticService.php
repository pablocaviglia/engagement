<?php


namespace EngagementBundle\Service;


use EngagementBundle\Entity\Analytic\RESTAnalytic;
use EngagementBundle\Repository\AnalyticRESTRepository;

class AnalyticService
{

    private $analyticRESTRepository;

    public function __construct(AnalyticRESTRepository $analyticRESTRepository) {
        $this->analyticRESTRepository = $analyticRESTRepository;
    }

    function save($analytic) {
        if($analytic instanceof RESTAnalytic) {
            $this->analyticRESTRepository->save($analytic);
        }
    }
}