<?php


namespace EngagementBundle\Service;


use EngagementBundle\Entity\User;
use EngagementBundle\Exception\InvalidUserException;
use EngagementBundle\Repository\UserRepository;

class UserService
{

    private $userRepository;

    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    function createUser(User $user) {
        $user = $this->userRepository->createUser($user);
        return $user;
    }

    function getUser($id) {
        $user = $this->userRepository->findOneBy(array('id'=>$id));
        if(is_null($user)) {
            throw new InvalidUserException("Cannot find user with id=$id");
        }
        return $user;
    }

    function findBy($limit, $offset, $count)
    {
        $users = $this->userRepository->getUsers($limit, $offset, $count);
        return $users;
    }

    function getUsersToFollow($userId, $limit, $offset, $count) {
        $user = $this->getUser($userId);
        $result = $this->userRepository->getUsersToFollow($user, $limit, $offset, $count);
        return $result;
    }

    function getUserFollowings($userId, $limit, $offset, $count) {
        $user = $this->getUser($userId);
        $users = $this->userRepository->getUserFollowings($user, $limit, $offset, $count);
        return $users;
    }

    function getUserFollowers($userId, $limit, $offset, $count) {
        $user = $this->getUser($userId);
        $users = $this->userRepository->getUserFollowers($user, $limit, $offset, $count);
        return $users;
    }
}