<?php

namespace EngagementBundle\Service;

use Elastica\Filter\Range;
use Elastica\Query;
use Elastica\Query\Filtered;
use Elastica\Result;
use EngagementBundle\Constant\CriteriaField;
use EngagementBundle\DataMapper\PostDataMapper;
use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Entity\Impression;
use EngagementBundle\Entity\Post;
use EngagementBundle\Entity\User;
use EngagementBundle\Exception\InvalidPostException;
use EngagementBundle\Repository\ImpressionRepository;
use EngagementBundle\Repository\PostRepository;
use EngagementBundle\Util\DateUtil;
use EngagementBundle\VO\V1\PostVO;
use Symfony\Component\DependencyInjection\ContainerInterface;

class PostService
{

    private $postRepository;
    private $userService;
    private $contentGroupService;
    private $container;
    private $impressionRepository;

    public function __construct(ContainerInterface $container, PostRepository $postRepository, UserService $userService, ContentGroupService $contentGroupService, ImpressionRepository $impressionRepository)
    {
        $this->postRepository = $postRepository;
        $this->userService = $userService;
        $this->contentGroupService = $contentGroupService;
        $this->container = $container;
        $this->impressionRepository = $impressionRepository;
    }

    function getPost($id) {
        $post = $this->postRepository->findOneBy(array('id'=>$id));
        if(is_null($post)) {
            throw new InvalidPostException("Cannot find post with id=$id");
        }
        return $post;
    }

    function getPostsBy($criteria, $order, $limit, $offset)
    {
        $users = $this->postRepository->findBy($criteria, $order, $limit, $offset);
        return $users;
    }

    function getPostsByUserId($userId, $limit, $offset, $count)
    {
        $result = $this->postRepository->getPosts($userId, $limit, $offset, $count);
        return $result;
    }

    function addPost($user, Post $post) {

        if(is_numeric($user)) {
            $user = $this->userService->getUser($user);
        }

        $post->setUser($user);
        $post->setCreatedAt(new \DateTime());
        $post->setUpdatedAt(new \DateTime());

        //content group association
        if(!is_null($post->getContentGroup())) {
            $contentGroup = $this->contentGroupService->getContentGroup($post->getContentGroup()->getId());
            $post->setContentGroup($contentGroup);
        }

        $post = $this->postRepository->save($post);
        return $post;
    }

    function savePostContent(Post $newPost, $postid) {
        if ($postid != $newPost->getId()) {
            throw new InvalidPostException("Payload and REST endpoint IDs do not match");
        }

        $post = $this->postRepository->find($postid);
        if(!is_null($post)) {
            $post->setUpdatedAt(new \DateTime());
            $post->setTitle($newPost->getTitle());
            $post->setContent($newPost->getContent());
            $post->setOpenGraphData($newPost->getOpenGraphData());
            $post = $this->postRepository->save($post);
        }
        else {
            throw new InvalidPostException("Post Not Found");
        }
        return $post;
    }

    function deletePost($postId) {
        $this->postRepository->deletePost($postId);
    }

    function deleteByContentGroupId($contentGroupId) {
        $this->postRepository->deleteByContentGroupId($contentGroupId);
    }

    function getUserFeed($userId, $limit, $offset, $count, array $criteria=array()) {
        $result = array();
        $user = $this->userService->getUser($userId);
        if(!is_null($user)) {
            $result = $this->postRepository->getUserFeed($user, $limit, $offset, $count, $criteria);
        }
        return $result;
    }

    function getContentGroupFeed($contentGroupId, $limit, $offset, $count, array $criteria=array()) {
        $result = array();
        $contentGroup = $this->contentGroupService->getContentGroup($contentGroupId);
        if(!is_null($contentGroup)) {
            $result = $this->postRepository->getContentGroupFeed($contentGroup, $limit, $offset, $count, $criteria);
        }
        return $result;
    }

    function getTopPosters($topPostersNumber) {
        $result = $this->postRepository->getTopPosters($topPostersNumber);
        return $result;
    }

    function getSearchResults($searchTerm, $limit, $offset, array $criteria=array()) {

        //main queries
        $query = new Query();
        $boolQuery = new Query\BoolQuery();

        //base text search
        $baseQuery = new Query\SimpleQueryString($searchTerm, array('_all'));
        $boolQuery->addMust($baseQuery);

        //add criterias
        foreach($criteria as $criteriaField=>$criteriaValue) {
            if($criteriaField == CriteriaField::POST_CREATED_AT) {

                //transform to DateTime object
                $createdAtDateTime = new \DateTime();
                $createdAtDateTime->setTimestamp($criteriaValue/1000);

                //create query
                $createdAtQuery = new Query\Range("createdAt", array("gte"=>DateUtil::formatES($createdAtDateTime)));
                $boolQuery->addMust($createdAtQuery);
            }
        }

        $query->setQuery($boolQuery);
        $query->setSize($limit);
        $query->setFrom($offset);

        $finder = $this->container->get('fos_elastica.finder.app.post');
        $results = $finder->find($query);

        return $results;
    }

    function logImpression($userId, Post $post) {
        $user = $this->userService->getUser($userId);
        $impression = New Impression();

        $impression->setPost($post);
        $impression->setUser($user);
        $impression = $this->impressionRepository->save($impression);

        return $impression;
    }
}