<?php


namespace EngagementBundle\Service;


use EngagementBundle\Entity\ContentGroupFollowing;
use EngagementBundle\Repository\ContentGroupFollowingRepository;

class ContentGroupFollowingService
{

    private $userService;
    private $contentGroupService;
    private $contentGroupFollowingRepository;

    public function __construct(ContentGroupFollowingRepository $contentGroupFollowingRepository, UserService $userService, ContentGroupService $contentGroupService)
    {
        $this->contentGroupFollowingRepository = $contentGroupFollowingRepository;
        $this->userService = $userService;
        $this->contentGroupService = $contentGroupService;
    }

    function findFollowingsByUserId($userId, $orderBy, $limit, $offset, $count)
    {
        $result = $this->contentGroupFollowingRepository->findFollowingsByUserId($userId, $orderBy, $limit, $offset, $count);
        return $result;
    }

    function addFollow($userId, $contentGroupIdToFollow)
    {
        $contentGroupFollowing = $this->contentGroupFollowingRepository->findByUserAndContentGroupToFollow($userId, $contentGroupIdToFollow);
        $user = $this->userService->getUser($userId);
        $contentGroupToFollow = $this->contentGroupService->getContentGroup($contentGroupIdToFollow);

        if(is_null($contentGroupFollowing) && !is_null($user) && !is_null($contentGroupToFollow)) {

            $contentGroupFollowing = new ContentGroupFollowing();
            $contentGroupFollowing->setCreatedAt(new \DateTime());
            $contentGroupFollowing->setUser($user);
            $contentGroupFollowing->setFollowingContentGroup($contentGroupToFollow);

            $this->contentGroupFollowingRepository->save($contentGroupFollowing);
        }
    }

    function removeFollow($userId, $contentGroupIdToUnfollow) {
        $this->contentGroupFollowingRepository->removeFollow($userId, $contentGroupIdToUnfollow);
    }
}