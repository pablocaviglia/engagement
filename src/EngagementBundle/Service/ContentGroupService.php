<?php


namespace EngagementBundle\Service;

use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Exception\ErrorCode;
use EngagementBundle\Exception\InvalidContentGroupException;
use EngagementBundle\Repository\ContentGroupFollowingRepository;
use EngagementBundle\Repository\ContentGroupRepository;
use EngagementBundle\Repository\ImpressionRepository;
use EngagementBundle\Repository\PostRepository;

class ContentGroupService
{

    private $contentGroupRepository;
    private $userService;
    private $postRepository;
    private $impressionRepository;
    private $contentGroupFollowingRepository;

    public function __construct(
        ContentGroupRepository $contentGroupRepository,
        UserService $userService,
        PostRepository $postRepository,
        ImpressionRepository $impressionRepository,
        ContentGroupFollowingRepository $contentGroupFollowingRepository)
    {
        $this->contentGroupRepository = $contentGroupRepository;
        $this->userService = $userService;
        $this->postRepository = $postRepository;
        $this->impressionRepository = $impressionRepository;
        $this->contentGroupFollowingRepository = $contentGroupFollowingRepository;
    }

    function getContentGroup($id) {
        $contentGroup = $this->contentGroupRepository->findOneBy(array('id'=>$id));
        if(is_null($contentGroup)) {
            throw new InvalidContentGroupException("Cannot find content group with id=$id", ErrorCode::CONTENT_GROUP_INVALID_ID);
        }
        return $contentGroup;
    }

    function getContentGroups($limit, $offset, $count, $criteria)
    {
        $result = $this->contentGroupRepository->getContentGroups($limit, $offset, $count, $criteria);
        return $result;
    }

    function createContentGroup(ContentGroup $contentGroup) {

        $user = $this->userService->getUser($contentGroup->getCreatedBy()->getId());
        if(!is_null($user)) {

            //verifies if content
            //group name already exists
            $contentGroupName = $contentGroup->getName();
            if(!$this->contentGroupRepository->existsContentGroupName($contentGroupName)) {
                $contentGroup->setCreatedBy($user);
                $contentGroup->setCreatedAt(new \DateTime());
                $contentGroup->setUpdatedAt(new \DateTime());
                //save
                $contentGroup = $this->contentGroupRepository->saveContentGroup($contentGroup);
            }
            else {
                throw new InvalidContentGroupException("A group with the name \"$contentGroupName\" already exists.", ErrorCode::CONTENT_GROUP_INVALID_NAME);
            }
        }

        return $contentGroup;
    }

    function modifyContentGroup(ContentGroup $newContentGroup) {

        $contentGroupId = $newContentGroup->getId();
        $contentGroup = $this->contentGroupRepository->find($contentGroupId);
        if(!is_null($contentGroup)) {
            //validate name existence
            $contentGroupName = $newContentGroup->getName();
            if(!$this->contentGroupRepository->existsContentGroupName($contentGroupName, $contentGroupId)) {

                //set new values
                $contentGroup->setName($newContentGroup->getName());
                $contentGroup->setDescription($newContentGroup->getDescription());
                $contentGroup->setPhotoUrl($newContentGroup->getPhotoUrl());
                $contentGroup->setUpdatedAt(new \DateTime());

                //save
                $contentGroup = $this->contentGroupRepository->saveContentGroup($contentGroup);
            }
            else {
                throw new InvalidContentGroupException("A group with the name \"$contentGroupName\" already exists.", ErrorCode::CONTENT_GROUP_INVALID_NAME);
            }
        }
        else {
            throw new InvalidContentGroupException("Cannot find content group with id=$contentGroupId", ErrorCode::CONTENT_GROUP_INVALID_ID);
        }

        return $contentGroup;
    }

    function deleteContentGroup($contentGroupId) {

        //delete group followings
        $this->contentGroupFollowingRepository->deleteByContentGroupId($contentGroupId);

        //delete post impressions
        $this->impressionRepository->deleteByContentGroupId($contentGroupId);

        //delete posts associated to group
        $this->postRepository->deleteByContentGroupId($contentGroupId);

        //delete group
        $this->contentGroupRepository->deleteContentGroup($contentGroupId);
    }
}