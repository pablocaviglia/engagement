<?php

namespace EngagementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EngagementBundle\Entity\ContentGroup;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $contentGroup1 = new ContentGroup();
        $contentGroup1->setName("The first group");
        $contentGroup1->setDescription("Description of the group");
        $contentGroup1->setCreatedBy($this->getReference('post-user'));

        $manager->persist($contentGroup1);
        $manager->flush();

        $contentGroup2 = new ContentGroup();
        $contentGroup2->setName("The second group");
        $contentGroup2->setDescription("Description of the second group");
        $contentGroup2->setCreatedBy($this->getReference('second-user'));

        $manager->persist($contentGroup2);
        $manager->flush();

    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 3;
    }
}