<?php

namespace EngagementBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EngagementBundle\Entity\Following;
use EngagementBundle\Entity\User;

class LoadUserData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $user = new User();
        $user->setFirst("Roger");
        $user->setLast("Clyne");
        $manager->persist($user);
        $manager->flush();

        $this->addReference('post-user', $user);

        $user2 = new User();
        $user2->setFirst("Joe");
        $user2->setLast("Hendrickson");
        $manager->persist($user2);
        $manager->flush();

        $this->addReference('second-user', $user2);

        $user3 = new User();
        $user3->setFirst("Fancy");
        $user3->setLast("Loving");
        $manager->persist($user3);
        $manager->flush();

        $this->addReference('third-user', $user3);

        $following = new Following();
        $following->setUser($user);
        $following->setFollowingUser($user2);
        $manager->persist($following);
        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}