<?php

namespace EngagementBundle\DataFixtures\ORM;

use DateTimeZone;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use EngagementBundle\Entity\Post;

class LoadPostData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $json = "{
                      \"ogTitle\":\"Ask Well: Should You Filter Your Water?\",
                      \"ogType\":\"article\",
                      \"ogUrl\":\"http://well.blogs.nytimes.com/2015/12/31/ask-well-should-you-filter-your-water/\",
                      \"ogSiteName\":\"Well\",
                      \"ogDescription\":\"Although municipal tap water is regulated and most utilities stay within legally mandated limits on certain contaminants, some of the limits may be too lenient, a research analyst says.\",
                      \"ogImage\":{
                         \"url\":\"http://graphics8.nytimes.com/images/2015/12/22/health/well_water/well_water-facebookJumbo.jpg\"
                      }
                  }";

        $currentDate = new \DateTime();

        $post = new Post();
        $post->setTitle('First post');
        $post->setContent('This is a seed post from the fixture and associated with seed user lemons!');
        $post->setOpenGraphData($json);
        $post->setUser($this->getReference('post-user'));
        $post->setCreatedAt($currentDate);
        $post->setUpdatedAt($currentDate);

        $manager->persist($post);

        $post2 = new Post();
        $post2->setTitle('Post from another user');
        $post2->setContent('This is a seed post made from another user. I should only see this if I am following that user');
        $post2->setOpenGraphData($json);
        $post2->setUser($this->getReference('second-user'));
        $post2->setCreatedAt($currentDate);
        $post2->setUpdatedAt($currentDate);

        $post3 = new Post();
        $post3->setTitle('Post from another user (2nd post)');
        $post3->setContent('This is a seed post made from another user. I should only see this if I am following that user (2nd post)');
        $post3->setOpenGraphData($json);
        $post3->setUser($this->getReference('second-user'));
        $post3->setCreatedAt($currentDate);
        $post3->setUpdatedAt($currentDate);

        $post4 = new Post();
        $post4->setTitle('Post from user that is not followed');
        $post4->setContent('This is the body of that post');
        $post4->setOpenGraphData($json);
        $post4->setUser($this->getReference('third-user'));
        $post4->setCreatedAt($currentDate);
        $post4->setUpdatedAt($currentDate);

        $manager->persist($post2);
        $manager->persist($post3);
        $manager->persist($post4);
        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 2;
    }
}