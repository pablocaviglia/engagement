<?php

namespace EngagementBundle\Repository;

use Doctrine\ORM\Query\ResultSetMappingBuilder;
use EngagementBundle\Constant\CriteriaField;
use EngagementBundle\Entity\ContentGroupFollowing;
use EngagementBundle\Entity\Post;
use EngagementBundle\Entity\User;
use EngagementBundle\Entity\Following;

class PostRepository extends \Doctrine\ORM\EntityRepository
{

    function save(Post $post) {
        $em = $this->getEntityManager();
        $em->persist($post);
        $em->flush($post);
        return $post;
    }

    public function deletePost($postId)
    {
        $em = $this->_em;
        $post = $em->getPartialReference(Post::class, array('id' => $postId));
        $em->remove($post);
        $em->flush($post);
    }

    public function deleteByContentGroupId($contentGroupId)
    {
        $em = $this->_em;
        $query = $em->createQuery("DELETE FROM " . Post::class . " p WHERE p.contentGroup = :contentGroupId");
        $query->setParameter('contentGroupId', $contentGroupId);
        $query->execute();
    }

    public function getPosts($userId, $limit, $offset, $count)
    {

        $selectClause = $count ? "count(p)" : "p";

        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();
        $queryBuilder->select($selectClause)
            ->from(Post::class, 'p')
            ->where('p.user = ?1');

        if(!$count) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $queryBuilder->setParameter(1, $userId);
        $queryBuilder->orderBy("p.id", "DESC");

        $query  = $queryBuilder->getQuery();

        //execute query
        if($count) {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0][1];
            }
            else {
                $result = 0;
            }
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }

    public function getTopPosters($topPostersNumber)
    {

        //main query
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->select("u, count(u) as qntyPosts")
            ->from(Post::class, 'p')
            ->from(User::class, 'u')
            ->where('p.user=u')
            ->groupBy("p.user")
            ->orderBy("qntyPosts", "DESC")
            ->setMaxResults($topPostersNumber);

        //execute query
        $query  = $queryBuilder->getQuery();
        $result = $query->getResult();

        return $result;
    }

    public function getUserFeed($user, $limit, $offset, $count, array $criteria=array())
    {

        $selectClause = $count ? "count(*) as count" : "*";

        $createdAtCondition = "";

        //add criterias
        foreach($criteria as $criteriaField=>$criteriaValue) {
            if($criteriaField == CriteriaField::POST_CREATED_AT) {
                $createdAtDateTime = new \DateTime();
                $createdAtDateTime->setTimestamp($criteriaValue/1000);
                $createdAtCondition = " and `created_at` >= :createdAt";
            }
        }

        $sqlQuery =
        "
            select $selectClause FROM
            (

              SELECT p0_.*
                FROM post p0_
                WHERE p0_.contentgroup_id IN (SELECT c0_.content_group_id FROM content_group_following c0_ WHERE c0_.user_id = :userId)
                $createdAtCondition

            UNION

              SELECT p1_.*
                FROM post p1_
                WHERE p1_.user_id IN (SELECT f1_.following_user_id FROM following f1_ WHERE f1_.user_id = :userId)
                AND p1_.contentgroup_id IS NULL
                $createdAtCondition

            UNION

              SELECT p2_.*
                FROM post p2_
                WHERE p2_.user_id = :userId $createdAtCondition

            )
            as p
            ORDER BY p.id DESC
        ";

        if(!$count) {
            $sqlQuery .= " LIMIT :offset,:limit";
        }

        //limit control
        if(!is_numeric($limit) || $limit > 100) {
            $limit = 20;
        }

        //offset control
        if(!is_numeric($offset)) {
            $offset = 0;
        }

        //force numeric casting
        $offset = (integer) $offset;
        $limit = (integer) $limit;
        $userId = $user->getId();

        //map resultset with Post class
        $rsm = new ResultSetMappingBuilder($this->_em);

        if(!$count) {
            $rsm->addRootEntityFromClassMetadata(Post::class, 'p');
        }
        else {
            $rsm->addScalarResult('count','count');
        }

        //create native query and set paramaters
        $query = $this->_em->createNativeQuery($sqlQuery, $rsm);
        $query->setParameter('userId', $userId);

        if(!$count) {
            $query->setParameter('offset', $offset);
            $query->setParameter('limit', $limit);
        }

        if($createdAtCondition != '') {
            $query->setParameter('createdAt', $createdAtDateTime, \Doctrine\DBAL\Types\Type::DATETIME);
        }

        //get execution result
        if(!$count) {
            $result = $query->getResult();
        }
        else {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0]['count'];
            }
            else {
                $result = 0;
            }
        }

        return $result;

    }

    public function getContentGroupFeed($contentGroup, $limit, $offset, $count, array $criteria=array())
    {
        $selectClause = $count ? "count(p)" : "p";

        //main query
        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->select($selectClause)
            ->from(Post::class, 'p')
            ->where('p.contentGroup = ?1');

        //add criterias
        foreach($criteria as $criteriaField=>$criteriaValue) {
            if($criteriaField == CriteriaField::POST_CREATED_AT) {
                $createdAtDateTime = new \DateTime();
                $createdAtDateTime->setTimestamp($criteriaValue/1000);
                $queryBuilder->andWhere("p.createdAt > :createdAt")->setParameter("createdAt", $createdAtDateTime);
            }
        }

        $queryBuilder->setMaxResults($limit);
        $queryBuilder->setFirstResult($offset);
        $queryBuilder->setParameter(1, $contentGroup->getId());
        $queryBuilder->orderBy("p.id", "DESC");

        //execute query
        $query  = $queryBuilder->getQuery();

        if($count) {
            $result = $query->getSingleScalarResult();
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }
}