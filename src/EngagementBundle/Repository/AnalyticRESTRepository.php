<?php


namespace EngagementBundle\Repository;


use EngagementBundle\Entity\Analytic\RESTAnalytic;

class AnalyticRESTRepository extends \Doctrine\ORM\EntityRepository
{

    function save(RESTAnalytic $analytic) {
        $em = $this->getEntityManager();
        $em->persist($analytic);
        $em->flush();
    }
}