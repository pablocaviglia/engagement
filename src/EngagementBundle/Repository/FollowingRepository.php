<?php

namespace EngagementBundle\Repository;
use Doctrine\ORM\Query;
use EngagementBundle\Entity\Following;

class FollowingRepository extends \Doctrine\ORM\EntityRepository
{

    function save(Following $following) {
        $em = $this->getEntityManager();
        $em->persist($following);
        $em->flush();
    }

    function findByUserAndUserToFollow($userId, $userIdToFollow) {
        $following = $this->findOneBy(array("user" => $userId, "followingUser" => $userIdToFollow));
        return $following;
    }

    function findFollowingsByUserId($userId, array $orderBy, $limit, $offset, $count) {

        $queryBuilder = $this->_em->createQueryBuilder();

        $selectClause = $count ? "count(Following)" : "Following, u, fu";

        $queryBuilder->select($selectClause)
            ->from('EngagementBundle\Entity\Following', 'Following')
            ->where('Following.user = ?1')
            ->leftJoin("Following.user", "u")
            ->leftJoin("Following.followingUser", "fu");

        //set order by
        foreach($orderBy as $key=>$value) {
            $queryBuilder->orderBy($key, $value);
        }

        //param
        $queryBuilder->setParameter(1, $userId);

        $queryBuilder->setMaxResults($limit);
        $queryBuilder->setFirstResult($offset);
        $query = $queryBuilder->getQuery();

        //execute query
        if($count) {
            $result = $query->getSingleScalarResult();
        }
        else {
            $result = $query->getResult();
        }

        return $result;

    }

    function findFollowersByUserId($userId, array $orderBy, $limit, $offset, $count) {

        $queryBuilder = $this->_em->createQueryBuilder();

        $selectClause = $count ? "count(Following)" : "Following, u, fu";

        $queryBuilder->select($selectClause)
            ->from('EngagementBundle\Entity\Following', 'Following')
            ->where('Following.followingUser = ?1')
            ->leftJoin("Following.user", "u")
            ->leftJoin("Following.followingUser", "fu");

        //set order by
        foreach($orderBy as $key=>$value) {
            $queryBuilder->orderBy($key, $value);
        }

        //param
        $queryBuilder->setParameter(1, $userId);

        $queryBuilder->setMaxResults($limit);
        $queryBuilder->setFirstResult($offset);
        $query = $queryBuilder->getQuery();

        //execute query
        //execute query
        if($count) {
            $result = $query->getSingleScalarResult();
        }
        else {
            $result = $query->getResult();
        }
        return $result;

    }

    function removeFollow($userId, $userIdToUnfollow) {
        $em = $this->getEntityManager();
        $following = $this->findOneBy(array("user" => $userId, "followingUser" => $userIdToUnfollow));
        if(!is_null($following)) {
            $em->remove($following);
            $em->flush();
        }
    }
}
