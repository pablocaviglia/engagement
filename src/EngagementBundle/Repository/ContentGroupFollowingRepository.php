<?php


namespace EngagementBundle\Repository;


use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Entity\ContentGroupFollowing;

class ContentGroupFollowingRepository extends \Doctrine\ORM\EntityRepository
{

    function save(ContentGroupFollowing $contentGroupFollowing) {
        $em = $this->getEntityManager();
        $em->persist($contentGroupFollowing);
        $em->flush();
    }

    public function deleteByContentGroupId($contentGroupId)
    {
        $em = $this->_em;
        $query = $em->createQuery("DELETE FROM " . ContentGroupFollowing::class . " cgf WHERE cgf.followingContentGroup = :contentGroupId");
        $query->setParameter('contentGroupId', $contentGroupId);
        $query->execute();
    }

    function findFollowingsByUserId($userId, array $orderBy, $limit, $offset, $count) {

        $selectClause = $count ? "count(ContentGroupFollowing)" : "ContentGroupFollowing, u, cg";

        $queryBuilder = $this->_em->createQueryBuilder();
        $queryBuilder->select($selectClause)
            ->from('EngagementBundle\Entity\ContentGroupFollowing', 'ContentGroupFollowing')
            ->where('ContentGroupFollowing.user = ?1')
            ->leftJoin("ContentGroupFollowing.user", "u")
            ->leftJoin("ContentGroupFollowing.followingContentGroup", "cg");

        //set order by
        foreach($orderBy as $key=>$value) {
            $queryBuilder->orderBy($key, $value);
        }

        //param
        $queryBuilder->setParameter(1, $userId);

        if(!$count) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query = $queryBuilder->getQuery();

        //execute query
        if($count) {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0][1];
            }
            else {
                $result = 0;
            }
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }

    function findByUserAndContentGroupToFollow($userId, $contentGroupIdToFollow) {
        $following = $this->findOneBy(array("user" => $userId, "followingContentGroup" => $contentGroupIdToFollow));
        return $following;
    }

    function removeFollow($userId, $contentGroupIdToUnfollow) {
        $em = $this->getEntityManager();
        $following = $this->findOneBy(array("user" => $userId, "followingContentGroup" => $contentGroupIdToUnfollow));
        if(!is_null($following)) {
            $em->remove($following);
            $em->flush();
        }
    }
}