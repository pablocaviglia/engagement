<?php

namespace EngagementBundle\Repository;

use EngagementBundle\Entity\User;

class UserRepository extends \Doctrine\ORM\EntityRepository
{

    public function createUser(User $user) {
        $em = $this->_em;
        $em->persist($user);
        $em->flush($user);
        return $user;
    }

    public function getUsersToFollow($user, $limit, $offset, $count)
    {

        //not in subquery
        $subQueryBuilder = $this->_em->createQueryBuilder();
        $subQueryBuilder->select('IDENTITY(f.followingUser)')
            ->from('EngagementBundle\Entity\Following', 'f')
            ->where('f.user = ?1');

        $selectClause = $count ? "count(mm)" : "mm";

        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();
        $queryBuilder->select($selectClause)
            ->from('EngagementBundle\Entity\User', 'mm')
            ->where($queryBuilder->expr()->notIn('mm.id', $subQueryBuilder->getDQL()))
            ->andWhere('mm.id <> ?2');

        if(!$count) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $queryBuilder->setParameter(1, $user->getId());
        $queryBuilder->setParameter(2, $user->getId());

        $query  = $queryBuilder->getQuery();

        //execute query
        if($count) {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0][1];
            }
            else {
                $result = 0;
            }
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }

    public function getUsers($limit, $offset, $count)
    {

        $selectClause = $count ? "count(u)" : "u";

        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();
        $queryBuilder->select($selectClause)
            ->from(User::class, 'u');

        if(!$count) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query  = $queryBuilder->getQuery();

        //execute query
        if($count) {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0][1];
            }
            else {
                $result = 0;
            }
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }

    public function getUserFollowings($user, $limit, $offset, $count)
    {
        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();

        $selectClause = $count ? "count(u)" : "u";

        $queryBuilder->select($selectClause)
            ->from('EngagementBundle\Entity\User', 'u')
            ->from('EngagementBundle\Entity\Following', 'f')
            ->where('f.user = ?1')
            ->andWhere('f.followingUser = u');

        $queryBuilder->setParameter(1, $user->getId());

        if(!$count) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query  = $queryBuilder->getQuery();

        if($count) {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0][1];
            }
            else {
                $result = 0;
            }
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }

    public function getUserFollowers($user, $limit, $offset, $count)
    {

        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();

        $selectClause = $count ? "count(u)" : "u";

        $queryBuilder->select($selectClause)
            ->from('EngagementBundle\Entity\User', 'u')
            ->from('EngagementBundle\Entity\Following', 'f')
            ->where('f.followingUser = ?1')
            ->andWhere('f.user = u');

        $queryBuilder->setParameter(1, $user->getId());

        if(!$count) {
            $queryBuilder->setMaxResults($limit);
            $queryBuilder->setFirstResult($offset);
        }

        $query  = $queryBuilder->getQuery();

        if($count) {
            $result = $query->getResult();
            if(sizeof($result) > 0) {
                $result = $result[0][1];
            }
            else {
                $result = 0;
            }
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }
}