<?php

namespace EngagementBundle\Repository;

use EngagementBundle\Constant\CriteriaField;
use EngagementBundle\Entity\ContentGroup;

class ContentGroupRepository extends \Doctrine\ORM\EntityRepository
{

    public function saveContentGroup(ContentGroup $contentGroup) {
        $em = $this->_em;
        $em->persist($contentGroup);
        $em->flush($contentGroup);
        return $contentGroup;
    }

    public function getContentGroups($limit, $offset, $count, $criteria)
    {

        $selectClause = $count ? "count(cg)" : "cg";

        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();
        $queryBuilder->select($selectClause)
            ->from(ContentGroup::class, 'cg');

        //add criterias
        foreach($criteria as $criteriaField=>$criteriaValue) {

            if($criteriaField == CriteriaField::CONTENT_GROUP_CREATED_BY) {
                $queryBuilder->andWhere("cg.createdBy = :createdBy")->setParameter("createdBy", $criteriaValue);
            }

            if($criteriaField == CriteriaField::CONTENT_GROUP_NAME_LIKE) {
                $queryBuilder->andWhere("cg.name like :nameLike")->setParameter("nameLike", "%$criteriaValue%");
            }
        }

        $queryBuilder->setMaxResults($limit);
        $queryBuilder->setFirstResult($offset);

        $query  = $queryBuilder->getQuery();

        //execute query
        if($count) {
            $result = $query->getSingleScalarResult();
        }
        else {
            $result = $query->getResult();
        }

        return $result;
    }

    public function existsContentGroupName($name, $idException=null)
    {
        //main query
        $queryBuilder  = $this->_em->createQueryBuilder();
        $queryBuilder->select("count(cg)")
            ->from(ContentGroup::class, 'cg')
            ->where("cg.name = :name")->setParameter("name", $name);

        if(!is_null($idException)) {
            $queryBuilder->andWhere("cg.id != :idException")->setParameter("idException", $idException);
        }

        $query = $queryBuilder->getQuery();

        //execute query
        $result = $query->getSingleScalarResult();

        return $result != 0;
    }

    public function deleteContentGroup($contentGroupId)
    {
        $em = $this->_em;
        $post = $em->getPartialReference(ContentGroup::class, array('id' => $contentGroupId));
        $em->remove($post);
        $em->flush($post);
    }
}