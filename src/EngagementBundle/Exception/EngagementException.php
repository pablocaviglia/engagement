<?php


namespace EngagementBundle\Exception;


use Exception;

class EngagementException extends Exception
{

    public function __construct($message, $code, array $extras = array(), Exception $previous = null) {

        $messageJson = array('type'=>'managed','message'=>utf8_encode($message), 'error_code'=>$code);
        foreach($extras as $extraKey=>$extraValue) {
            $messageJson[utf8_encode($extraKey)] = utf8_encode($extraValue);
        }

        parent::__construct(json_encode($messageJson), $code, $previous);
    }

    public function __toString() {
        return __CLASS__ . ": [{$this->getCode()}]: {$this->getMessage()}\n";
    }
}