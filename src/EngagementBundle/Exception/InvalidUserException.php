<?php


namespace EngagementBundle\Exception;


class InvalidUserException extends EngagementException
{
    public function __construct($message, array $extras = array(), Exception $previous = null) {
        parent::__construct($message, ErrorCode::USER_INVALID_ID, $extras, $previous);
    }
}