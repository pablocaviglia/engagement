<?php


namespace EngagementBundle\Exception;


class ErrorCode
{

    //user
    const USER_INVALID_ID = 10000;

    //post
    const POST_INVALID_ID = 40000;

    //content group
    const CONTENT_GROUP_INVALID_ID = 20000;
    const CONTENT_GROUP_INVALID_NAME = 20001;

}