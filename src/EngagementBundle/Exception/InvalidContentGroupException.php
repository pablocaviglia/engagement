<?php


namespace EngagementBundle\Exception;


class InvalidContentGroupException extends EngagementException
{

    public function __construct($message, $errorCode, array $extras = array(), Exception $previous = null) {
        parent::__construct($message, $errorCode, $extras, $previous);
    }
}