<?php


namespace EngagementBundle\Exception;


class InvalidPostException extends EngagementException
{

    public function __construct($message, array $extras = array(), Exception $previous = null) {
        parent::__construct($message, ErrorCode::POST_INVALID_ID, $extras, $previous);
    }
}