<?php


namespace EngagementBundle\Exception;


use EngagementBundle\Exception\EngagementException;
use FOS\RestBundle\Util\ExceptionWrapper;
use FOS\RestBundle\View\ExceptionWrapperHandlerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;

class FOSExceptionHandler implements ExceptionWrapperHandlerInterface
{

    /**
     * @param array $data
     *
     * @return ExceptionWrapper
     */
    public function wrap($data)
    {
        $exception = $data['exception'];

        try {
            $messageJson = json_decode($data['exception']->getMessage());
            $managedException = $messageJson->type == 'managed';
        }
        catch(\Exception $e) {
            $managedException = false;
        }

        $newException = array(
            'success' => false,
            'managed' => $managedException,
            'exception' => array(
                'exception_class' => $exception->getClass(),
                'status_text' => $data['status_text']
            )
        );

        if($managedException) {
            foreach($messageJson as $messageKey=>$messageValue) {
                $newException['exception'][$messageKey] = $messageValue;
            }
        }
        else {
            $newException['message'] = $data['message'];
        }

        return $newException;
    }
}
