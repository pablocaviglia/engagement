<?php

namespace EngagementBundle\Controller;

use EngagementBundle\Constant\CriteriaField;
use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\ContentGroupDataMapper;
use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\VO\V1\ContentGroupVO;
use EngagementBundle\VO\V1\CountVO;
use EngagementBundle\VO\V1\ResultStateVO;
use FOS\RestBundle\Controller\Annotations\Route;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;


class ContentGroupController extends BaseController
{

    const MAX_RESULTS_PER_PAGE = 20;

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns all content groups",
     *  output="array<EngagementBundle\VO\V1\ContentGroupVO>",
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "default"="0"
     *      }
     *  }
     * )
     *
     * @Route(path="/api/v1/content_groups")
     * @Method("GET")
     *
     * @return Response
     */
    public function getGroups(Request $request)
    {

        //request params
        $limit = $request->query->get('limit');
        $offset = $request->query->get('offset');
        $count = $request->query->get('count');

        //limits
        $limit = $limit > ContentGroupController::MAX_RESULTS_PER_PAGE || $limit <= 0 ? ContentGroupController::MAX_RESULTS_PER_PAGE : $limit;

        //get users
        $contentGroupService = parent::getContentGroupService();
        $result  = $contentGroupService->getContentGroups($limit, $offset, $count, array());

        if($count) {
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $contentGroupsVO = $this->getContentGroupDataMapper()->mapListFullEntityToVO_V1($result);
            $response = $this->buildJsonResponse($contentGroupsVO);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns all content groups count",
     *  output="EngagementBundle\VO\V1\CountVO"
     * )
     *
     * @Route(path="/api/v1/content_groups/count")
     * @Method("GET")
     *
     * @return Response
     */
    public function getGroupsCount()
    {
        $contentGroupService = parent::getContentGroupService();
        $result  = $contentGroupService->getContentGroups(1, 0, true, array());
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns content groups filtering by 'search' parameter through group name field",
     *  output="array<EngagementBundle\VO\V1\ContentGroupVO>",
     *  parameters={
     *      {
     *          "name"="search",
     *          "description"="group name to search for",
     *          "required"="false",
     *          "dataType"="string",
     *          "requirement"="\d+",
     *          "default"="20"
     *      }
     *  }
     * )
     *
     * @Route(path="/api/v1/content_group/query")
     * @Method("GET")
     *
     * @return Response
     */
    public function getGroupsByQuery(Request $request)
    {

        //request params
        $search = $request->query->get('search');

        $criteria = array();
        if(!is_null($search)) {
            $criteria[CriteriaField::CONTENT_GROUP_NAME_LIKE] = $search;
        }

        //get users
        $contentGroupService = parent::getContentGroupService();
        $result  = $contentGroupService->getContentGroups(10, 0, false, $criteria);

        $contentGroupsVO = $this->getContentGroupDataMapper()->mapListBaseEntityToVO_V1($result);
        $response = $this->buildJsonResponse($contentGroupsVO);

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns a content group instance",
     *  output="EngagementBundle\VO\V1\ContentGroupVO",
     *  requirements={
     *      {
     *          "name"="contentGroupId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the content group"
     *      }
     *  }
     * )
     * @Route("/api/v1/content_group/{contentGroupId}", requirements={"contentGroupId" = "\d+"})
     * @Method("GET")
     */
    public function getContentGroup($contentGroupId)
    {

        $contentGroup = $this->getContentGroupService()->getContentGroup($contentGroupId);
        $contentGroupVO = $this->getContentGroupDataMapper()->mapFullEntityToVO_V1($contentGroup);

        $response = $this->buildJsonResponse($contentGroupVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns the posts feed of a content group",
     *  output="array<EngagementBundle\VO\V1\PostVO>",
     *  requirements={
     *      {
     *          "name"="contentGroupId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the content group"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "default"="0"
     *      },
     *      {
     *          "name"="created_at",
     *          "description"="unix timestamp to indicate since datetime filter",
     *          "dataType"="long",
     *          "requirement"="\d+",
     *          "default"=""
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/content_group/{contentGroupId}/feed", requirements={"contentGroupId" = "\d+"})
     * @Method("GET")
     */
    public function showContentGroupFeed(Request $request, $contentGroupId)
    {
        //request params
        $limit = $request->query->get('limit');
        $offset = $request->query->get('offset');
        $count = $request->query->get('count');
        $createdAt = $request->query->get('created_at');

        $criteria = array();
        if(!is_null($createdAt)) {
            $criteria[CriteriaField::POST_CREATED_AT] = $createdAt;
        }

        //limits
        $limit = $limit > ContentGroupController::MAX_RESULTS_PER_PAGE || $limit <= 0 ? ContentGroupController::MAX_RESULTS_PER_PAGE : $limit;

        $result = parent::getPostService()->getContentGroupFeed($contentGroupId, $limit, $offset, $count, $criteria);
        if($count) {
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $postsVO = $this->getPostDataMapper()->mapListFullEntityToVO_V1($result);
            $response = $this->buildJsonResponse($postsVO);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns the posts feed count of a content group",
     *  output="EngagementBundle\VO\V1\CountVO",
     *  requirements={
     *      {
     *          "name"="contentGroupId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the content group"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="created_at",
     *          "description"="unix timestamp to indicate since datetime filter",
     *          "dataType"="long",
     *          "requirement"="\d+",
     *          "default"=""
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/content_group/{contentGroupId}/feed/count", requirements={"contentGroupId" = "\d+"})
     * @Method("GET")
     */
    public function showContentGroupFeedCount(Request $request, $contentGroupId)
    {
        //request params
        $createdAt = $request->query->get('created_at');

        $criteria = array();
        if(!is_null($createdAt)) {
            $criteria[CriteriaField::POST_CREATED_AT] = $createdAt;
        }

        $result = parent::getPostService()->getContentGroupFeed($contentGroupId, 1, 0, true, $criteria);
        $response = $this->buildJsonResponse(new CountVO($result));

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Creates a content group",
     *  output="EngagementBundle\VO\V1\ContentGroupVO",
     *  requirements={
     *      {
     *          "name"="contentGroup",
     *          "dataType"="EngagementBundle\VO\V1\ContentGroupVO",
     *          "description"="json on 'Content' field ====> "
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/content_group")
     * @Method("POST")
     */
    public function createContentGroup(Request $request)
    {
        $content = $request->getContent();
        $contentGroupVO = parent::mapJSONToModel($content, ContentGroupVO::class);

        //create a content group
        $contentGroup = new ContentGroup();
        $this->getContentGroupDataMapper()->mapVO_V1ToEntity($contentGroupVO, $contentGroup);

        $contentGroupService = $this->getContentGroupService();
        $contentGroup = $contentGroupService->createContentGroup($contentGroup);

        //get saved content group and returns
        $contentGroupVO = $this->getContentGroupDataMapper()->mapFullEntityToVO_V1($contentGroup, $this->getSerializer());
        $response = $this->buildJsonResponse($contentGroupVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Modifies a content group",
     *  output="EngagementBundle\VO\V1\ResultStateVO",
     *  requirements={
     *      {
     *          "name"="contentGroupId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the content group"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/content_group/{contentGroupId}", requirements={"contentGroupId" = "\d+"})
     * @Method("PUT")
     */
    public function modifyContentGroup(Request $request, $contentGroupId)
    {
        $content = $request->getContent();
        $contentGroupVO = parent::mapJSONToModel($content, ContentGroupVO::class);

        //create a content group
        $contentGroup = new ContentGroup();
        $contentGroup->setId($contentGroupId);
        $this->getContentGroupDataMapper()->mapVO_V1ToEntity($contentGroupVO, $contentGroup);

        $contentGroupService = $this->getContentGroupService();
        $contentGroup = $contentGroupService->modifyContentGroup($contentGroup);

        //get saved content group and returns
        $contentGroupVO = $this->getContentGroupDataMapper()->mapFullEntityToVO_V1($contentGroup, $this->getSerializer());
        $response = $this->buildJsonResponse($contentGroupVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Deletes a content group",
     *  output="EngagementBundle\VO\V1\ResultStateVO",
     *  requirements={
     *      {
     *          "name"="contentGroupId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the content group"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/content_group/{contentGroupId}", requirements={"contentGroupId" = "\d+"})
     * @Method("DELETE")
     */
    public function deleteContentGroup($contentGroupId)
    {
        $this->getContentGroupService()->deleteContentGroup($contentGroupId);
        $result = new ResultStateVO(true);
        $response = $this->buildJsonResponse($result);
        return $response;
    }

    function getMaxResultsPerPage()
    {
        return ContentGroupController::MAX_RESULTS_PER_PAGE;
    }
}