<?php


namespace EngagementBundle\Controller;


use EngagementBundle\DataMapper\DataMapperUtil;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class BaseController extends FOSRestController
{

    const PARAM_NAME_LIMIT = 'limit';
    const PARAM_NAME_OFFSET = 'offset';
    const PARAM_NAME_COUNT = 'count';

    function getUserDataMapper() {
        return $this->get("engagement.user_mapper");
    }

    function getFollowingDataMapper() {
        return $this->get("engagement.following_mapper");
    }

    function getFollowerDataMapper() {
        return $this->get("engagement.follower_mapper");
    }

    function getPostDataMapper() {
        return $this->get("engagement.post_mapper");
    }

    function getContentGroupDataMapper() {
        return $this->get("engagement.content_group__mapper");
    }

    function getUserService() {
        return $this->get('engagement.user_service');
    }

    function getPostService() {
        return $this->get('engagement.post_service');
    }

    function getFollowingService() {
        return $this->get('engagement.following_service');
    }

    function getContentGroupFollowingService() {
        return $this->get('engagement.content_group_following_service');
    }

    function getContentGroupService() {
        return $this->get('engagement.content_group_service');
    }

    function mapModelToJson($instance) {
        $serializer = $this->getSerializer();
        $response = DataMapperUtil::mapModelToJSON($serializer, $instance);
        return $response;
    }

    function mapJsonToModel($json, $modelClass) {
        $serializer = $this->getSerializer();
        $response = DataMapperUtil::mapJSONToModel($json, $serializer, $modelClass);
        return $response;
    }

    function buildJsonResponse($json) {
        $response = new Response($this->mapModelToJson($json));
        return $response;
    }

    function getSerializer() {
        return $this->get('serializer');
    }

    function setHeaderCount($count, Response $response) {
        $response->headers->set('X-Total-Count', $count);
    }

    function setHeaderPagination($count, Response $response) {

        $limit = $this->getLimit();
        $offset = $this->getOffset();

        //check if the offset is valid
        //in relation to the limit
        $offsetModulus = $offset % $limit;

        if($offsetModulus == 0) {

            $firstPageLink = "";
            $previousPageLink = "";
            $nextPageLink = "";
            $lastPageLink = "";

            //this logic allows test to work
            if(isset($_SERVER['HTTP_HOST']) && isset($_SERVER['PHP_SELFT'])) {
                $basePath = "http://".$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
            }
            else {
                $basePath = '';
            }

            $currentPage = $offset / $limit;
            $maxPages = floor($count / $limit);

            if($currentPage > 0) {

                //evaluate 'first page' link creation
                $firstPageLink =
                    $basePath
                    ."?".BaseController::PARAM_NAME_LIMIT."=".$limit
                    ."&".BaseController::PARAM_NAME_OFFSET."=0"
                    ."&".$this->filterQueryParameters(array(BaseController::PARAM_NAME_LIMIT, BaseController::PARAM_NAME_OFFSET));

                //evaluate 'previous page' link creation
                $previousPageLink =
                    $basePath
                    ."?".BaseController::PARAM_NAME_LIMIT."=".$limit
                    ."&".BaseController::PARAM_NAME_OFFSET."=".($offset-$limit)
                    ."&".$this->filterQueryParameters(array(BaseController::PARAM_NAME_LIMIT, BaseController::PARAM_NAME_OFFSET));

            }

            //evaluate 'last page' link creation
            if($currentPage < $maxPages) {

                //evaluate 'last page' link creation
                $lastPageLink =
                    $basePath
                    ."?".BaseController::PARAM_NAME_LIMIT."=".$limit
                    ."&".BaseController::PARAM_NAME_OFFSET."=".($limit*$maxPages)
                    ."&".$this->filterQueryParameters(array(BaseController::PARAM_NAME_LIMIT, BaseController::PARAM_NAME_OFFSET));

                //evaluate 'next page' link creation
                $nextPageLink =
                    $basePath
                    ."?".BaseController::PARAM_NAME_LIMIT."=".$limit
                    ."&".BaseController::PARAM_NAME_OFFSET."=".($offset+$limit)
                    ."&".$this->filterQueryParameters(array(BaseController::PARAM_NAME_LIMIT, BaseController::PARAM_NAME_OFFSET));

            }

            $headerContent = empty($previousPageLink) && empty($nextPageLink) && empty($firstPageLink) && empty($lastPageLink) ? null : '';

            if(!is_null($headerContent)) {
                $existsLink = false;
                if(!empty($previousPageLink)) {
                    $existsLink = true;
                    $headerContent .= "<$previousPageLink>; rel=\"prev\"";
                }
                if(!empty($nextPageLink)) {
                    $headerContent .= $existsLink ? ',' : '';
                    $existsLink = true;
                    $headerContent .= "<$nextPageLink>; rel=\"next\"";
                }
                if(!empty($firstPageLink)) {
                    $headerContent .= $existsLink ? ',' : '';
                    $existsLink = true;
                    $headerContent .= "<$firstPageLink>; rel=\"first\"";
                }
                if(!empty($lastPageLink)) {
                    $headerContent .= $existsLink ? ',' : '';
                    $headerContent .= "<$lastPageLink>; rel=\"last\"";
                }
            }

            //set header
            $response->headers->set('Link', $headerContent);
        }
    }

    function filterQueryParameters(array $toFilterParams=null) {

        if(is_null($toFilterParams)) {
            $toFilterParams = array();
        }

        //filtered query string
        $queryStrModified = "";

        //tokenize the request query params
        $receivedParams = explode("&", $_SERVER['QUERY_STRING']);
        foreach ($receivedParams as $currentReceivedParam) {

            $filtered = false;

            //filter params
            foreach($toFilterParams as $currentFilterParam) {
                $explodedParams = explode("=", $currentReceivedParam);
                if(sizeof($explodedParams) > 0) {
                    $paramNameToken = trim($explodedParams[0]);
                    if($currentFilterParam == $paramNameToken) {
                        $filtered = true;
                        break;
                    }
                }
            }

            //if the param wasn't filtered add
            //it to the new query string
            if(!$filtered) {
                if(!empty($queryStrModified)) {
                    $queryStrModified .= "&";
                }
                $queryStrModified .= $currentReceivedParam;
            }
        }

        return $queryStrModified;
    }

    function getLimit() {
        $maxResultsPerPage = $this->getMaxResultsPerPage();
        $limit = $this->getRequest()->query->get(BaseController::PARAM_NAME_LIMIT);
        $limit = $limit > $maxResultsPerPage || $limit <= 0 ? $maxResultsPerPage : $limit;
        return $limit;
    }

    function getOffset() {
        $offset = $this->getRequest()->query->get(BaseController::PARAM_NAME_OFFSET);
        if(empty($offset)) {
            $offset = 0;
        }
        return $offset;
    }

    function getCount() {
        return $this->getRequest()->query->get(BaseController::PARAM_NAME_COUNT);
    }

    function getRequest() {
        return Request::createFromGlobals();
    }

    abstract function getMaxResultsPerPage();

}