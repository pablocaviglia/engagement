<?php

namespace EngagementBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends BaseController
{
    /**
     * @Route("/")
     * @Route("{id}/groups")
     * @Route("{id}/following")
     * @Route("{id}/followers")
     * @Route("{id}/posts")
     * @Route("/groups/{id}")
     * @Route("/groups")
     * @Route("/search")
     * @Route("/headlines")
     */
    public function indexAction()
    {
        return $this->render('EngagementBundle:Default:index.html.twig');
    }

    function getMaxResultsPerPage()
    {
        return 0;
    }
}
