<?php

namespace EngagementBundle\Controller;


use EngagementBundle\Constant\CriteriaField;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\Request;
use EngagementBundle\VO\V1\PostVO;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

class SearchController extends BaseController
{
    /**
     * @ApiDoc(
     *  description="Returns Posts based on search criteria",
     *  section="posts",
     *  requirements={
     *      {
     *          "name"="search",
     *          "dataType"="string",
     *          "requirement"="\s+",
     *          "required"="true",
     *          "description"="search string"
     *      }
     *  },
     *  filters={
     *     {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="10"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="created_at",
     *          "description"="unix timestamp to indicate since datetime filter",
     *          "dataType"="long",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"=""
     *      }
     *  }
     * )
     * @Route("/api/v1/search/")
     * @Method("GET")
     */
    public function searchAction(Request $request)
    {
        $searchTerm = $request->query->get('search');
        $limit = $request->query->get('limit');
        $offset = $request->query->get('offset');
        $createdAt = $request->query->get('created_at');

        $criteria = array();
        if(!is_null($createdAt)) {
            $criteria[CriteriaField::POST_CREATED_AT] = $createdAt;
        }

        $postService = parent::getPostService();
        $posts = $postService->getSearchResults($searchTerm, $limit, $offset, $criteria);
        $postsVO = $this->getPostDataMapper()->mapListFullEntityToVO_V1($posts);
        $response = $this->buildJsonResponse($postsVO);
        return $response;
    }

    function getMaxResultsPerPage()
    {
        return 0;
    }
}