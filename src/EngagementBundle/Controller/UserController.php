<?php

namespace EngagementBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use EngagementBundle\Constant\CriteriaField;
use EngagementBundle\DataMapper\DataMapperUtil;
use EngagementBundle\DataMapper\FollowerDataMapper;
use EngagementBundle\DataMapper\FollowingDataMapper;
use EngagementBundle\DataMapper\PostDataMapper;
use EngagementBundle\DataMapper\UserDataMapper;
use EngagementBundle\Entity\Following;
use EngagementBundle\Exception\EngagementException;
use EngagementBundle\VO\V1\CountVO;
use EngagementBundle\VO\V1\PostVO;
use EngagementBundle\VO\V1\UserVO;
use Exception;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;
use FOS\RestBundle\Request\ParamFetcher;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\Form\Exception\RuntimeException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use EngagementBundle\Entity\User;
use EngagementBundle\Entity\Post;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

/**
 * API endpoints related to users
 */
class UserController extends BaseController
{

    const MAX_RESULTS_PER_PAGE = 20;

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns all users",
     *  output="array<EngagementBundle\VO\V1\UserVO>",
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      }
     *  }
     * )
     *
     * @Route(path="/api/v1/users")
     * @Method("GET")
     *
     * @return Response
     */
    public function showUsers()
    {

        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();

        //get users
        $userService = parent::getUserService();

        if($count) {
            $result  = $userService->findBy($limit, $offset, true);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result  = $userService->findBy($limit, $offset, false);
            $usersVO = $this->getUserDataMapper()->mapListBaseEntityToVO_V1($result);
            $response = $this->buildJsonResponse($usersVO);

            $count = $userService->findBy($limit, $offset, true);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns all users count",
     *  output="EngagementBundle\VO\V1\CountVO"
     * )
     *
     * @Route(path="/api/v1/users/count")
     * @Method("GET")
     *
     * @return Response
     */
    public function showUsersCount()
    {
        $userService = parent::getUserService();
        $result  = $userService->findBy(1, 0, true);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns an User instance",
     *  output="EngagementBundle\VO\V1\UserVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "description"="id of the user",
     *          "required"="true"
     *      }
     *  }
     * )
     * @Route("/api/v1/user/{userId}", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUser($userId)
    {

        $user = $this->getUserService()->getUser($userId);
        $userVO = $this->getUserDataMapper()->mapBaseEntityToVO_V1($user);

        //posts qnty
        $postsQnty = $this->getPostService()->getPostsByUserId($userId, 5, 0, true);
        $userVO->setPostsQnty($postsQnty);

        //follows qnty
        $followingsQnty = $this->getFollowingService()->findFollowingsByUserId($userId, array(), 5, 0, true);
        $userVO->setFollowingsQnty($followingsQnty);

        //followers qnty
        $followersQnty = $this->getFollowingService()->findFollowersByUserId($userId, array(), 5, 0, true);
        $userVO->setFollowersQnty($followersQnty);

        $response = $this->buildJsonResponse($userVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns possible users to follow",
     *  output="array<EngagementBundle\VO\V1\UserVO>",
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/who_to_follow", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUsersToFollow($userId)
    {
        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();

        $userService = parent::getUserService();

        if($count) {
            $result = $userService->getUsersToFollow($userId, $limit, $offset, true);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result = $userService->getUsersToFollow($userId, $limit, $offset, false);
            $usersVO = $this->getUserDataMapper()->mapListBaseEntityToVO_V1($result);
            $response = $this->buildJsonResponse($usersVO);

            $count = $userService->getUsersToFollow($userId, $limit, $offset, true);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns count of possible users to follow",
     *  output="EngagementBundle\VO\V1\CountVO"
     * )
     *
     * @Route("/api/v1/user/{userId}/who_to_follow/count", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUsersToFollowCount($userId)
    {
        $userService = parent::getUserService();
        $result = $userService->getUsersToFollow($userId, 1, 0, true);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Returns the posts feed of a user",
     *  output="array<EngagementBundle\VO\V1\PostVO>",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="created_at",
     *          "description"="unix timestamp to indicate since datetime filter",
     *          "dataType"="long",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"=""
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/feed", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserFeed($userId)
    {
        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();
        $createdAt = $this->getRequest()->query->get('created_at');

        $criteria = array();
        if(!is_null($createdAt)) {
            $criteria[CriteriaField::POST_CREATED_AT] = $createdAt;
        }

        if($count) {
            $result = parent::getPostService()->getUserFeed($userId, $limit, $offset, true, $criteria);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result = parent::getPostService()->getUserFeed($userId, $limit, $offset, false, $criteria);
            $postsVO = $this->getPostDataMapper()->mapListFullEntityToVO_V1($result);
            $response = $this->buildJsonResponse($postsVO);

            $count = parent::getPostService()->getUserFeed($userId, $limit, $offset, true, $criteria);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Returns the posts feed count of a user",
     *  output="EngagementBundle\VO\V1\CountVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="created_at",
     *          "description"="unix timestamp to indicate since datetime filter",
     *          "dataType"="long",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"=""
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/feed/count", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserFeedCount(Request $request, $userId)
    {
        //request params
        $createdAt = $request->query->get('created_at');

        $criteria = array();
        if(!is_null($createdAt)) {
            $criteria[CriteriaField::POST_CREATED_AT] = $createdAt;
        }

        $result = parent::getPostService()->getUserFeed($userId, 1, 0, true, $criteria);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Returns the posts made by a user",
     *  output="array<EngagementBundle\VO\V1\PostVO>",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/posts", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserPosts($userId)
    {
        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();

        if($count) {
            $result = parent::getPostService()->getPostsByUserId($userId, $limit, $offset, true);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result = parent::getPostService()->getPostsByUserId($userId, $limit, $offset, false);
            $postsVO = $this->getPostDataMapper()->mapListFullEntityToVO_V1($result);
            $response = $this->buildJsonResponse($postsVO);

            $count = parent::getPostService()->getPostsByUserId($userId, $limit, $offset, true);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Returns the posts count made by a user",
     *  output="EngagementBundle\VO\V1\CountVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/posts/count", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserPostsCount($userId)
    {
        $result = parent::getPostService()->getPostsByUserId($userId, 1, 0, true);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns the user's followings",
     *  output="array<EngagementBundle\VO\V1\UserVO>",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/following", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserFollowing($userId)
    {
        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();

        if($count) {
            $result = parent::getUserService()->getUserFollowings($userId, $limit, $offset, true);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result = parent::getUserService()->getUserFollowings($userId, $limit, $offset, false);
            $usersVO = $this->getUserDataMapper()->mapListBaseEntityToVO_V1($result);
            $response = $this->buildJsonResponse($usersVO);

            $count = $result = parent::getUserService()->getUserFollowings($userId, $limit, $offset, true);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);

        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns the user's followings count",
     *  output="EngagementBundle\VO\V1\CountVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/following/count", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserFollowingCount($userId)
    {
        $result = parent::getUserService()->getUserFollowings($userId, 1, 0, true);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns the user's followers",
     *  output="array<EngagementBundle\VO\V1\UserVO>",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      }
     *  }
     * )
     * @Route("/api/v1/user/{userId}/followers", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserFollowers($userId)
    {
        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();

        if($count) {
            $result = parent::getUserService()->getUserFollowers($userId, $limit, $offset, true);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result = parent::getUserService()->getUserFollowers($userId, $limit, $offset, false);
            $usersVO = $this->getUserDataMapper()->mapListBaseEntityToVO_V1($result);
            $response = $this->buildJsonResponse($usersVO);

            $count = parent::getUserService()->getUserFollowers($userId, $limit, $offset, true);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns the user's followers count",
     *  output="EngagementBundle\VO\V1\CountVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  }
     * )
     * @Route("/api/v1/user/{userId}/followers/count", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserFollowersCount($userId)
    {
        $result = parent::getUserService()->getUserFollowers($userId, 1, 0, true);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Follows a user",
     *  output="EngagementBundle\VO\V1\UserVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      },
     *      {
     *          "name"="userToFollowId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user to follow"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/following/{userToFollowId}", requirements={"userId" = "\d+", "userToFollowId" = "\d+"})
     * @Method("POST")
     */
    public function addFollow($userId, $userToFollowId)
    {
        $followingService = parent::getFollowingService();
        $followingService->addFollow($userId, $userToFollowId);

        //get saved user and returns
        $user = $this->getUserService()->getUser($userId);
        $userVO = $this->getUserDataMapper()->mapBaseEntityToVO_V1($user);
        $response = $this->buildJsonResponse($userVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Creates a user",
     *  output="EngagementBundle\VO\V1\UserVO",
     *  requirements={
     *      {
     *          "name"="user",
     *          "dataType"="EngagementBundle\VO\V1\UserVO",
     *          "required"="true",
     *          "description"="json on 'Content' field ====> "
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user")
     * @Method("POST")
     */
    public function createUser(Request $request)
    {
        $content = $request->getContent();
        $userVO = parent::mapJSONToModel($content, UserVO::class);

        //create a user object
        $user = new User();
        $this->getUserDataMapper()->mapVO_V1ToEntity($userVO, $user);

        $userService = $this->getUserService();
        $user = $userService->createUser($user);

        //get saved post and returns
        $userVO = $this->getUserDataMapper()->mapFullEntityToVO_V1($user, $this->getSerializer());
        $response = $this->buildJsonResponse($userVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Unfollows a user",
     *  output="EngagementBundle\VO\V1\UserVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      },
     *      {
     *          "name"="userToUnfollowId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user to unfollow"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/following/{userToUnfollowId}", requirements={"userToUnfollowId" = "\d+"})
     * @Method("DELETE")
     */
    public function removeFollow($userId, $userToUnfollowId)
    {

        $followingService = parent::getFollowingService();
        $followingService->removeFollow($userId, $userToUnfollowId);

        //get saved user and returns
        $user = $this->getUserService()->getUser($userId);
        $userVO = $this->getUserDataMapper()->mapBaseEntityToVO_V1($user);
        $response = $this->buildJsonResponse($userVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Creates a post",
     *  output="EngagementBundle\VO\V1\PostVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      },
     *      {
     *          "name"="post",
     *          "dataType"="EngagementBundle\VO\V1\PostVO",
     *          "required"="true",
     *          "description"="post json on 'Content' field ====> "
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/post", requirements={"userId" = "\d+"})
     * @Method("POST")
     */
    public function savePost(Request $request, $userId)
    {
        $content = $request->getContent();
        $postVO = parent::mapJSONToModel($content, PostVO::class);

        //create a post object
        $post = new Post();
        $this->getPostDataMapper()->mapVO_V1ToEntity($postVO, $post);

        $postService = $this->getPostService();
        $post = $postService->addPost($userId, $post);

        //get saved post and returns
        $postVO = $this->getPostDataMapper()->mapFullEntityToVO_V1($post, $this->getSerializer());
        $response = $this->buildJsonResponse($postVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Follows a content group",
     *  output="EngagementBundle\VO\V1\UserVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      },
     *      {
     *          "name"="contentGroupToFollowId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the content group to follow"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/content_group/follow/{contentGroupToFollowId}", requirements={"userId" = "\d+", "contentGroupToFollowId" = "\d+"})
     * @Method("POST")
     */
    public function addContentGroupFollow($userId, $contentGroupToFollowId)
    {
        $contentGroupFollowingService = parent::getContentGroupFollowingService();
        $contentGroupFollowingService->addFollow($userId, $contentGroupToFollowId);

        //get saved user and returns
        $user = $this->getUserService()->getUser($userId);
        $userVO = $this->getUserDataMapper()->mapBaseEntityToVO_V1($user);
        $response = $this->buildJsonResponse($userVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Unfollows a content group",
     *  output="EngagementBundle\VO\V1\UserVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      },
     *      {
     *          "name"="contentGroupToUnfollowId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user to unfollow"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/content_group/unfollow/{contentGroupToUnfollowId}", requirements={"contentGroupToUnfollowId" = "\d+"})
     * @Method("DELETE")
     */
    public function removeContentGroupFollow($userId, $contentGroupToUnfollowId)
    {

        $contentGroupFollowingService = parent::getContentGroupFollowingService();
        $contentGroupFollowingService->removeFollow($userId, $contentGroupToUnfollowId);

        //get saved user and returns
        $user = $this->getUserService()->getUser($userId);
        $userVO = $this->getUserDataMapper()->mapBaseEntityToVO_V1($user);
        $response = $this->buildJsonResponse($userVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns the user's content groups followings",
     *  output="array<EngagementBundle\VO\V1\ContentGroupVO>",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  },
     *  filters={
     *      {
     *          "name"="limit",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="20"
     *      },
     *      {
     *          "name"="offset",
     *          "description"="search pointer movement",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      },
     *      {
     *          "name"="count",
     *          "description"="indicates if the result must be the count or the data",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="false",
     *          "default"="0"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/content_group/following", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserContentGroupFollowing($userId)
    {
        //request params
        $limit = $this->getLimit();
        $offset = $this->getOffset();
        $count = $this->getCount();

        if($count) {
            $result = parent::getContentGroupFollowingService()->findFollowingsByUserId($userId, array(), $limit, $offset, true);
            $response = $this->buildJsonResponse(new CountVO($result));
        }
        else {
            $result = parent::getContentGroupFollowingService()->findFollowingsByUserId($userId, array(), $limit, $offset, false);
            $contentGroupArray = array();
            foreach($result as $contentGroupFollowing) {
                $contentGroupArray[] = $contentGroupFollowing->getFollowingContentGroup();
            }

            $contentGroupsVO = $this->getContentGroupDataMapper()->mapListBaseEntityToVO_V1($contentGroupArray);
            $response = $this->buildJsonResponse($contentGroupsVO);

            $count = parent::getContentGroupFollowingService()->findFollowingsByUserId($userId, array(), $limit, $offset, true);

            //set headers
            $this->setHeaderCount($count, $response);
            $this->setHeaderPagination($count, $response);
        }

        return $response;
    }

    /**
     * @ApiDoc(
     *  section="content groups",
     *  description="Returns the user's content groups followings count",
     *  output="EngagementBundle\VO\V1\CountVO",
     *  requirements={
     *      {
     *          "name"="userId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the user"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/user/{userId}/content_group/following/count", requirements={"userId" = "\d+"})
     * @Method("GET")
     */
    public function showUserContentGroupFollowingCount($userId)
    {
        $result = parent::getContentGroupFollowingService()->findFollowingsByUserId($userId, array(), 1, 0, true);
        $response = $this->buildJsonResponse(new CountVO($result));
        return $response;
    }

    function getMaxResultsPerPage()
    {
        return UserController::MAX_RESULTS_PER_PAGE;
    }
}