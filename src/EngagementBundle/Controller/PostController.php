<?php

namespace EngagementBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use EngagementBundle\Entity\Post;
use EngagementBundle\VO\V1\PostVO;
use EngagementBundle\VO\V1\ResultStateVO;
use EngagementBundle\VO\V1\TopPosterVO;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Tests\AppBundle\Controller\PostsControllerTest;

class PostController extends BaseController
{

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Update a post (title, content and open graph)",
     *  output="EngagementBundle\VO\V1\PostVO",
     *  requirements={
     *      {
     *          "name"="post",
     *          "dataType"="EngagementBundle\VO\V1\PostVO",
     *          "required"="true",
     *          "description"="post json on 'Content' field ====> "
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/post/{postid}")
     * @Method("PUT")
     */
    public function updatePost(Request $request, $postid)
    {
        $content = $request->getContent();
        $postVO = parent::mapJSONToModel($content, PostVO::class);

        //create a post object
        $post = new Post();
        $this->getPostDataMapper()->mapVO_V1ToEntity($postVO, $post);

        $postService = $this->getPostService();
        $post = $postService->savePostContent($post, $postid);

        //get saved post and returns
        $postVO = $this->getPostDataMapper()->mapFullEntityToVO_V1($post, $this->getSerializer());
        $response = $this->buildJsonResponse($postVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Deletes a post",
     *  output="EngagementBundle\VO\V1\ResultStateVO",
     *  requirements={
     *      {
     *          "name"="postId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/post/{postId}")
     * @Method("DELETE")
     */
    public function deletePost($postId)
    {
        $this->getPostService()->deletePost($postId);
        $result = new ResultStateVO(true);
        $response = $this->buildJsonResponse($result);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Get a post",
     *  output="EngagementBundle\VO\V1\PostVO",
     *  requirements={
     *      {
     *          "name"="postId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/post/{postId}")
     * @Method("GET")
     */
    public function getPost($postId)
    {
        $post = $this->getPostService()->getPost($postId);
        $postVO = $this->getPostDataMapper()->mapBaseEntityToVO_V1($post);
        $response = $this->buildJsonResponse($postVO);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="posts",
     *  description="Log and impression on a post",
     *  requirements={
     *      {
     *          "name"="postId",
     *          "dataType"="integer",
     *          "requirement"="\d+",
     *          "required"="true",
     *          "description"="id of the post"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/post/{postId}/impression")
     * @Method("POST")
     */
    public function logImpression(Request $request, $postId)
    {
        $post = $this->getPostService()->getPost($postId);
        $userId = $request->get('userId');

        $impression = $this->getPostService()->logImpression($userId, $post);

        $response = new Response();
        $response->setStatusCode(Response::HTTP_NO_CONTENT);
        return $response;
    }

    /**
     * @ApiDoc(
     *  section="users",
     *  description="Returns top posters",
     *  output="array<EngagementBundle\VO\V1\TopPosterVO>",
     *  requirements={
     *      {
     *          "name"="topPostersNumber",
     *          "description"="registries limit",
     *          "dataType"="integer",
     *          "required"="true",
     *          "requirement"="\d+",
     *          "default"="10"
     *      }
     *  }
     * )
     *
     * @Route("/api/v1/post/top_posters/{topPostersNumber}")
     * @Method("GET")
     */
    public function getTopPosters($topPostersNumber)
    {
        $result = $this->getPostService()->getTopPosters($topPostersNumber);
        $topPostersArray = array();
        foreach($result as $topPoster) {
            $topPostersArray[] = new TopPosterVO($this->getUserDataMapper(), $topPoster[0], $topPoster['qntyPosts']);
        }

        $response = $this->buildJsonResponse($topPostersArray);
        return $response;
    }

    function getMaxResultsPerPage()
    {
        return 0;
    }
}