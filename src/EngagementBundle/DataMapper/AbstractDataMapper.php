<?php


namespace EngagementBundle\DataMapper;


abstract class AbstractDataMapper
{

    protected $serializer;
    protected $userDataMapper;
    protected $postDataMapper;
    protected $followingDataMapper;
    protected $followerDataMapper;
    protected $contentGroupDataMapper;

    public function __construct($serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @return mixed
     */
    public function getUserDataMapper()
    {
        return $this->userDataMapper;
    }

    /**
     * @param mixed $userDataMapper
     */
    public function setUserDataMapper($userDataMapper)
    {
        $this->userDataMapper = $userDataMapper;
    }

    /**
     * @return mixed
     */
    public function getPostDataMapper()
    {
        return $this->postDataMapper;
    }

    /**
     * @param mixed $postDataMapper
     */
    public function setPostDataMapper($postDataMapper)
    {
        $this->postDataMapper = $postDataMapper;
    }

    /**
     * @return mixed
     */
    public function getFollowingDataMapper()
    {
        return $this->followingDataMapper;
    }

    /**
     * @param mixed $followingDataMapper
     */
    public function setFollowingDataMapper($followingDataMapper)
    {
        $this->followingDataMapper = $followingDataMapper;
    }

    /**
     * @return mixed
     */
    public function getFollowerDataMapper()
    {
        return $this->followerDataMapper;
    }

    /**
     * @param mixed $followerDataMapper
     */
    public function setFollowerDataMapper($followerDataMapper)
    {
        $this->followerDataMapper = $followerDataMapper;
    }

    public function getSerializer()
    {
        return $this->serializer;
    }

    /**
     * @return mixed
     */
    public function getContentGroupDataMapper()
    {
        return $this->contentGroupDataMapper;
    }

    /**
     * @param mixed $contentGroupDataMapper
     */
    public function setContentGroupDataMapper($contentGroupDataMapper)
    {
        $this->contentGroupDataMapper = $contentGroupDataMapper;
    }

    public function setSerializer($serializer)
    {
        $this->serializer = $serializer;
    }
}