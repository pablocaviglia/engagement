<?php


namespace EngagementBundle\DataMapper;


use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Entity\User;
use EngagementBundle\VO\V1\ContentGroupVO;

class ContentGroupDataMapper extends AbstractDataMapper
{

    function mapBaseEntityToVO_V1(ContentGroup &$entity) {
        $vo = new ContentGroupVO();
        $vo->setId($entity->getId());
        $vo->setCreatedAt($entity->getCreatedAt());
        $vo->setUpdatedAt($entity->getUpdatedAt());
        $vo->setName($entity->getName());
        $vo->setDescription($entity->getDescription());
        $vo->setPhotoUrl($entity->getPhotoUrl());
        return $vo;
    }

    function mapFullEntityToVO_V1(ContentGroup &$entity) {
        $vo = self::mapBaseEntityToVO_V1($entity);

        //created by
        if(!is_null($entity->getCreatedBy())) {
            $user = $entity->getCreatedBy();
            $userVO = $this->userDataMapper->mapBaseEntityToVO_V1($user);
            $vo->setCreatedBy($userVO);
        }

        return $vo;
    }

    function mapListBaseEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapBaseEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapListFullEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapFullEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapVO_V1ToEntity(ContentGroupVO &$vo, ContentGroup &$entity) {
        DataMapperUtil::setIfNotNull($vo, $entity, 'id', 'id');
        DataMapperUtil::setIfNotNull($vo, $entity, 'createdAt', 'createdAt');
        DataMapperUtil::setIfNotNull($vo, $entity, 'updatedAt', 'updatedAt');
        DataMapperUtil::setIfNotNull($vo, $entity, 'name', 'name');
        DataMapperUtil::setIfNotNull($vo, $entity, 'description', 'description');
        DataMapperUtil::setIfNotNull($vo, $entity, 'photoUrl', 'photoUrl');

        if(!is_null($vo->getCreatedBy())) {
            $createdByVO = $vo->getCreatedBy();
            $createdBy = new User();
            $this->userDataMapper->mapVO_V1ToEntity($createdByVO, $createdBy);
            $entity->setCreatedBy($createdBy);
        }
    }
}