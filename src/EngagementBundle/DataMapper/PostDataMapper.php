<?php

namespace EngagementBundle\DataMapper;


use EngagementBundle\Entity\ContentGroup;
use EngagementBundle\Entity\Post;
use EngagementBundle\Entity\User;
use EngagementBundle\VO\V1\OpenGraphDataVO;
use EngagementBundle\VO\V1\PostVO;
use Symfony\Component\Config\Definition\Exception\Exception;

class PostDataMapper extends AbstractDataMapper
{

    function mapBaseEntityToVO_V1(&$entity) {

        $vo = new PostVO();
        $vo->setId($entity->getId());
        $vo->setContent($entity->getContent());
        $vo->setCreatedAt($entity->getCreatedAt());
        $vo->setTitle($entity->getTitle());
        $vo->setUpdatedAt($entity->getUpdatedAt());
        $arrayVO[] = $vo;

        if(!is_null($entity->getOpenGraphData())) {
            try {
                $openGraphDataJson = $entity->getOpenGraphData();
                $openGraphDataVO = DataMapperUtil::mapJSONToModel($openGraphDataJson, $this->serializer, OpenGraphDataVO::class);
                $vo->setOpenGraphData($openGraphDataVO);
            }
            catch(\Exception $e) {
                $ogError = new OpenGraphDataVO();
                $ogError->setErrorParsing("Invalid json, cannot parse");
                $vo->setOpenGraphData($ogError);
            }
        }

        return $vo;
    }

    function mapFullEntityToVO_V1(&$entity) {

        $vo = self::mapBaseEntityToVO_V1($entity);

        //user
        $user = $entity->getUser();
        if(!is_null($user)) {
            $vo->setUser($this->userDataMapper->mapBaseEntityToVO_V1($user));
        }

        //content group
        $contentGroup = $entity->getContentGroup();
        if(!is_null($contentGroup)) {
            $vo->setContentGroup($this->contentGroupDataMapper->mapBaseEntityToVO_V1($contentGroup));
        }

        return $vo;
    }

    function mapListBaseEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapBaseEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapListFullEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapFullEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapVO_V1ToEntity(PostVO &$vo, Post &$entity) {

        DataMapperUtil::setIfNotNull($vo, $entity, 'id', 'id');
        DataMapperUtil::setIfNotNull($vo, $entity, 'title', 'title');
        DataMapperUtil::setIfNotNull($vo, $entity, 'content', 'content');
        DataMapperUtil::setIfNotNull($vo, $entity, 'createdAt', 'createdAt');
        DataMapperUtil::setIfNotNull($vo, $entity, 'updatedAt', 'updatedAt');

        if(!is_null($vo->getOpenGraphData())) {
            $openGraphDataJson = DataMapperUtil::mapModelToJSON($this->serializer, $vo->getOpenGraphData());
            $entity->setOpenGraphData($openGraphDataJson);
        }

        if(!is_null($vo->getUser())) {
            $user = new User();
            $userVO = $vo->getUser();
            $this->getUserDataMapper()->mapVO_V1ToEntity($userVO, $user);
            $entity->setUser($user);
        }

        if(!is_null($vo->getContentGroup())) {
            $contentGroup = new ContentGroup();
            $contentGroupVO = $vo->getContentGroup();
            $this->getContentGroupDataMapper()->mapVO_V1ToEntity($contentGroupVO, $contentGroup);
            $entity->setContentGroup($contentGroup);
        }
    }
}