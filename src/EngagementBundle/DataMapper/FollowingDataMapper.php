<?php


namespace EngagementBundle\DataMapper;


use EngagementBundle\Entity\Following;
use EngagementBundle\VO\V1\FollowingVO;

class FollowingDataMapper extends AbstractDataMapper
{

    function mapBaseEntityToVO_V1(Following &$entity) {
        $vo = new FollowingVO();
        $vo->setId($entity->getId());
        $vo->setCreatedAt($entity->getCreatedAt());
        return $vo;
    }

    function mapFullEntityToVO_V1(Following &$entity) {

        $vo = self::mapBaseEntityToVO_V1($entity);

        if(!is_null($entity->getFollowingUser()))
        {
            $user = $entity->getFollowingUser();
            $vo->setFollowingUser($this->userDataMapper->mapBaseEntityToVO_V1($user));
        }

        return $vo;
    }

    function mapListBaseEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapBaseEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapListFullEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapFullEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapVO_V1ToEntity(FollowingVO &$vo, Following &$entity) {
        DataMapperUtil::setIfNotNull($vo, $entity, 'createdAt', 'createdAt');
    }
}