<?php


namespace EngagementBundle\DataMapper;


class DataMapperUtil
{

    static function setIfNotNull(&$objectFrom, &$objectTo, $propertyNameFrom, $propertyNameTo) {

        $getterFrom = 'get'.$propertyNameFrom;
        $setterTo = 'set'.$propertyNameTo;

        if(!is_null($objectFrom->$getterFrom())) {
            $objectTo->$setterTo($objectFrom->$getterFrom());
        }
    }

    static function mapJSONToModel(&$json, $serializer, $modelClass) {
        return $serializer->deserialize($json, $modelClass, 'json');
    }

    static function mapModelToJSON($serializer, $modelInstance) {
        return $serializer->serialize($modelInstance, 'json');
    }
}