<?php

namespace EngagementBundle\DataMapper;
use EngagementBundle\Entity\User;
use EngagementBundle\VO\V1\UserVO;


class UserDataMapper extends AbstractDataMapper
{

    function mapBaseEntityToVO_V1(User &$entity)
    {
        $vo = new UserVO();
        $vo->setId( $entity->getId());
        $vo->setFirst($entity->getFirst());
        $vo->setLast($entity->getLast());
        $vo->setPhotoUrl($entity->getPhotoUrl());
        return $vo;
    }

    function mapFullEntityToVO_V1(User &$entity)
    {
        $vo = self::mapBaseEntityToVO_V1($entity);
        return $vo;
    }

    function mapListBaseEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapBaseEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapListFullEntityToVO_V1(&$entities) {
        $arrayVO = array();
        foreach($entities as $entity) {
            $arrayVO[] = self::mapFullEntityToVO_V1($entity);
        }
        return $arrayVO;
    }

    function mapVO_V1ToEntity(UserVO &$vo, User &$entity) {
        DataMapperUtil::setIfNotNull($vo, $entity, 'id', 'id');
        DataMapperUtil::setIfNotNull($vo, $entity, 'first', 'first');
        DataMapperUtil::setIfNotNull($vo, $entity, 'last', 'last');
        DataMapperUtil::setIfNotNull($vo, $entity, 'photoUrl', 'photoUrl');
    }
}