<?php
/**
 * This class is complementary of JMS serializer.
 * Because of an still unknown reason (bug), JMS deserialize
 * non-associative arrays to JSON using the index of the
 * elements as part of the deserialized result.
 * This is a fix to override this behavior.
 *
 * @author  Almog Baku
 *          almog.baku@gmail.com
 *          http://www.AlmogBaku.com
 *
 * 25/06/15 15:28
 */

namespace EngagementBundle\DataMapper\Fix;

use \JMS\Serializer\JsonSerializationVisitor as JsonSerializationVisitorBase;

class JsonSerializationVisitor extends JsonSerializationVisitorBase
{
    public function getResult()
    {
        if($this->getRoot() instanceof \ArrayObject) {
            $this->setRoot((array) $this->getRoot());
        }
        return parent::getResult();
    }
}