<?php


namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;
use JMS\Serializer\Annotation\Type;


class CountVO
{

    /**
     * @Type("integer")
     * @Since("v1")
     * @Until("v1")
     */
    var $count;

    /**
     * CountVO constructor.
     * @param $count
     */
    public function __construct($count)
    {
        $this->count = $count;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * @param mixed $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }
}