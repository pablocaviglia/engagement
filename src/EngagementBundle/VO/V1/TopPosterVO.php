<?php


namespace EngagementBundle\VO\V1;

use EngagementBundle\DataMapper\UserDataMapper;
use EngagementBundle\Entity\User;
use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class TopPosterVO
{

    /**
     * @Type("EngagementBundle\VO\V1\UserVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $user;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $qntyPosts;

    /**
     * TopPosterVO constructor.
     */
    public function __construct(UserDataMapper $dataMapper, User &$user, $qntyPosts)
    {
        $this->user= $dataMapper->mapBaseEntityToVO_V1($user);
        $this->qntyPosts = $qntyPosts;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getQntyPosts()
    {
        return $this->qntyPosts;
    }

    /**
     * @param mixed $qntyPosts
     */
    public function setQntyPosts($qntyPosts)
    {
        $this->qntyPosts = $qntyPosts;
    }
}