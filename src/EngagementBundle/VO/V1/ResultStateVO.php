<?php


namespace EngagementBundle\VO\V1;


use EngagementBundle\Constant\ResultState;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;
use JMS\Serializer\Annotation\Type;

class ResultStateVO
{

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $state;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $message;

    /**
     * ResultStateVO constructor.
     */
    public function __construct($ok, $message='')
    {
        if($ok) {
            $this->state = ResultState::OK;
        }
        else {
            $this->state = ResultState::ERROR;
        }
        $this->message = $message;
    }

    /**
     * @return mixed
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param mixed $state
     */
    public function setState($state)
    {
        $this->state = $state;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }
}