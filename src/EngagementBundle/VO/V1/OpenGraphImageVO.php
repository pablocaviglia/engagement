<?php

namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class OpenGraphImageVO
{

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
s     */
    private $url;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }
}