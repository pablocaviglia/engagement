<?php


namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class FollowingVO
{

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $id;

    /**
     * @Type("DateTime")
     * @Since("v1")
     * @Until("v1")
     */
    private $createdAt;

    /**
     * @Type("EngagementBundle\VO\V1\UserVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $followingUser;

    /**
     * @Type("EngagementBundle\VO\V1\UserVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $user;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getFollowingUser()
    {
        return $this->followingUser;
    }

    /**
     * @param mixed $followingUser
     */
    public function setFollowingUser($followingUser)
    {
        $this->followingUser = $followingUser;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
}