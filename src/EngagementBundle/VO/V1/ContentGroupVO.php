<?php


namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class ContentGroupVO
{

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $id;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $name;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $photoUrl;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $description;

    /**
     * @Type("EngagementBundle\VO\V1\UserVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $createdBy;

    /**
     * @Type("DateTime")
     * @Since("v1")
     * @Until("v1")
     */
    private $createdAt;

    /**
     * @Type("DateTime")
     * @Since("v1")
     * @Until("v1")
     */
    private $updatedAt;

    /**
     * @Type("array<EngagementBundle\VO\V1\PostVO>")
     * @Since("v1")
     * @Until("v1")
     */
    private $posts;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    /**
     * @param mixed $photoUrl
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts)
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }
}