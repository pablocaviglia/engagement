<?php


namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class FollowerVO
{

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $id;

    /**
     * @Type("DateTime")
     * @Since("v1")
     * @Until("v1")
     */
    private $createdAt;

    /**
     * @Type("EngagementBundle\VO\V1\UserVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $followerUser;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getFollowerUser()
    {
        return $this->followerUser;
    }

    /**
     * @param mixed $user
     */
    public function setFollowerUser($user)
    {
        $this->followerUser = $user;
    }
}