<?php


namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class PostVO
{

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $id;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $title;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $content;

    /**
     * @Type("EngagementBundle\VO\V1\OpenGraphDataVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $openGraphData;

    /**
     * @Type("DateTime")
     * @Since("v1")
     * @Until("v1")
     */
    private $createdAt;

    /**
     * @Type("DateTime")
     * @Since("v1")
     * @Until("v1")
     */
    private $updatedAt;

    /**
     * @Type("EngagementBundle\VO\V1\UserVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $user;

    /**
     * @Type("EngagementBundle\VO\V1\ContentGroupVO")
     * @Since("v1")
     * @Until("v1")
     */
    private $contentGroup;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return mixed
     */
    public function getOpenGraphData()
    {
        return $this->openGraphData;
    }

    /**
     * @param mixed $openGraphData
     */
    public function setOpenGraphData($openGraphData)
    {
        $this->openGraphData = $openGraphData;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getContentGroup()
    {
        return $this->contentGroup;
    }

    /**
     * @param mixed $contentGroup
     */
    public function setContentGroup($contentGroup)
    {
        $this->contentGroup = $contentGroup;
    }
}