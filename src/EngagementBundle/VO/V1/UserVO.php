<?php

namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\VirtualProperty;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;
use JMS\Serializer\Annotation\Exclude;

class UserVO
{
    /**
     *
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $id;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $first;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $last;

    /**
     * @Type("string")
     * @Since("v1")
     * @Until("v1")
     */
    private $photoUrl;

    /**
     * @Type("integer")
     * @Since("v1")
     * @Until("v1")
     */
    private $postsQnty;

    /**
     * @Type("integer")
     * @Since("v1")
     * @Until("v1")
     */
    private $followingsQnty;

    /**
     * @Type("integer")
     * @Since("v1")
     * @Until("v1")
     */
    private $followersQnty;

    /**
     * @Exclude
     */
    private $posts;

    /**
     * @Exclude
     */
    private $followings;

    /**
     * @Exclude
     */
    private $followers;


    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @VirtualProperty
     */
    public function getFullName()
    {
        return $this->first . " " . $this->last;
    }

    /**
     * @return mixed
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * @param mixed $first
     */
    public function setFirst($first)
    {
        $this->first = $first;
    }

    /**
     * @return mixed
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * @param mixed $last
     */
    public function setLast($last)
    {
        $this->last = $last;
    }

    /**
     * @return mixed
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    /**
     * @param mixed $photoUrl
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
    }

    /**
     * @return mixed
     */
    public function getPostsQnty()
    {
        return $this->postsQnty;
    }

    /**
     * @param mixed $postsQnty
     */
    public function setPostsQnty($postsQnty)
    {
        $this->postsQnty = $postsQnty;
    }

    /**
     * @return mixed
     */
    public function getFollowingsQnty()
    {
        return $this->followingsQnty;
    }

    /**
     * @param mixed $followingsQnty
     */
    public function setFollowingsQnty($followingsQnty)
    {
        $this->followingsQnty = $followingsQnty;
    }

    /**
     * @return mixed
     */
    public function getFollowersQnty()
    {
        return $this->followersQnty;
    }

    /**
     * @param mixed $followersQnty
     */
    public function setFollowersQnty($followersQnty)
    {
        $this->followersQnty = $followersQnty;
    }
}