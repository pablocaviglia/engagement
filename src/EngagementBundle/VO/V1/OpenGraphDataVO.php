<?php

namespace EngagementBundle\VO\V1;

use JMS\Serializer\Annotation\Type;
use JMS\Serializer\Annotation\SerializedName;
use JMS\Serializer\Annotation\Since;
use JMS\Serializer\Annotation\Until;

class OpenGraphDataVO
{

    /**
     * @Type("string")
     * @SerializedName("ogTitle")
     * @Since("v1")
     * @Until("v1")
     */
    private $ogTitle;

    /**
     * @Type("string")
     * @SerializedName("ogType")
     * @Since("v1")
     * @Until("v1")
     */
    private $ogType;

    /**
     * @Type("string")
     * @SerializedName("ogUrl")
     * @Since("v1")
     * @Until("v1")
     */
    private $ogUrl;

    /**
     * @Type("string")
     * @SerializedName("ogSiteName")
     * @Since("v1")
     * @Until("v1")
     */
    private $ogSiteName;

    /**
     * @Type("string")
     * @SerializedName("ogDescription")
     * @Since("v1")
     * @Until("v1")
     */
    private $ogDescription;

    /**
     * @Type("EngagementBundle\VO\V1\OpenGraphImageVO")
     * @SerializedName("ogImage")
     * @Since("v1")
     * @Until("v1")
     */
    private $ogImage;

    /**
     * @Type("string")
     * @SerializedName("errorParsing")
     * @Since("v1")
     * @Until("v1")
     */
    private $errorParsing;

    /**
     * @return mixed
     */
    public function getOgTitle()
    {
        return $this->ogTitle;
    }

    /**
     * @param mixed $ogTitle
     */
    public function setOgTitle($ogTitle)
    {
        $this->ogTitle = $ogTitle;
    }

    /**
     * @return mixed
     */
    public function getOgType()
    {
        return $this->ogType;
    }

    /**
     * @param mixed $ogType
     */
    public function setOgType($ogType)
    {
        $this->ogType = $ogType;
    }

    /**
     * @return mixed
     */
    public function getOgUrl()
    {
        return $this->ogUrl;
    }

    /**
     * @param mixed $ogUrl
     */
    public function setOgUrl($ogUrl)
    {
        $this->ogUrl = $ogUrl;
    }

    /**
     * @return mixed
     */
    public function getOgSiteName()
    {
        return $this->ogSiteName;
    }

    /**
     * @param mixed $ogSiteName
     */
    public function setOgSiteName($ogSiteName)
    {
        $this->ogSiteName = $ogSiteName;
    }

    /**
     * @return mixed
     */
    public function getOgDescription()
    {
        return $this->ogDescription;
    }

    /**
     * @param mixed $ogDescription
     */
    public function setOgDescription($ogDescription)
    {
        $this->ogDescription = $ogDescription;
    }

    /**
     * @return mixed
     */
    public function getOgImage()
    {
        return $this->ogImage;
    }

    /**
     * @param mixed $ogImage
     */
    public function setOgImage($ogImage)
    {
        $this->ogImage = $ogImage;
    }

    /**
     * @return mixed
     */
    public function getErrorParsing()
    {
        return $this->errorParsing;
    }

    /**
     * @param mixed $errorParsing
     */
    public function setErrorParsing($errorParsing)
    {
        $this->errorParsing = $errorParsing;
    }
}