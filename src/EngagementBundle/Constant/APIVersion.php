<?php

namespace EngagementBundle\Constant;


abstract class APIVersion
{
    const V1 = 'v1';
    const V2 = 'v2';
}