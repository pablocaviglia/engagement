<?php


namespace EngagementBundle\Constant;


class CriteriaField
{

    //post
    const POST_CREATED_AT = "CREATED_AT";


    //content group
    const CONTENT_GROUP_CREATED_BY = "CREATED_BY";
    const CONTENT_GROUP_NAME_LIKE = "NAME_LIKE";

}