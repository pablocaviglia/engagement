<?php


namespace EngagementBundle\Constant;


class ResultState
{

    const OK = 'ok';
    const ERROR = 'error';

}