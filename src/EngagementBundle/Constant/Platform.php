<?php


namespace EngagementBundle\Constant;


class Platform
{

    const ANDROID = 'android';
    const IOS = 'ios';
    const WEB = 'web';

}