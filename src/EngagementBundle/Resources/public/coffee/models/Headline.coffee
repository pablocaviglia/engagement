define (require) ->

  Backbone = require 'backbone'

  class Headline extends Backbone.Model

    url: ->
      ''

    initialize: (attributes, options) ->
      @options = if options then options else {}
      return
