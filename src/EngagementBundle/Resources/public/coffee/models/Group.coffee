define (require) ->

  App = require 'cs!coffee/App'
  Backbone = require 'backbone'

  class Group extends Backbone.Model

    url: ->
      if @has('id') # Edit/Delete
        "/api/v1/content_group/#{@get('id')}"
      else # Creation
        return '/api/v1/content_group'

    initialize: (attributes, options) ->
      @options = if options then options else {}
      return

    follow: ->
      if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
        @xhr.abort()
       # Return the xhr
      @xhr = $.ajax(
        type: 'POST'
        url: "/api/v1/user/#{App.user.get('id')}/content_group/follow/#{@get('id')}"
        success: =>
          App.user.groups.add(@) # Update the local groups collection
          App.user.groups.count()
      )

    unfollow: ->
      if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
        @xhr.abort()
       # Return the xhr
      @xhr = $.ajax(
        type: 'DELETE'
        url: "/api/v1/user/#{App.user.get('id')}/content_group/unfollow/#{@get('id')}"
        success: =>
          App.user.groups.remove(@) # Update the local groups collection
          App.user.groups.count()
      )
