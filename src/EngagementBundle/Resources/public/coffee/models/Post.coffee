define (require) ->

  Backbone = require 'backbone'
  App = require 'cs!coffee/App'

  class Post extends Backbone.Model

    url: ->
      if @has('id') # Edit/Delete
        return "/api/v1/post/#{@get('id')}"
      else # Creation
        return "/api/v1/user/#{@get('user').id}/post"

    initialize: (attributes, options) ->
      @options = if options then options else {}
      return

    sendImpression: ->
      $.ajax(
        type: 'POST'
        url: "/api/v1/post/#{@get('id')}/impression"
        data:
          userId: App.user.get('id')
        success: =>
          @trigger('impression')
      )
