define (require) ->

  Backbone = require 'backbone'

  class OpenGraph extends Backbone.Model

    url: ->
      "//uniphy-preview.herokuapp.com/og?url=#{encodeURIComponent(@get('url'))}"

    initialize: (attributes, options) ->
      @options = if options then options else {}
      return
