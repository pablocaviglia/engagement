define (require) ->

  Backbone = require 'backbone'
  App = require 'cs!coffee/App'

  class User extends Backbone.Model

    url: ->
      "/api/v1/user/#{@get('id')}"

    initialize: (attributes, options) ->
      @options = if options then options else {}
      return

    follow: ->
      if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
        @xhr.abort()
       # Return the xhr
      @xhr = $.ajax(
        type: 'POST'
        silent: true
        url: "/api/v1/user/#{App.user.get('id')}/following/#{@get('id')}"
        success: =>
          App.user.following.add(@) # Update the local following collection
          App.user.following.count()
      )

    unfollow: ->
      if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
        @xhr.abort()
       # Return the xhr
      @xhr = $.ajax(
        type: 'DELETE'
        url: "/api/v1/user/#{App.user.get('id')}/following/#{@get('id')}"
        success: =>
          App.user.following.remove(@) # Update the local following collection
          App.user.following.count()
      )
