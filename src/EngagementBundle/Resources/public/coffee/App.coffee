define (require) ->

  # $ = require 'jquery'
  _ = require 'underscore'
  Backbone = require 'backbone'
  Marionette = require 'marionette'

  require 'bootstrap-notify'

  # Create a new Marionette Application
  App = new Marionette.Application()

  # Global timeout of 10 seconds
  $.ajaxSetup(
    timeout: 10000
  )

  class Layout extends Marionette.LayoutView
    template: _.template '<div id="app"></div>'
    regions:
      appRegion: '#app'

  App.href = window.location.href

  App.layout = new Layout(el: 'body')

  App.layout.render()

  App.on 'start', ->
    Backbone.history.start({ pushState: true })
    try
      console.log('%c       __     __  __   __  __   ______   ____     __  __   __    __ \n      /\\_\\   /\\ \\/\\ \\ /\\ \\/\\ \\ /\\__  _\\ /\\  _`\\  /\\ \\/\\ \\ /\\ \\  /\\ \\\n    __\\/_/_  \\ \\ \\ \\ \\\\ \\ `\\\\ \\\\/_/\\ \\/ \\ \\ \\L\\ \\\\ \\ \\_\\ \\\\ `\\`\\\\/\'/\n   /\\_\\ /\\_\\  \\ \\ \\ \\ \\\\ \\ , ` \\  \\ \\ \\  \\ \\ ,__/ \\ \\  _  \\`\\ `\\ /\' \n __\\/_/_\\/_/_  \\ \\ \\_\\ \\\\ \\ \\`\\ \\  \\_\\ \\__\\ \\ \\/   \\ \\ \\ \\ \\ `\\ \\ \\ \n/\\_\\ /\\_\\ /\\_\\  \\ \\_____\\\\ \\_\\ \\_\\ /\\_____\\\\ \\_\\    \\ \\_\\ \\_\\  \\ \\_\\\n\\/_/ \\/_/ \\/_/   \\/_____/ \\/_/\\/_/ \\/_____/ \\/_/     \\/_/\\/_/   \\/_/', 'color: #df7d27;')
      console.log('%c ©2016 Uniphy Health, LLC. All rights reserved.', 'color: #000; font-size: 90%;')
    catch: (e) ->
      return
    return

  App.on 'unauthenticate', (->
    delete App.user
  ), @

  ### # Sets us up for notifications if we want to use them
  setTimeout (->
    $.notify {
      title: 'New Version Available'
      message: 'Refresh your browser to sign in to the latest version of the Engagement app.'
    },
      element: 'body'
      position: null
      type: 'default'
      allow_dismiss: true
      newest_on_top: false
      showProgressbar: false
      placement:
        from: 'bottom'
        align: 'right'
      offset: 20
      spacing: 10
      z_index: 1031
      delay: 95000
      timer: 1000
      mouse_over: null
      animate:
        enter: 'animated fadeInDown'
        exit: 'animated fadeOutUp'
      icon_type: 'class'
      url_target: '',
      template: '<div data-notify="container" class="col-md-3 alert alert-{0}" role="alert"><button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button><h4 data-notify="title">{1}</h4><span data-notify="message">{2}</span><div class="progress" data-notify="progressbar"><div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div></div><div class="clearfix"><a href="/" class="btn btn-warning btn-sm pull-right" target="{4}" data-notify="url">Refresh</a></div></div>'
    return
  ), 600
  ###

  return App
