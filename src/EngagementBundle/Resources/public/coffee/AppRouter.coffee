define (require) ->

  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'

  class AppRouter extends Marionette.AppRouter

    appRoutes:
      '(/)': 'index'
      ':id/following(/)': 'following'
      ':id/followers(/)': 'followers'
      ':id/groups(/)': 'groupsFollowing'
      ':id/posts(/)': 'posts'
      'groups(/)': 'groups'
      'groups(/):contentGroupId(/)': 'groupPosts'
      'search(/)?q=:q': 'search'
      'headlines': 'headlines'
      '*notFound': 'index'
      #'admin(/)': 'admin'
      #'admin/users(/)': 'adminUsers'
      #'admin/posts(/)': 'adminPosts'

    # Override native Backbone.js method to check for a logged in user
    execute: (callback, args) ->
      if not App.user
        @_login(callback, args)
        return false
      if callback
        callback.apply(@, args)
      return

    # Override native Backbone.js method to trigger an event even if not { trigger: true }
    navigate: (fragment, options) ->
      Backbone.history.navigate(fragment, options)
      @trigger('navigate')
      return @

    urlParameters: -> # Public
      a = window.location.search.substr(1).split('&')
      if a is ''
        return {}
      b = {}
      i = 0
      while i < a.length
        p = a[i].split('=', 2)
        if p.length is 1
          b[p[0]] = ''
        else
          b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, ' '))
        i++
      b

    initialize: ->

      @listenTo App, 'athenticate:done', (options) ->
        if options.callback # Specific Url requested
          options.callback.apply(@, options.args)
        else # No Url requested (User could have logged out and then logged back in)
          @controller.index()
      , @

      @listenTo App, 'unauthenticate', (->
        App.appRouter.navigate('/')
        require ['cs!coffee/views/Login'], (Login) ->
          App.layout.getRegion('appRegion').show new Login()
          return
        return
      ), @

      return

    controller: # Routes

      index: -> # Authenticated layout for now
        App.appRouter.navigate('/') # Reset the URL just in case the user came from search or some other bad URL
        require ['cs!coffee/views/Main', 'views/feed/FeedPage'], (Main, FeedPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new FeedPage())
          else
            App.layout.getRegion('appRegion').show new Main(
              module: new FeedPage()
            )
          return
        return

      groups: -> # Authenticated layout for now
        require ['cs!coffee/views/Main', 'views/groups/GroupsPage'], (Main, GroupsPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new GroupsPage())
          else
            App.layout.getRegion('appRegion').show new Main(
              module: new GroupsPage()
            )
          return
        return

      groupPosts: (contentGroupId) -> # Authenticated layout for now
        require ['cs!coffee/views/Main', 'views/groups/GroupPostsPage'], (Main, GroupPostsPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new GroupPostsPage( contentGroupId: contentGroupId ))
          else
            App.layout.getRegion('appRegion').show new Main(
              module: new GroupPostsPage( contentGroupId: contentGroupId )
            )
          return
        return

      following: (id) -> # Authenticated layout for now
        require ['cs!coffee/views/Main', 'views/user/UserPage', 'views/following/FollowingPage'], (Main, UserPage, FollowingPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
              userId: id
              route: 'following'
              module: new FollowingPage( userId: id )
            ))
          else
            App.layout.getRegion('appRegion').show(new Main(
              module: new UserPage(
                userId: id
                route: 'following'
                module: new FollowingPage( userId: id )
              )
            ))
          return
        return

      followers: (id) -> # Authenticated layout for now
        require ['cs!coffee/views/Main', 'views/user/UserPage', 'views/followers/FollowersPage'], (Main, UserPage, FollowersPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
              userId: id
              route: 'followers'
              module: new FollowersPage( userId: id )
            ))
          else
            App.layout.getRegion('appRegion').show(new Main(
              module: new UserPage(
                userId: id
                route: 'followers'
                module: new FollowersPage( userId: id )
              )
            ))
          return
        return

      groupsFollowing: (id) -> # Authenticated layout for now
        require ['cs!coffee/views/Main', 'views/user/UserPage', 'views/groups/GroupsFollowingPage'], (Main, UserPage, GroupsFollowingPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
              userId: id
              route: 'groupsFollowing'
              module: new GroupsFollowingPage( userId: id )
            ))
          else
            App.layout.getRegion('appRegion').show(new Main(
              module: new UserPage(
                userId: id
                route: 'groupsFollowing'
                module: new GroupsFollowingPage( userId: id )
              )
            ))
          return
        return

      posts: (id) -> # Authenticated layout for now
        require ['cs!coffee/views/Main', 'views/user/UserPage', 'views/posts/PostsPage'], (Main, UserPage, PostsPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
              userId: id
              route: 'posts'
              module: new PostsPage( userId: id )
            ))
          else
            App.layout.getRegion('appRegion').show(new Main(
              module: new UserPage(
                userId: id
                route: 'posts'
                module: new PostsPage( userId: id )
              )
            ))
          return
        return

      search: (q) ->
        q = decodeURIComponent(q)
        require ['cs!coffee/views/Main', 'views/search/SearchPage'], (Main, SearchPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new SearchPage(q: q))
          else
            App.layout.getRegion('appRegion').show new Main(
              q: q # Need q to render the input value
              module: new SearchPage(q: q) # Need q to render the search feed
            )
          return
        return

      headlines: (q) ->
        require ['cs!coffee/views/Main', 'views/headlines/HeadlinesPage'], (Main, HeadlinesPage) ->
          # if Main is already rendered
          if App.layout.getRegion('appRegion').currentView and App.layout.getRegion('appRegion').currentView.showPage
            App.layout.getRegion('appRegion').currentView.showPage(new HeadlinesPage())
          else
            App.layout.getRegion('appRegion').show new Main(
              module: new HeadlinesPage() # Need q to render the search feed
            )
          return
        return

      ###
      admin: -> # Authenticated layout for now
        require ['cs!coffee/views/Admin', 'views/admin/users/UsersPage'], (Admin, UsersPage) ->
          App.layout.getRegion('appRegion').show new Admin(
            module: new UsersPage()
            nav: 'users'
          )
          return
        return

      adminUsers: -> # Authenticated layout for now
        require ['cs!coffee/views/Admin', 'views/admin/users/UsersPage'], (Admin, UsersPage) ->
          App.layout.getRegion('appRegion').show new Admin(
            module: new UsersPage()
            nav: 'users'
          )
          return
        return

      adminPosts: -> # Authenticated layout for now
        require ['cs!coffee/views/Admin', 'views/admin/posts/PostsPage'], (Admin, PostsPage) ->
          App.layout.getRegion('appRegion').show new Admin(
            module: new PostsPage()
            nav: 'posts'
          )
          return
        return
      ###

    _login: (callback, args, name) ->
      require ['cs!coffee/views/Login'], (Login) ->
        App.layout.getRegion('appRegion').show new Login(
          callback: callback
          args: args
        )
        return
      return
