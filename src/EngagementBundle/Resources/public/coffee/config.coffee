require.config(
  paths:
    'underscore': ['//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min', '/assets/libs/backbone/underscore-min']
    'backbone': ['//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.2.3/backbone-min', '/assets/libs/backbone/backbone-min']
    'marionette': ['//cdnjs.cloudflare.com/ajax/libs/backbone.marionette/2.4.4/backbone.marionette.min', '/assets/libs/backbone/backbone.marionette.min']
    'backgrid': '/assets/libs/backbone/backgrid.min'
    'backgrid-paginator': '/assets/libs/backbone/backgrid-paginator'
    'backbone-paginator': '/assets/libs/backbone/backbone.paginator.min'
    'backbone.paginator': 'backbone-paginator'
    'jquery': ['//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min', '/assets/libs/jquery/jquery-1.11.3.min']
    'jquery-truncate': '/assets/libs/jquery/jquery.truncate'
    'jquery-textcomplete': '/assets/libs/jquery/jquery.textcomplete'
    'text': '/assets/libs/require/text'
    'handlebars': ['//cdnjs.cloudflare.com/ajax/libs/handlebars.js/4.0.5/handlebars.min', '/assets/libs/handlebars/handlebars-v4.0.5']
    'bootstrap-affix': '/assets/libs/bootstrap/affix'
    'bootstrap-alert': '/assets/libs/bootstrap/alert'
    'bootstrap-button': '/assets/libs/bootstrap/button'
    'bootstrap-carousel':'/assets/libs/bootstrap/carousel'
    'bootstrap-collapse': '/assets/libs/bootstrap/collapse'
    'bootstrap-dropdown': '/assets/libs/bootstrap/dropdown'
    'bootstrap-modal': '/assets/libs/bootstrap/modal'
    'bootstrap-popover': '/assets/libs/bootstrap/popover'
    'bootstrap-scrollspy': '/assets/libs/bootstrap/scrollspy'
    'bootstrap-tab': '/assets/libs/bootstrap/tab'
    'bootstrap-tooltip': '/assets/libs/bootstrap/tooltip'
    'bootstrap-transition': '/assets/libs/bootstrap/transition'
    'bootstrap-notify': '/assets/libs/bootstrap/bootstrap-notify.min'
    'bootstrap-typeahead': '/assets/libs/bootstrap/bootstrap3-typeahead.min'
    'moment': ['//cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.6/moment.min', '/assets/libs/moment/moment.min']
    'md5': ['/assets/libs/md5/md5.min']
    'masonry': '/assets/libs/masonry/masonry.pkgd-4.0.0.min'
    'autolinker': ['/assets/libs/autolinker/Autolinker.min']
    'showdown': ['//cdnjs.cloudflare.com/ajax/libs/showdown/1.3.0/showdown.min', '/assets/libs/showdown/showdown.min']
    'emojify': ['https://cdnjs.cloudflare.com/ajax/libs/emojify.js/0.9.5/emojify.min', '/assets/libs/emojify/emojify.min']
  shim:
    'underscore':
      exports: '_'
    'backbone':
      deps: ['underscore', 'jquery']
      exports: 'Backbone'
    'marionette':
      deps: ['underscore', 'backbone', 'jquery']
      exports: 'Marionette'
    'backgrid':
      deps: ['underscore', 'backbone']
      exports: 'Backgrid'
    'backgrid-paginator':
      deps: ['underscore', 'backbone', 'backgrid', 'backbone-paginator']
    'backbone-paginator':
      deps: ['underscore', 'backbone']
    'jquery':
      exports: '$'
    'jquery-truncate':
      deps: ['jquery']
    'jquery-textcomplete':
      deps: ['jquery']
    'handlebars':
      exports: 'Handlebars'
    'bootstrap-affix':
      deps: ['jquery']
      exports: '$.fn.affix'
    'bootstrap-alert':
      deps: ['jquery']
      exports: '$.fn.alert'
    'bootstrap-button':
      deps: ['jquery']
      exports: '$.fn.button'
    'bootstrap-carousel':
      deps: ['jquery']
      exports: '$.fn.carousel'
    'bootstrap-collapse':
      deps: ['jquery']
      exports: '$.fn.collapse'
    'bootstrap-dropdown':
      deps: ['jquery']
      exports: '$.fn.dropdown'
    'bootstrap-modal':
      deps: ['jquery']
      exports: '$.fn.modal'
    'bootstrap-popover':
      deps: ['jquery', 'bootstrap-tooltip']
      exports: '$.fn.popover'
    'bootstrap-scrollspy':
      deps: ['jquery']
      exports: '$.fn.scrollspy'
    'bootstrap-tab':
      deps: ['jquery']
      exports: '$.fn.tab'
    'bootstrap-tooltip':
      deps: ['jquery']
      exports: '$.fn.tooltip'
    'bootstrap-transition':
      deps: ['jquery']
      exports: '$.fn.transition'
    'bootstrap-notify':
      deps: ['jquery']
      exports: '$.fn.notify'
    'bootstrap-typeahead':
      deps: ['jquery']
      exports: '$.fn.typeahead'
)

###
  Including jQuery at the top level
  This will register jQuery as a global variable so it can be used by modules and the tests
###

require ['jquery', 'cs!coffee/App', 'cs!coffee/AppRouter'], ($, App, AppRouter) ->
  App.appRouter = new AppRouter()
  App.start()
  return
