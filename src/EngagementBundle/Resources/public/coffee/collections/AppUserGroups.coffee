define (require) ->

  Backbone = require 'backbone'
  Group = require 'cs!coffee/models/Group'

  class AppUserGroups extends Backbone.Collection

    model: Group

    url: ->
      return "/api/v1/user/#{@options.userId}/content_group/following"

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        totalRecords: 0
      return

    count: ->
      $.ajax
        url: "/api/v1/user/#{@options.userId}/content_group/following/count"
        success: (response) =>
          @state.totalRecords = parseInt(response.count, 10)
          @trigger('count')
          return
