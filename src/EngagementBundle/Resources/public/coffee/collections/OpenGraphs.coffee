define (require) ->

  Backbone = require 'backbone'
  OpenGraph = require 'cs!coffee/models/OpenGraph'

  class OpenGraphs extends Backbone.Collection

    model: OpenGraph

    url: ->
      return ''

    initialize: (models, options) ->
      @options = if options then options else {}
      return

    bySuccess: ->
      filtered = @filter((model) ->
        return model.has('success') and model.get('success') is true and model.has('data') and model.get('data').ogUrl and model.get('data').ogType and model.get('data').ogImage and model.get('data').ogImage.url and model.get('data').ogDescription and (model.get('data').ogType is 'article' or model.get('data').ogType is 'website') # Require some default data
      )
      return new OpenGraphs(filtered)
