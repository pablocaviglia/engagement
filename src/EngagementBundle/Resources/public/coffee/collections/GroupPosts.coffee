define (require) ->

  Backbone = require 'backbone'
  Post = require 'cs!coffee/models/Post'

  class GroupPosts extends Backbone.Collection

    model: Post

    url: ->
      return "/api/v1/content_group/#{@options.contentGroupId}/feed?offset=#{@state.offset}&limit=#{@pagination.limit}"

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        offset: 0
        exhausted: false
        totalRecords: 0
      @pagination =
        limit: 2
      return
