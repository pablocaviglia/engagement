define (require) ->

  Backbone = require 'backbone'
  User = require 'cs!coffee/models/User'

  class WhoToFollow extends Backbone.Collection

    model: User

    url: ->
      return "/api/v1/user/#{@options.userId}/who_to_follow?offset=#{@state.offset}&limit=#{@pagination.limit}"

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        offset: 0
        exhausted: false
        totalRecords: 0
      @pagination =
        limit: 4
      return

    parse: (response) ->
      if response.length < @pagination.limit
        @state.exhausted = true
      @state.offset = @state.offset + @pagination.limit
      @pagination.limit = 1 # Only increment by 1 the next time we get users
      return response

    lazyFetch: (options) ->
      options = if options then options else {}
      options.remove = false
      if not @state.exhausted
        @fetch(options)
      else
        return $.Deferred().resolve()
