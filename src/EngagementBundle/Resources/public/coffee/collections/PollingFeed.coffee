define (require) ->

  Backbone = require 'backbone'
  Post = require 'cs!coffee/models/Post'
  App = require 'cs!coffee/App'

  class PollingFeed extends Backbone.Collection

    model: Post

    url: ->
      return "/api/v1/user/#{@options.userId}/feed"

    initialize: (models, options) ->
      @options = if options then options else {}
      return

    parse: (response) ->
      _.reject(response, (post) -> # Do not show the post if it is a post from the current user
        return post.user.id is App.user.id
      )
