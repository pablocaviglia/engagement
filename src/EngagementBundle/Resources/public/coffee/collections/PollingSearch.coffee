define (require) ->

  Backbone = require 'backbone'
  Post = require 'cs!coffee/models/Post'
  App = require 'cs!coffee/App'

  class PollingSearch extends Backbone.Collection

    model: Post

    url: ->
      return "/api/v1/search?search=#{encodeURIComponent(@options.q)}"

    initialize: (models, options) ->
      @options = if options then options else {}
      return
