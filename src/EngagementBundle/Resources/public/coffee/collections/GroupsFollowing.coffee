define (require) ->

  Backbone = require 'backbone'
  Group = require 'cs!coffee/models/Group'

  class GroupsFollowing extends Backbone.Collection

    model: Group

    url: ->
      return "/api/v1/user/#{@options.userId}/content_group/following?offset=#{@state.offset}&limit=#{@pagination.limit}"

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        offset: 0
        exhausted: false
        totalRecords: 0
      @pagination =
        limit: 10
      return

    parse: (response) ->
      if response.length < @pagination.limit
        @state.exhausted = true
      @state.offset = @state.offset + @pagination.limit
      return response

    lazyFetch: (options) ->
      options = if options then options else {}
      options.remove = false
      if not @state.exhausted
        @fetch(options)
      else
        return $.Deferred().resolve()

    count: ->
      $.ajax
        url: "/api/v1/user/#{@options.userId}/content_group/following/count"
        success: (response) =>
          @state.totalRecords = parseInt(response.count, 10)
          @trigger('count')
          return
