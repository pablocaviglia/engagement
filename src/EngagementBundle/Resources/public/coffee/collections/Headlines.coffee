define (require) ->

  Backbone = require 'backbone'
  Headline = require 'cs!coffee/models/Headline'

  class Headlines extends Backbone.Collection

    model: Headline

    url: ->
      return "//hidden-escarpment-96413.herokuapp.com/ogh?offset=#{@state.offset}&limit=#{@pagination.limit}"

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        offset: 0
        exhausted: false
        totalRecords: 0
        pageSize: 0
      @pagination =
        limit: 10
      return

    parse: (response) ->
      @pageSize = response.length
      if response.length < @pagination.limit
        @state.exhausted = true
      @state.offset = @state.offset + @pagination.limit
      return response

    lazyFetch: (options) ->
      options = if options then options else {}
      options.remove = false
      if not @state.exhausted
        @fetch(options)
      else
        return $.Deferred().resolve()
