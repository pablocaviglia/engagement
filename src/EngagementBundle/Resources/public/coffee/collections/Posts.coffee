define (require) ->

  Backbone = require 'backbone'
  Post = require 'cs!coffee/models/Post'
  App = require 'cs!coffee/App'

  class Posts extends Backbone.Collection

    model: Post

    url: ->
      if @options.userId
        return "/api/v1/user/#{@options.userId}/posts?offset=#{@state.offset}&limit=#{@pagination.limit}" # Specify the user id and get posts
      if @options.contentGroupId
        return "/api/v1/content_group/#{@options.contentGroupId}/feed?offset=#{@state.offset}&limit=#{@pagination.limit}" # Specify the user id and get posts
      else if @options.q
        return "/api/v1/search?offset=#{@state.offset}&limit=#{@pagination.limit}&search=#{encodeURIComponent(@options.q)}" # Specify a search keyword
      else
        return "/api/v1/user/#{App.user.get('id')}/feed?offset=#{@state.offset}&limit=#{@pagination.limit}" # Leave blank to retrieve current user's feed

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        offset: 0
        exhausted: false
        totalRecords: 0
      @pagination =
        limit: 10
      return

    parse: (response) ->
      if response.length < @pagination.limit
        @state.exhausted = true
      @state.offset = @state.offset + @pagination.limit
      return response

    lazyFetch: (options) ->
      options = if options then options else {}
      options.remove = false
      if not @state.exhausted
        @fetch(options)
      else
        return $.Deferred().resolve()
