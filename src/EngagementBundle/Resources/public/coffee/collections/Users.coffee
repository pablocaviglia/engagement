define (require) ->

  Backbone = require 'backbone'
  User = require 'cs!coffee/models/User'

  require 'backbone-paginator'

  class Users extends Backbone.PageableCollection

    model: User

    url: ->
      return '/api/v1/users'

    initialize: (models, options) ->
      @options = if options then options else {}
      return

    state:
      pageSize: 10
      firstPage: 0
      currentPage: 0

    mode: 'server'

    queryParams:
      currentPage: 'offset'
      offset: ->
        @state.currentPage * @state.pageSize
      totalPages: null
      totalRecords: null
      pageSize: 'limit'
      sortKey: 'sortBy'
      order: 'sortOrder'

    count: ->
      $.ajax
        url: '/api/v1/users/count'
        success: (response) =>
          @state.totalRecords = parseInt(response.count, 10)
          @trigger('count')
          return
