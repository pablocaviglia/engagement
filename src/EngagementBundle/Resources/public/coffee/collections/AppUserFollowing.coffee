define (require) ->

  Backbone = require 'backbone'
  User = require 'cs!coffee/models/User'

  class AppUserFollowing extends Backbone.Collection

    model: User

    url: ->
      return "/api/v1/user/#{@options.userId}/following"

    initialize: (models, options) ->
      @options = if options then options else {}
      @state =
        totalRecords: 0
      return

    count: ->
      $.ajax
        url: "/api/v1/user/#{@options.userId}/following/count"
        success: (response) =>
          @state.totalRecords = parseInt(response.count, 10)
          @trigger('count')
          return
