define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  Backbone = require 'backbone'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  User = require 'cs!coffee/models/User'
  UserHeader = require 'cs!coffee/views/user/UserHeader'
  userPage = require 'text!templates/user/userPage.html'

  class UserPage extends Marionette.LayoutView

    template: Handlebars.compile userPage

    regions:
      userHeaderRegion: '[data-region="user-header-region"]'
      userAlertRegion: '[data-region="user-alert-region"]'
      userPageRegion: '[data-region="user-page-region"]'
      whoToFollowRegion: '[data-region="who-to-follow-region"]'

    initialize: ->
      @model = new User( id: @options.userId )
      return

    onRender: ->
      $.when(@model.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          @getRegion('userHeaderRegion').show(new UserHeader(
            model: @model
            route: @options.route
          ))
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The user details could not be loaded at this time.'
            @getRegion('userAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      @showPage(@options.module)
      if App.user.get('id') is @options.userId
        require ['collections/WhoToFollow', 'views/feed/WhoToFollowCollection'], (WhoToFollow, WhoToFollowCollection) =>
          # This doesn't necessarily have to happen so going to go with a separate call instead of throwing it in the main $.when
          @whotofollow = new WhoToFollow([], { userId: App.user.get('id') })
          $.when(@whotofollow.fetch()).done( =>
            if @whotofollow.models.length
              @getRegion('whoToFollowRegion').show(new WhoToFollowCollection(collection: @whotofollow))
            return
          ).fail( ->
            
          )
        return
      return

    childEvents:
      showPostsTab: (view) ->
        App.appRouter.navigate(view.options.model.get('id') + '/posts')
        require ['cs!coffee/views/posts/PostsPage'], (PostsPage) =>
          @getRegion('userPageRegion').show(new PostsPage( userId: view.options.model.get('id') ))
          return
        return

      showFollowingTab: (view) ->
        App.appRouter.navigate(view.options.model.get('id') + '/following')
        require ['cs!coffee/views/following/FollowingPage'], (FollowingPage) =>
          @getRegion('userPageRegion').show(new FollowingPage( userId: view.options.model.get('id') ))
          return
        return

      showFollowersTab: (view) ->
        App.appRouter.navigate(view.options.model.get('id') + '/followers')
        require ['cs!coffee/views/followers/FollowersPage'], (FollowersPage) =>
          @getRegion('userPageRegion').show(new FollowersPage( userId: view.options.model.get('id') ))
          return
        return

      showGroupsFollowingTab: (view) ->
        App.appRouter.navigate(view.options.model.get('id') + '/groups')
        require ['cs!coffee/views/groups/GroupsFollowingPage'], (GroupsFollowingPage) =>
          @getRegion('userPageRegion').show(new GroupsFollowingPage( userId: view.options.model.get('id') ))
          return
        return

    showPage: (module) ->
      @getRegion('userPageRegion').show(module)
      return
