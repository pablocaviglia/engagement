define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  userHeader = require 'text!templates/user/userHeader.html'

  class UserHeader extends Marionette.ItemView

    template: Handlebars.compile userHeader

    events:
      'click [data-event="poststab"]': '_showPosts' # Appending "tab" here to the event so it doesn't propagate up the the main layout
      'click [data-event="followingtab"]': '_showFollowing'
      'click [data-event="followerstab"]': '_showFollowers'
      'click [data-event="groupsfollowingtab"]': '_showGroupsFollowing'

    templateHelpers: ->
      helpers:
        is_route_followers: do =>
          @options.route is 'followers'
        is_route_following: do =>
          @options.route is 'following'
        is_route_posts: do =>
          @options.route is 'posts'
        is_route_groups_following: do =>
          @options.route is 'groupsFollowing'

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _showPosts: (e) ->
      e.preventDefault()
      $(@el).find('.nav li').removeClass('active')
      $(e.currentTarget).parent().addClass('active')
      @triggerMethod('showPostsTab')
      return

    _showFollowing: (e) ->
      e.preventDefault()
      $(@el).find('.nav li').removeClass('active')
      $(e.currentTarget).parent().addClass('active')
      @triggerMethod('showFollowingTab')
      return

    _showFollowers: (e) ->
      e.preventDefault()
      $(@el).find('.nav li').removeClass('active')
      $(e.currentTarget).parent().addClass('active')
      @triggerMethod('showFollowersTab')
      return

    _showGroupsFollowing: (e) ->
      e.preventDefault()
      $(@el).find('.nav li').removeClass('active')
      $(e.currentTarget).parent().addClass('active')
      @triggerMethod('showGroupsFollowingTab')
      return
