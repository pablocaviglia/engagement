define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  GroupsCollectionItem = require 'cs!coffee/views/groups/GroupsCollectionItem'

  class GroupsCollection extends Marionette.CollectionView

    className: 'cards cards-cols-xs-1 cards-cols-sm-1 cards-cols-md-1 cards-cols-lg-2'

    childView: GroupsCollectionItem
