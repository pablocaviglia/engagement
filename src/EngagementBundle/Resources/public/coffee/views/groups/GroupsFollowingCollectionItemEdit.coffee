define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  groupsFollowingCollectionItemEdit = require 'text!templates/groups/groupsFollowingCollectionItemEdit.html'

  class GroupsFollowingCollectionItemEdit extends Marionette.ItemView

    events:
      'click [data-event="cancel"]': '_cancelEditGroup'
      'submit [data-event="update"]': '_updateGroup'

    template: Handlebars.compile groupsFollowingCollectionItemEdit

    regions:
      groupEditAlertRegion: '[data-region="group-edit-alert-region"]'

    onRender: ->
      image = new Image()
      image.addEventListener('load', =>
        $(@el).find('.img-placeholder').replaceWith('<img src="/assets/images/group-' + @model.get('id') + '.jpg" />')
        return
      )
      image.addEventListener('error', ->
        return
      )
      image.src = '/assets/images/group-' + @model.get('id') + '.jpg'

      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _cancelEditGroup: (e) ->
      e.preventDefault()
      @triggerMethod('cancelEditGroup')
      return

    _updateGroup: (e) ->
      e.preventDefault()
      data = _.object(_.map($(e.currentTarget).serializeArray(), _.values)) # An underscore methed to serialize the form into an object
      $(e.currentTarget).find('fieldset').prop('disabled', true) # This needs to be after the data is collected!
      if not $.trim(data.name) or not $.trim(data.description)
        require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
          $(e.currentTarget).find('fieldset').prop('disabled', false)
          @getRegion('groupEditAlertRegion').show new AppAlert(scrollTo: true, model: new Backbone.Model(
            type: 'danger'
            text: 'The group needs a name and description.'
            dismissable: true
          ))
          return
      else
        if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
          @xhr.abort()
        @xhr = @model.save data,
          success: =>
            $(@el).fadeOut( =>
              @triggerMethod 'editGroupSuccess'
              return
            )
            return
          error: (model, response) =>
            $(e.currentTarget).find('fieldset').prop('disabled', false)
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The group could not be edited at this time.'
              @getRegion('groupEditAlertRegion').show new AppAlert(scrollTo: true, model: new Backbone.Model(
                type: 'danger'
                text: text
                dismissable: true
              ))
              return
            return
      return
