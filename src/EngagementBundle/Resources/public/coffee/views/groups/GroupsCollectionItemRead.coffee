define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  moment = require 'moment'
  groupsCollectionItemRead = require 'text!templates/groups/groupsCollectionItemRead.html'
  GroupsCollectionItemButton = require 'cs!coffee/views/groups/GroupsCollectionItemButton'

  class GroupsCollectionItemRead extends Marionette.LayoutView

    regions:
      followButtonRegion: '[data-region="follow-button-region"]'

    templateHelpers: ->
      helpers:
        app_user_is_group_owner: do =>
          @model.get('created_by').id is App.user.get('id')

    events:
      'click [data-event="group"]': '_showGroupPage'
      'click [data-event="delete"]': '_deleteGroup'
      'click [data-event="edit"]': '_editGroup'

    template: Handlebars.compile groupsCollectionItemRead

    onRender: ->
      @getRegion('followButtonRegion').show(new GroupsCollectionItemButton(
        model: @model
      ))

      image = new Image()
      image.addEventListener('load', =>
        $(@el).find('.img-placeholder').replaceWith('<img src="/assets/images/group-' + @model.get('id') + '.jpg" />')
        return
      )
      image.addEventListener('error', ->
        return
      )
      image.src = '/assets/images/group-' + @model.get('id') + '.jpg'

      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _showGroupPage: (e) ->
      e.preventDefault()
      e.stopPropagation()
      contentGroupId = @model.get('id')
      App.appRouter.navigate('groups/' + contentGroupId)
      require ['cs!coffee/views/groups/GroupPostsPage'], (GroupPostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new GroupPostsPage( contentGroupId: contentGroupId ))
        return
      return

    _deleteGroup: (e) ->
      e.preventDefault()
      $(@el).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp(=>
          @model.destroy({ silent: true })
        )
      )
      return

    _editGroup: (e) ->
      e.preventDefault()
      @triggerMethod('editGroup')
      return
