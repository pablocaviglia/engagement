define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  groupsFollowingCollectionItemRead = require 'text!templates/groups/groupsFollowingCollectionItemRead.html'

  class GroupsFollowingCollectionItemRead extends Marionette.ItemView

    templateHelpers: ->
      helpers:
        app_user_on_own_page: do =>
          App.user.get('id') is @options.userId
        app_user_following: do =>
          App.user.following.get(@model.get('id'))

    events:
      'click [data-event="group"]': '_showGroupPage'
      'mouseenter [data-event="hoverfollowing"]': '_hoverFollowingButton'
      'mouseleave [data-event="hoverfollowing"]': '_unHoverFollowingButton'
      'submit [data-event="unfollow"]': '_unfollow'
      'click [data-event="delete"]': '_deleteGroup'
      'click [data-event="edit"]': '_editGroup'

    template: Handlebars.compile groupsFollowingCollectionItemRead

    onRender: ->
      image = new Image()
      image.addEventListener('load', =>
        $(@el).find('.img-placeholder').replaceWith('<img src="/assets/images/group-' + @model.get('id') + '.jpg" />')
        return
      )
      image.addEventListener('error', ->
        return
      )
      image.src = '/assets/images/group-' + @model.get('id') + '.jpg'

      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _hoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-warning').addClass('btn-danger').find('span:not(.xhr)').text('Unfollow')

    _unHoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-danger').addClass('btn-warning').find('span:not(.xhr)').text('Following')

    _unfollow: (e) -> # Use caution here, jQuery in use...
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-warning').addClass('btn-danger')
      @model.unfollow().done( ->
        #App.user.following.count() # Moved to the actual model
      )
      $(@el).delay(200).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp()
      )
      return

    _showGroupPage: (e) ->
      e.preventDefault()
      e.stopPropagation()
      contentGroupId = @model.get('id')
      App.appRouter.navigate('groups/' + contentGroupId)
      require ['cs!coffee/views/groups/GroupPostsPage'], (GroupPostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new GroupPostsPage( contentGroupId: contentGroupId ))
        return
      return

    _deleteGroup: (e) ->
      e.preventDefault()
      $(@el).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp(=>
          @model.destroy({ silent: true })
        )
      )
      return

    _editGroup: (e) ->
      e.preventDefault()
      @triggerMethod('editGroup')
      return
