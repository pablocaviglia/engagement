define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  moment = require 'moment'
  groupsCollectionItemButton = require 'text!templates/groups/groupsCollectionItemButton.html'

  class GroupsCollectionItemButton extends Marionette.ItemView

    events:
      'submit [data-event="unfollow"]': '_unfollow'
      'submit [data-event="follow"]': '_follow'
      'mouseenter [data-event="hoverfollowing"]': '_hoverFollowingButton'
      'mouseleave [data-event="hoverfollowing"]': '_unHoverFollowingButton'

    templateHelpers: ->
      helpers:
        app_user_following: do =>
          App.user.groups.get(@model.get('id'))

    template: Handlebars.compile groupsCollectionItemButton

    _hoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-warning').addClass('btn-danger').find('span:not(.xhr)').text('Unfollow')

    _unHoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-danger').addClass('btn-warning').find('span:not(.xhr)').text('Following')

    _unfollow: (e) -> # Use caution here, jQuery in use...
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-warning').addClass('btn-danger')
      @model.unfollow().done( =>
        @render()
      )
      return

    _follow: (e) ->
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-default').addClass('btn-warning')
      @model.follow().done( =>
        @render()
      )
      return
