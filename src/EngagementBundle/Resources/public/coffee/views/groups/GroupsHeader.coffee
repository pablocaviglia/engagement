define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  Backbone = require 'backbone'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  groupsHeader = require 'text!templates/groups/groupsHeader.html'
  GroupsHeaderRead = require 'cs!coffee/views/groups/GroupsHeaderRead'

  class GroupsHeader extends Marionette.LayoutView

    template: Handlebars.compile groupsHeader

    regions:
      groupRegion: '[data-region="group-region"]'

    onRender: ->
      @getRegion('groupRegion').show(new GroupsHeaderRead( model: @model ))
      return

    childEvents:
      editGroup: ->
        require ['cs!coffee/views/groups/GroupsHeaderEdit'], (GroupsHeaderEdit) =>
          @getRegion('groupRegion').show(new GroupsHeaderEdit( model: @model ))
          return
        return
      cancelEditGroup: ->
        @getRegion('groupRegion').show(new GroupsHeaderRead( model: @model ))
        return
      editGroupSuccess: ->
        @getRegion('groupRegion').show(new GroupsHeaderRead( model: @model ))
        return
