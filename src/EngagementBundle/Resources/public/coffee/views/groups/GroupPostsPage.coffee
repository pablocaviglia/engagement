define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Posts = require 'cs!coffee/collections/Posts'
  PostsCollection = require 'cs!coffee/views/posts/PostsCollection'
  SharePost = require 'cs!coffee/views/posts/SharePost'
  Group = require 'cs!coffee/models/Group'
  GroupsHeader = require 'cs!coffee/views/groups/GroupsHeader'
  groupPostsPage = require 'text!templates/groups/groupPostsPage.html'

  class GroupPostsPage extends Marionette.LayoutView

    template: Handlebars.compile groupPostsPage

    regions:
      groupHeaderRegion: '[data-region="group-header-region"]'
      groupPostsAlertRegion: '[data-region="group-posts-alert-region"]'
      groupPostsPollingAlertRegion: '[data-region="group-posts-polling-alert-region"]'
      groupPostsCollectionRegion: '[data-region="group-posts-collection-region"]'
      groupSharePostRegion: '[data-region="group-share-post-region"]'

    initialize: ->
      @collection = new Posts([], contentGroupId: @options.contentGroupId)
      @model = new Group(id: @options.contentGroupId)
      return

    onRender: ->
      $.when(@model.fetch(), @collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('groupPostsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: 'There are no posts in the group.'
                dismissable: false
              ))
              return
          @getRegion('groupPostsCollectionRegion').show(new PostsCollection(collection: @collection))
          # Still show the share post view
          @getRegion('groupSharePostRegion').show(new SharePost(
            groupModel: @model
          ))
          @getRegion('groupHeaderRegion').show(new GroupsHeader(
            model: @model
          ))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The posts could not be loaded at this time.'
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            @getRegion('groupPostsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      @render_time = (new Date()).getTime()
      @polling = setInterval(@_polling, 60000)
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    childEvents:
      submitPostSuccess: (view) ->
        @collection.add view.post, at: 0
        @getRegion('groupPostsAlertRegion').empty()
        @getRegion('groupSharePostRegion').show(new SharePost( groupModel: view.options.groupModel ))
        return
      loadBackgroundPosts: (view) ->
        clearInterval(@polling)
        @collection.add(@polling_collection.models, at: 0)
        @getRegion('groupPostsAlertRegion').empty() # Emptying this view in case it tells the user there are no posts
        @getRegion('groupPostsPollingAlertRegion').empty()
        @polling_collection.reset()
        @polling = setInterval(@_polling, 60000)
        return

    onDestroy: ->
      $(window).off('scroll', @_lazyListening)
      clearInterval(@polling)

    _polling: =>
      require ['collections/PollingFeed'], (PollingFeed) => # Require this module later because it is not needed until the first poll
        if not @polling_collection
          @polling_collection = new PollingFeed([], { userId: App.user.get('id') })
        @col_length = @polling_collection.models.length
        @polling_collection.fetch(
          remove: false
          data:
            created_at: @render_time
        ).done( =>
          if @col_length isnt @polling_collection.models.length and not @getRegion('groupPostsPollingAlertRegion').currentView # Only show the alert if posts were returned and only show the alert if there is not already an alert in the region (it will rerender itself if more posts are found)
            require ['cs!coffee/views/groups/GroupPostsPollingAlert'], (GroupPostsPollingAlert) =>
              @getRegion('grouPostsPollingAlertRegion').show new GroupPostsPollingAlert(collection: @polling_collection)
              return
        )
        @render_time = (new Date()).getTime()
        return
      return

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
