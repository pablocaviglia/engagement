define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  GroupsFollowingCollectionItem = require 'cs!coffee/views/groups/GroupsFollowingCollectionItem'

  class GroupsFollowingCollection extends Marionette.CollectionView

    className: 'cards cards-cols-xs-1 cards-cols-sm-1 cards-cols-md-2 cards-cols-lg-2'

    childView: GroupsFollowingCollectionItem

    childViewOptions: ->
      options =
        userId: @options.userId
