define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Groups = require 'cs!coffee/collections/Groups'
  GroupsCollection = require 'cs!coffee/views/groups/GroupsCollection'
  CreateGroup = require 'cs!coffee/views/groups/CreateGroup'
  groupsPage = require 'text!templates/groups/groupsPage.html'

  class GroupsPage extends Marionette.LayoutView

    template: Handlebars.compile groupsPage

    regions:
      groupsAlertRegion: '[data-region="groups-alert-region"]'
      groupsCollectionRegion: '[data-region="groups-collection-region"]'
      groupsCreateRegion: '[data-region="groups-create-region"]'

    initialize: ->
      @collection = new Groups()
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('groupsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: 'There are no groups.'
                dismissable: false
              ))
              return
          @getRegion('groupsCollectionRegion').show(new GroupsCollection(collection: @collection))

          # Still show the create group view
          @getRegion('groupsCreateRegion').show(new CreateGroup())
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The groups could not be loaded at this time.'
            @getRegion('groupsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    childEvents:
      createGroupSuccess: (view) ->
        @collection.add view.group, at: 0
        @getRegion('groupsAlertRegion').empty()
        @getRegion('groupsCreateRegion').show(new CreateGroup())
        return
