define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  GroupsFollowing = require 'cs!coffee/collections/GroupsFollowing'
  GroupsFollowingCollection = require 'cs!coffee/views/groups/GroupsFollowingCollection'
  groupsFollowingPage = require 'text!templates/groups/groupsFollowingPage.html'
  User = require 'cs!coffee/models/User'

  class GroupsFollowingPage extends Marionette.LayoutView

    template: Handlebars.compile groupsFollowingPage

    regions:
      groupsFollowingAlertRegion: '[data-region="groups-following-alert-region"]'
      groupsFollowingCollectionRegion: '[data-region="groups-following-collection-region"]'

    initialize: ->
      @collection = new GroupsFollowing([], { userId: @options.userId })
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('groupsFollowingAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: do =>
                  if @options.userId is App.user.get('id')
                    return 'You are not following any groups.'
                  else
                    return 'The user is not following any groups.'
                dismissable: false
              ))
              return
          @getRegion('groupsFollowingCollectionRegion').show(new GroupsFollowingCollection(
            collection: @collection
            userId: @options.userId
          ))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The list could not be loaded at this time.'
            @getRegion('groupsFollowingAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    onDestroy: ->
      $(window).off('scroll', @_lazyListening)

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
