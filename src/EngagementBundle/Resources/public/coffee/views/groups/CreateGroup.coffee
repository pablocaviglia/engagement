define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  Backbone = require 'backbone'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  createGroup = require 'text!templates/groups/createGroup.html'
  Group = require 'cs!coffee/models/Group'

  require 'bootstrap-typeahead'

  class CreateGroup extends Marionette.LayoutView

    events:
      'focus [data-event="name"]': '_showFields'
      'submit [data-event="create"]': '_createGroup'

    regions:
      createAlertRegion: '[data-region="create-alert-region"]'

    template: Handlebars.compile createGroup

    onRender: ->
      $(@el).find('[data-event="name"]').typeahead(
        source: (query, process) ->
          $.getJSON '/api/v1/content_group/query', { search: query }, (data) ->
            process(data)
        autoSelect: false
        #menu: '<ul class="typeahead dropdown-menu" role="listbox"><li class="dropdown-header">Existing Groups</li></ul>',
        item: '<li><span class="dropdown-item" href="#" role="option"></span></li>'
        useSpan: true
      )

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _showFields: ->
      view = $(@el)
      $(@el).find('[data-event="focus"]').css('opacity', 0).slideDown( ->
        $(@).animate(opacity: 1)
        view.find('[data-event="name"]').removeAttr('data-event') # Don't need to keep sliding down so remove the event listener
      )
      return

    _createGroup: (e) ->
      e.preventDefault()
      data = _.object(_.map($(e.currentTarget).serializeArray(), _.values)) # An underscore methed to serialize the form into an object
      $(e.currentTarget).find('fieldset').prop('disabled', true) # This needs to be after the data is collected!
      if not $.trim(data.name) or not $.trim(data.description)
        require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
          $(e.currentTarget).find('fieldset').prop('disabled', false)
          @getRegion('createAlertRegion').show new AppAlert(scrollTo: true, model: new Backbone.Model(
            type: 'danger'
            text: 'The group needs a name and description.'
            dismissable: true
          ))
          return
      else
        data.created_by = id: App.user.get('id')
        @group = new Group(data)
        if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
          @xhr.abort()
        @xhr = @group.save data,
          success: =>
            $(@el).fadeOut( =>
              @triggerMethod 'createGroupSuccess'
              return
            )
            return
          error: (model, response) =>
            $(e.currentTarget).find('fieldset').prop('disabled', false)
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              text = if response and response.responseJSON and response.responseJSON.managed then response.responseJSON.exception.message else 'The group could not be created at this time.'
              @getRegion('createAlertRegion').show new AppAlert(scrollTo: true, model: new Backbone.Model(
                type: 'danger'
                text: text
                dismissable: true
              ))
              return
            return
      return
