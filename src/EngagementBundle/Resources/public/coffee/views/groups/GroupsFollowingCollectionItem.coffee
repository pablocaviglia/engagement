define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  groupsFollowingCollectionItem = require 'text!templates/groups/groupsFollowingCollectionItem.html'
  GroupsFollowingCollectionItemRead = require 'cs!coffee/views/groups/GroupsFollowingCollectionItemRead'

  class GroupsFollowingCollectionItem extends Marionette.LayoutView

    className: 'card col-sm-1 col-md-2'

    attributes: ->
      'data-id': @model.get 'id'

    template: Handlebars.compile groupsFollowingCollectionItem

    regions:
      groupRegion: '[data-region="group-region"]'

    onRender: ->
      @getRegion('groupRegion').show(new GroupsFollowingCollectionItemRead( model: @model, userId: @options.userId ))
      return

    childEvents:
      editGroup: ->
        require ['cs!coffee/views/groups/GroupsFollowingCollectionItemEdit'], (GroupsFollowingCollectionItemEdit) =>
          @getRegion('groupRegion').show(new GroupsFollowingCollectionItemEdit( model: @model ))
          return
        return
      cancelEditGroup: ->
        @getRegion('groupRegion').show(new GroupsFollowingCollectionItemRead( model: @model, userId: @options.userId ))
        return
      editGroupSuccess: ->
        @getRegion('groupRegion').show(new GroupsFollowingCollectionItemRead( model: @model, userId: @options.userId ))
        return
