define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  Backbone = require 'backbone'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  groupsHeaderRead = require 'text!templates/groups/groupsHeaderRead.html'
  GroupsCollectionItemButton = require 'cs!coffee/views/groups/GroupsCollectionItemButton'

  class GroupsHeaderRead extends Marionette.LayoutView

    events:
      'click [data-event="delete"]': '_deleteGroup'
      'click [data-event="edit"]': '_editGroup'

    regions:
      followGroupButtonRegion: '[data-region="follow-group-button-region"]'

    template: Handlebars.compile groupsHeaderRead

    templateHelpers: ->
      helpers:
        app_user_following: do =>
          if @model then return App.user.groups.get(@model.get('id')) else return ''
        app_user_is_group_owner: do =>
          @model.get('created_by').id is App.user.get('id')

    onRender: ->
      @getRegion('followGroupButtonRegion').show(new GroupsCollectionItemButton(
        model: @model
      ))
      image = new Image()
      image.addEventListener('load', =>
        $(@el).find('.img-placeholder').replaceWith('<img src="/assets/images/group-' + @model.get('id') + '.jpg" />')
        return
      )
      image.addEventListener('error', ->
        return
      )
      image.src = '/assets/images/group-' + @model.get('id') + '.jpg'

      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)

    _deleteGroup: (e) ->
      e.preventDefault()
      $(@el).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp(=>
          @model.destroy({ silent: true }).done( ->
            App.appRouter.navigate('groups')
            require ['cs!coffee/views/groups/GroupsPage'], (GroupsPage) ->
              App.layout.getRegion('appRegion').currentView.showPage(new GroupsPage())
              return
            return
          )
        )
      )
      return

    _editGroup: (e) ->
      e.preventDefault()
      @triggerMethod('editGroup')
      return
