define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  moment = require 'moment'
  groupsCollectionItem = require 'text!templates/groups/groupsCollectionItem.html'
  GroupsCollectionItemRead = require 'cs!coffee/views/groups/GroupsCollectionItemRead'

  class GroupsCollectionItem extends Marionette.LayoutView

    className: 'col-md-12 col-lg-6 card'

    attributes: ->
      'data-id': @model.get 'id'

    template: Handlebars.compile groupsCollectionItem

    regions:
      groupRegion: '[data-region="group-region"]'

    onRender: ->
      @getRegion('groupRegion').show(new GroupsCollectionItemRead( model: @model ))
      return

    childEvents:
      editGroup: ->
        require ['cs!coffee/views/groups/GroupsCollectionItemEdit'], (GroupsCollectionItemEdit) =>
          @getRegion('groupRegion').show(new GroupsCollectionItemEdit( model: @model ))
          return
        return
      cancelEditGroup: ->
        @getRegion('groupRegion').show(new GroupsCollectionItemRead( model: @model ))
        return
      editGroupSuccess: ->
        @getRegion('groupRegion').show(new GroupsCollectionItemRead( model: @model ))
        return
