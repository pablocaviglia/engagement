define (require) ->

  # $ = require 'jquery'
  _ = require 'underscore'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  SidebarStats = require 'cs!coffee/views/sidebar/SidebarStats'
  main = require 'text!templates/main.html'

  require 'bootstrap-transition'
  require 'bootstrap-dropdown'
  require 'bootstrap-modal'

  class Main extends Marionette.LayoutView

    template: Handlebars.compile main

    events:
      'click [data-event="home"]': '_goHome' # Does nothing for now
      'click [data-event="signout"]': '_signOutUser'
      'click [data-event="groups"]': '_showGroupsPage'
      'click [data-event="headlines"]': '_showHeadlinesPage'
      'submit [role="search"]': '_searchPosts'
      'click [data-event="user"]': '_showPostsPage'
      'click [data-toggle="slide"]': '_toggleSlideNav'
      'click [data-event="slide"]': '_hideSlideNav'

    regions:
      pageRegion: '[data-region="page-region"]'
      modalRegion: '[data-region="modal-region"]'
      sidebarStatsRegion: '[data-region="sidebar-stats-region"]'

    templateHelpers: ->
      helpers:
        search_text: do =>
          @options.q
        current_page_is_home: do ->
          Backbone.history.getFragment() is ''
        current_page_is_groups: do ->
          Backbone.history.getFragment().indexOf('groups') is 0
        current_page_is_headlines: do ->
          Backbone.history.getFragment().indexOf('headlines') is 0

    initialize: ->
      @model = App.user
      @listenTo(App.vent, 'modal', (view) ->
        @getRegion('modalRegion').show(view)
        $('#modal').modal()
        return
      , @)

      @listenTo(App.appRouter, 'navigate', ->
        window.scrollTo(0, 0)
        @_updateHeaderState()
      , @)

      @listenTo(App.appRouter, 'route', ->
        window.scrollTo(0, 0)
        @_updateHeaderState()
      , @)

      return

    onRender: ->
      $('body').addClass('authenticated')
      @showPage(@options.module)
      $.when(@model.followers.count(), @model.following.count(), @model.groups.count()).done( =>
        $(@getRegion('sidebarStatsRegion').el).css(
          opacity: 0
        )
        @getRegion('sidebarStatsRegion').show(new SidebarStats(
          model: @model
          followers: @model.followers
          following: @model.following
          groups: @model.groups
        ))
        $(@getRegion('sidebarStatsRegion').el).slideDown( ->
          $(@).animate(opacity: 1)
        )
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    ###
      Uniphy Public
    ###

    showPage: (module) ->
      @getRegion('pageRegion').show(module)
      return

    _updateHeaderState: -> # Keeps the header menu and search in sync with back/forward and page navigation (#TODO: Clean it up so we aren't checking every page)
      if Backbone.history.getFragment() is ''
        $(@el).find('[data-listen="navigation"]').removeClass('active')
        $(@el).find('[data-event="home"]').parent().addClass('active')
        $(@el).find('[data-listen="search"]').val('')
      else if Backbone.history.getFragment().indexOf('groups') is 0
        $(@el).find('[data-listen="navigation"]').removeClass('active')
        $(@el).find('[data-event="groups"]').parent().addClass('active')
        $(@el).find('[data-listen="search"]').val('')
      else if Backbone.history.getFragment().indexOf('headlines') is 0
        $(@el).find('[data-listen="navigation"]').removeClass('active')
        $(@el).find('[data-event="headlines"]').parent().addClass('active')
        $(@el).find('[data-listen="search"]').val('')
      else if Backbone.history.getFragment().indexOf('search') is 0
        $(@el).find('[data-listen="navigation"]').removeClass('active')
        $(@el).find('[data-listen="search"]').val(App.appRouter.urlParameters().q)
      else
        $(@el).find('[data-listen="navigation"]').removeClass('active')
        $(@el).find('[data-listen="search"]').val('')
      return

    # Search

    _searchPosts: (e) ->
      e.preventDefault()
      data = _.object(_.map($(e.currentTarget).serializeArray(), _.values))
      if data.q
        App.appRouter.navigate('/search?q=' + encodeURIComponent(data.q))
        require ['cs!coffee/views/search/SearchPage'], (SearchPage) =>
          @getRegion('pageRegion').show(new SearchPage(
            q: data.q
          ))
          return
        return

    # Sidebar Navigation

    _showPostsPage: (e) ->
      e.preventDefault()
      id = @model.get('id')
      App.appRouter.navigate(id + '/posts')
      require ['cs!coffee/views/user/UserPage', 'views/posts/PostsPage'], (UserPage, PostsPage) =>
        @showPage(new UserPage(
          userId: id
          route: 'posts'
          module: new PostsPage( userId: id )
        ))
        return
      return

    # Header Navigation

    _goHome: (e) ->
      e.preventDefault()
      App.appRouter.navigate('/')
      require ['cs!coffee/views/Main', 'views/feed/FeedPage'], (Main, FeedPage) =>
        @getRegion('pageRegion').show(new FeedPage())
        return
      return

    _showGroupsPage: (e) ->
      e.preventDefault()
      App.appRouter.navigate('/groups')
      require ['cs!coffee/views/groups/GroupsPage'], (GroupsPage) =>
        @showPage(new GroupsPage())
        return
      return

    _showHeadlinesPage: (e) ->
      e.preventDefault()
      App.appRouter.navigate('/headlines')
      require ['cs!coffee/views/headlines/HeadlinesPage'], (HeadlinesPage) =>
        @showPage(new HeadlinesPage())
        return
      return

    _signOutUser: (e) ->
      e.preventDefault()
      App.trigger 'unauthenticate'
      return

    # Mobile Nav

    _toggleSlideNav: (e) ->
      e.preventDefault()
      $('body').toggleClass('nav-slide-active')

    _hideSlideNav: (e) ->
      e.preventDefault()
      $('body').removeClass('nav-slide-active')
