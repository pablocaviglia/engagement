define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  appAlert = require 'text!templates/app/appAlert.html'

  require 'bootstrap-alert'

  class AppAlert extends Marionette.ItemView

    events:
      'click [data-event="dismiss"]': '_dismissAlert'

    template: Handlebars.compile appAlert

    onShow: ->
      options = @options
      $(@el).find('.alert').css('opacity', 0).slideDown( ->
        $(@).animate(opacity: 1)
        if options.scrollTo
          $('html, body').animate(
            scrollTop: $(@).offset().top - 80
          , 1000)
      )
      return

    _dismissAlert: (e) ->
      e.preventDefault()
      e.stopPropagation()
      $(@el).animate(opacity: 0, -> # OK to fadeOut() with jQuery because another alert will just overwrite the region if it needs to be shown
        $(@).slideUp()
      )
      return
