define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  followingCollectionItem = require 'text!templates/following/followingCollectionItem.html'

  class FollowingCollectionItem extends Marionette.ItemView

    className: 'col-md-6'

    attributes: ->
      'data-id': @model.get 'id'

    templateHelpers: ->
      helpers:
        app_user_on_own_page: do =>
          App.user.get('id') is @options.userId

    events:
      'click [data-event="user"]': '_showPostsPage'
      'mouseenter [data-event="hoverfollowing"]': '_hoverFollowingButton'
      'mouseleave [data-event="hoverfollowing"]': '_unHoverFollowingButton'
      'submit [data-event="unfollow"]': '_unfollow'

    template: Handlebars.compile followingCollectionItem

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _hoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-warning').addClass('btn-danger').find('span:not(.xhr)').text('Unfollow')

    _unHoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-danger').addClass('btn-warning').find('span:not(.xhr)').text('Following')

    _unfollow: (e) -> # Use caution here, jQuery in use...
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-warning').addClass('btn-danger')
      @model.unfollow().done( ->
        #App.user.following.count() # Moved to the actual model
      )
      $(@el).delay(200).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp()
      )
      return

    _showPostsPage: (e) ->
      e.preventDefault()
      e.stopPropagation()
      id = @model.get('id')
      App.appRouter.navigate(id + '/posts')
      require ['cs!coffee/views/user/UserPage', 'views/posts/PostsPage'], (UserPage, PostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: id
          route: 'posts'
          module: new PostsPage( userId: id )
        ))
        return
      return
