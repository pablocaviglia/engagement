define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  FollowingCollectionItem = require 'cs!coffee/views/following/FollowingCollectionItem'

  class FollowingCollection extends Marionette.CollectionView

    className: 'row'

    childView: FollowingCollectionItem

    childViewOptions: ->
      options =
        userId: @options.userId
