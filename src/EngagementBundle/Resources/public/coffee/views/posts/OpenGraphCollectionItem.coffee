define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  moment = require 'moment'
  openGraphCollectionItem = require 'text!templates/posts/openGraphCollectionItem.html'

  require 'bootstrap-transition'
  require 'bootstrap-tooltip'

  class OpenGraphCollectionItem extends Marionette.ItemView

    template: Handlebars.compile openGraphCollectionItem

    attributes: ->
      'data-id': @model.get 'id'

    onRender: ->
      $(@el).find('[data-toggle="tooltip"]').each( ->
        $(@).tooltip()
        return
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    events:
      'click [data-event="topper"]': '_closePreview'

    _closePreview: ->
      @triggerMethod('closePreview')
      return
