define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  moment = require 'moment'
  postsCollectionItemRead = require 'text!templates/posts/postsCollectionItemRead.html'
  Autolinker = require 'autolinker'
  showdown = require 'showdown'
  emojify = require 'emojify'

  require 'bootstrap-dropdown'
  require 'bootstrap-transition'
  require 'jquery-truncate'

  class PostsCollectionItemRead extends Marionette.ItemView

    template: Handlebars.compile postsCollectionItemRead

    events:
      'click [data-event="delete"]': '_deletePost'
      'click [data-event="edit"]': '_editPost'
      'click [data-event="more"]': '_readMore'
      'click [data-event="user"]': '_showPostsPage'
      'click [data-event="hashtag"]': '_hashtagSearch'
      'keyup [data-event="url"]': '_openGraphDetection'

    templateHelpers: ->
      helpers:
        app_href: do ->
          App.href
        created_date: do =>
          moment(@model.get('created_at')).format('MMM Do YYYY, h:mm A')
        meta_date_published: do =>
          moment(@model.get('created_at')).format('YYYY-MM-DD')
        content: do =>
          converter = new showdown.Converter(
            headerLevelStart: 3
            tables: false
          )
          converter.makeHtml(
            Autolinker.link($.truncate(emojify.replace(@model.get('content')),
                length: 750
                words: true
                stripTags: false
                ellipsis: '&#133; <a href="/" class="nowrap" data-event="more">read more &rsaquo;</a>'
              ),
              newWindow: true
              phone: false
              hashtag: 'twitter'
              replaceFn: (autolinker, match) ->
                switch match.getType()
                  when 'hashtag'
                    hashtag = match.getHashtag()
                    return '<a data-event="hashtag" href="/search?q=' + encodeURIComponent(match.matchedText) + '">' + match.matchedText + '</a>'
                return
            )
          )
        app_user_is_author: do =>
          @model.get('user').id is App.user.get('id')

    initialize: ->
      emojify.setConfig(
        img_dir: '/assets/images/emoji'
      )
      @listenTo(@model, 'impression', ->
        $(@el).find('.panel').addClass('blink')
        setTimeout( =>
          $(@el).find('.panel').removeClass('blink')
        , 2200)
      , @)

    onRender: ->
      if @model.has('open_graph_data') and @model.get('open_graph_data').ogImage
        image = new Image()
        image.addEventListener('load', =>
          $(@el).find('.og-placeholder').replaceWith('<img src="' + @model.get('open_graph_data').ogImage.url + '" />')
        )
        image.src = @model.get('open_graph_data').ogImage.url

      # Impressions
      $(window).on('scroll', @_isScrolledIntoView)
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      $(window).trigger('scroll')#@_isScrolledIntoView()
      return

    onDestroy: ->
      $(window).off('scroll', @_isScrolledIntoView)

    _isScrolledIntoView: =>
      setTimeout( =>
        $elem = $(@el)
        $window = $(window)
        $nav = $('nav.navbar-fixed-top')
        docViewTop = $window.scrollTop() + $nav.height()
        docViewBottom = docViewTop + $window.height() - $nav.height()
        elemTop = $elem.offset().top + 35 # 25 takes care of the top and bottom padding
        elemBottom = elemTop + $elem.height() - 35
        if (elemBottom >= docViewTop and elemTop <= docViewBottom and not @sentImpression)
          @sentImpression = true
          @model.sendImpression()
        else if (@sentImpression is true and elemBottom < docViewTop or elemTop > docViewBottom)
          @sentImpression = false
      , 1000)
      return

    _readMore: (e) ->
      e.preventDefault()
      converter = new showdown.Converter(
        headerLevelStart: 3
        tables: false
      )
      $(@el).find('[data-target="content"]').html(
        converter.makeHtml(Autolinker.link(
          emojify.replace(@model.get('content')),
          newWindow: true
          phone: false
          hashtag: 'twitter'
          replaceFn: (autolinker, match) ->
            switch match.getType()
              when 'hashtag'
                hashtag = match.getHashtag()
                return '<a data-event="hashtag" href="/search?q=' + encodeURIComponent(match.matchedText) + '">' + match.matchedText + '</a>'
            return
        ))
      )
      return

    _showPostsPage: (e) ->
      e.preventDefault()
      e.stopPropagation()
      id = @model.get('user').id
      App.appRouter.navigate(id + '/posts')
      require ['cs!coffee/views/user/UserPage', 'views/posts/PostsPage'], (UserPage, PostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: id
          route: 'posts'
          module: new PostsPage( userId: id )
        ))
        return
      return

    _hashtagSearch: (e) ->
      e.preventDefault()
      target = $(e.currentTarget)
      App.appRouter.navigate(target.attr('href'))
      require ['cs!coffee/views/Main', 'views/search/SearchPage'], (Main, SearchPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new SearchPage(q: target.text()))
        return
      return

    _editPost: (e) ->
      e.preventDefault()
      @triggerMethod('editPost')
      return

    _deletePost: (e) ->
      e.preventDefault()
      $(@el).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp(=>
          @model.destroy({ silent: true }).done( =>
            @triggerMethod('postDeleted') # Let the collection view know that a post was deleted so the offset can be adjusted
            return
          )
        )
      )
      return
