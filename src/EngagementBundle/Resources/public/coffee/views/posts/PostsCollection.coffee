define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  PostsCollectionItem = require 'cs!coffee/views/posts/PostsCollectionItem'

  class PostsCollection extends Marionette.CollectionView

    childView: PostsCollectionItem

    childEvents:
      postDeleted: ->
        @collection.state.offset-- # A post was deleted so drop the offset by 1 so the lazy loading doesn't miss any posts
        return
