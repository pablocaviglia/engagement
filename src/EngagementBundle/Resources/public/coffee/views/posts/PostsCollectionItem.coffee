define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  postsCollectionItem = require 'text!templates/posts/postsCollectionItem.html'
  PostsCollectionItemRead = require 'cs!coffee/views/posts/PostsCollectionItemRead'

  class PostsCollectionItem extends Marionette.LayoutView

    attributes: ->
      'data-id': @model.get 'id'

    template: Handlebars.compile postsCollectionItem

    regions:
      postRegion: '[data-region="post-region"]'

    onRender: ->
      @getRegion('postRegion').show(new PostsCollectionItemRead( model: @model ))
      return

    childEvents:
      editPost: ->
        require ['cs!coffee/views/posts/PostsCollectionItemEdit'], (PostsCollectionItemEdit) =>
          @getRegion('postRegion').show(new PostsCollectionItemEdit( model: @model ))
          return
        return
      cancelEditPost: ->
        @getRegion('postRegion').show(new PostsCollectionItemRead( model: @model ))
        return
      editPostSuccess: ->
        @getRegion('postRegion').show(new PostsCollectionItemRead( model: @model ))
        return
