define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  md5 = require 'md5'
  OpenGraphCollection = require 'cs!coffee/views/posts/OpenGraphCollection'
  OpenGraph = require 'cs!coffee/models/OpenGraph'
  OpenGraphs = require 'cs!coffee/collections/OpenGraphs'
  postsCollectionItemEdit = require 'text!templates/posts/postsCollectionItemEdit.html'

  class PostsCollectionItemEdit extends Marionette.LayoutView

    template: Handlebars.compile postsCollectionItemEdit

    events:
      'click [data-event="cancel"]': '_cancelEditPost'
      'submit [data-event="update"]': '_updatePost'
      'keyup [data-event="url"]': '_openGraphDetection'

    regions:
      postEditAlertRegion: '[data-region="post-edit-alert-region"]'
      openGraphRegion: '[data-region="open-graph-region"]'

    initialize: ->
      @opengraphcollection = new OpenGraphs()
      @opengraphverifiedcollection = new OpenGraphs()
      if @model.has('open_graph_data')
        ogUrl = @model.get('open_graph_data').ogUrl
        id = md5(ogUrl)
        @opengraphcollection.add(new OpenGraph(
          id: id
          url: ogUrl
        ))
        @opengraphverifiedcollection.add(new OpenGraph( # Add the model to the open graph collection
          id: id
          url: ogUrl
          data: @model.get('open_graph_data')
          success: true
        ))
      return

    onRender: ->
      if @model.has('open_graph_data')
        @getRegion('openGraphRegion').show(new OpenGraphCollection( collection: @opengraphverifiedcollection ))
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _cancelEditPost: (e) ->
      e.preventDefault()
      @triggerMethod('cancelEditPost')
      return

    _openGraphDetection: (e) ->
      urlRegex = /(https?:\/\/(?:www\.|(?!www))[^\s\.]+\.[^\s]{2,}|www\.[^\s]+\.[^\s]{2,})/ig
      urls = $(e.currentTarget).val().match(urlRegex)

      if urls and urls.length
        _.each(urls, (url) =>
          id = md5(url) # We need a unique Id as a way of making sure we don't have duplicate URLs in the collection
          @opengraphcollection.add(new OpenGraph( # Add the model to the open graph collection
            id: id
            url: url
          ))
          if not @opengraphcollection.get(id).has('success') # A model without a "success" true/false has not been fetched
            if @opengraphverifiedcollection.models.length is 0 # Only fade in the load if there is not already a preview
              $(@el).find('[data-loader="og"]').fadeIn()
              @opengraphcollection.get(id).fetch(
                timeout: 6000
                success: (model, response) =>
                  $(@el).find('[data-loader="og"]').fadeOut( =>
                    if model.has('success') and model.get('success') is true and model.has('data') and model.get('data').ogUrl and model.get('data').ogType and model.get('data').ogImage and model.get('data').ogImage.url and model.get('data').ogDescription and (model.get('data').ogType is 'article' or model.get('data').ogType is 'website') # Require some default data
                      if @opengraphverifiedcollection.models.length is 0
                        @opengraphverifiedcollection.add(model) #if the response meets all the criteria, add it to the verified collection that will display in the preview
                      if @opengraphverifiedcollection.models.length is 1 # Only allow 1 preview at a time for now but this sets it up for multiple previews if needed
                        @getRegion('openGraphRegion').show(new OpenGraphCollection( collection: @opengraphverifiedcollection ))
                    return
                  )
                error: =>
                  $(@el).find('[data-loader="og"]').fadeOut()
              )
        )

    _updatePost: (e) ->
      e.preventDefault()
      data = _.object(_.map($(e.currentTarget).serializeArray(), _.values)) # An underscore methed to serialize the form into an object
      $(e.currentTarget).find('fieldset').prop('disabled', true) # This needs to be after the data is collected!
      if not $.trim(data.content) or not $.trim(data.title)
        require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
          $(e.currentTarget).find('fieldset').prop('disabled', false)
          @getRegion('postEditAlertRegion').show new AppAlert(scrollTo: true, model: new Backbone.Model(
            type: 'danger'
            text: 'The post needs a title and content.'
            dismissable: true
          ))
          return
      else
        #data.user = id: App.user.get('id')
        #if @options.contentGroupId # If this view is on a content group page it should have a content group id in the options
        #  data.content_group = { id: @options.contentGroupId }
        if @opengraphverifiedcollection.models.length
          data.open_graph_data = @opengraphverifiedcollection.first().get('data')
        #@post = new Post(data)
        if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
          @xhr.abort()
        @xhr = @model.save data,
          success: =>
            $(@el).fadeOut( =>
              @triggerMethod 'editPostSuccess'
              return
            )
            return
          error: (model, response) =>
            $(e.currentTarget).find('fieldset').prop('disabled', false)
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The post could not be edited at this time.'
              @getRegion('postEditAlertRegion').show new AppAlert(scrollTo: true, model: new Backbone.Model(
                type: 'danger'
                text: text
                dismissable: true
              ))
              return
            return
      return
