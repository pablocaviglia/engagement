define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Posts = require 'cs!coffee/collections/Posts'
  PostsCollection = require 'cs!coffee/views/posts/PostsCollection'
  postsPage = require 'text!templates/posts/postsPage.html'
  User = require 'cs!coffee/models/User'

  class PostsPage extends Marionette.LayoutView

    template: Handlebars.compile postsPage

    regions:
      postsAlertRegion: '[data-region="posts-alert-region"]'
      postsCollectionRegion: '[data-region="posts-collection-region"]'

    initialize: ->
      @collection = new Posts([], { userId: @options.userId })
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('postsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: do =>
                  if @options.userId is App.user.get('id')
                    return 'You have not created any posts.'
                  else
                    return 'The user has not created any posts.'
                dismissable: false
              ))
              return
          @getRegion('postsCollectionRegion').show(new PostsCollection(
            collection: @collection
            userId: @options.userId
          ))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The posts could not be loaded at this time.'
            @getRegion('postsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    onDestroy: ->
      $(window).off('scroll', @_lazyListening)
      return

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
