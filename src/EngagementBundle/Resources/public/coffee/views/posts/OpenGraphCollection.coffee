define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  OpenGraphCollectionItem = require 'cs!coffee/views/posts/OpenGraphCollectionItem'
  openGraphCollection = require 'text!templates/posts/openGraphCollection.html'

  class OpenGraphCollection extends Marionette.CompositeView

    template: Handlebars.compile openGraphCollection

    childViewContainer: '.child-view'

    childView: OpenGraphCollectionItem

    childEvents:
      closePreview: (view) ->
        $(@el).fadeOut( =>
          @collection.remove(@collection.where(id: view.model.get('id')))
          return
        )
        return
