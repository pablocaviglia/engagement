define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Users = require 'cs!coffee/collections/Users'
  UsersTable = require 'cs!coffee/views/admin/users/UsersTable'
  usersPage = require 'text!templates/admin/users/usersPage.html'

  class UsersPage extends Marionette.LayoutView

    template: Handlebars.compile usersPage

    regions:
      usersAlertRegion: '[data-region="users-alert-region"]'
      usersTableRegion: '[data-region="users-table-region"]'

    initialize: ->
      @collection = new Users()
      return

    onRender: ->
      @collection.count().done( =>
        $.when(@collection.fetch()).done( =>
          $(@el).find('[data-loader="initial"]').fadeOut( =>
            @getRegion('usersTableRegion').show(new UsersTable(collection: @collection))
          )
          return
        ).fail((response) =>
          $(@el).find('[data-loader="initial"]').fadeOut( =>
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The users could not be loaded at this time.'
              @getRegion('usersAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'danger'
                text: text
                dismissable: true
              ))
              return
            return
          )
          return
        )
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The users could not be loaded at this time.'
            @getRegion('usersAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return
