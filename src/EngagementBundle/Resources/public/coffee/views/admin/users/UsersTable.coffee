define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  usersTable = require 'text!templates/admin/users/usersTable.html'
  Backgrid = require 'backgrid'

  require 'backgrid-paginator'

  class UsersTable extends Marionette.LayoutView

    template: Handlebars.compile usersTable

    onRender: ->
      ViewCell = Backgrid.Cell.extend(
        template: Handlebars.compile('<a role="button" class="btn btn-warning" href="/admin/users/{{id}}">View</a>')
        render: ->
          @$el.html @template(@model.toJSON())
          @$el.css
            width: '80px'
            'text-align': 'center'
          @delegateEvents()
          this
      )
      IndexCell = Backgrid.Cell.extend(render: ->
        @$el.text @model.collection.indexOf(@model) + 1 + @model.collection.state.currentPage * @model.collection.state.pageSize
        @$el.css width: '50px'
        this
      )
      grid = new (Backgrid.Grid)(
        columns: [
          {
            name: ''
            label: ''
            cell: IndexCell
            sortable: false
            editable: false
          }
          {
            label: 'Last Name'
            cell: 'string'
            sortable: false
            editable: false
            formatter: _.extend({}, Backgrid.CellFormatter.prototype, fromRaw: (rawValue, model) ->
              model.get('last')
            )
          }
          {
            label: 'First Name'
            cell: 'string'
            sortable: false
            editable: false
            formatter: _.extend({}, Backgrid.CellFormatter.prototype, fromRaw: (rawValue, model) ->
              model.get('first')
            )
          }
          {
            label: ''
            cell: ViewCell
            sortable: false
            editable: false
          }
        ]
        collection: @collection
        className: 'backgrid table table-striped table-bordered table-hover')

      topPagination = new (Backgrid.Extension.Paginator)(
        collection: @collection
        controls:
          rewind:
            label: '&lsaquo;&lsaquo;'
            title: 'First'
          back:
            label: '&lsaquo;'
            title: 'Previous'
          forward:
            label: '&rsaquo;'
            title: 'Next'
          fastForward:
            label: '&rsaquo;&rsaquo;'
            title: 'Last')

      bottomPagination = new (Backgrid.Extension.Paginator)(
        collection: @collection
        controls:
          rewind:
            label: '&lsaquo;&lsaquo;'
            title: 'First'
          back:
            label: '&lsaquo;'
            title: 'Previous'
          forward:
            label: '&rsaquo;'
            title: 'Next'
          fastForward:
            label: '&rsaquo;&rsaquo;'
            title: 'Last')

      $(@el).find('.panel-body').append grid.render().$el
      $(@el).find('.panel-body').append bottomPagination.render().$el
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return
