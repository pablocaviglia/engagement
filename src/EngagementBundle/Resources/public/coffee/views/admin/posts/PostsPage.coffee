define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Posts = require 'cs!coffee/collections/Posts'
  PostsTable = require 'cs!coffee/views/admin/posts/PostsTable'
  postsPage = require 'text!templates/admin/posts/postsPage.html'

  class PostsPage extends Marionette.LayoutView

    template: Handlebars.compile postsPage

    regions:
      postsAlertRegion: '[data-region="posts-alert-region"]'
      postsTableRegion: '[data-region="posts-table-region"]'

    initialize: ->
      @collection = new Posts()
      return

    onRender: ->
      @collection.count().done( =>
        $.when(@collection.fetch()).done( =>
          $(@el).find('[data-loader="initial"]').fadeOut( =>
            @getRegion('postsTableRegion').show(new PostsTable(collection: @collection))
          )
          return
        ).fail((response) =>
          $(@el).find('[data-loader="initial"]').fadeOut( =>
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The posts could not be loaded at this time.'
              @getRegion('postsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'danger'
                text: text
                dismissable: true
              ))
              return
            return
          )
          return
        )
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The posts could not be loaded at this time.'
            @getRegion('postsAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )

      return
