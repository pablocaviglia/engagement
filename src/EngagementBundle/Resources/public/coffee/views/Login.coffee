define (require) ->

  # $ = require 'jquery'
  _ = require 'underscore'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  User = require 'cs!coffee/models/User'
  login = require 'text!templates/login.html'
  AppUserFollowing = require 'cs!coffee/collections/AppUserFollowing'
  AppUserFollowers = require 'cs!coffee/collections/AppUserFollowers'
  AppUserGroups = require 'cs!coffee/collections/AppUserGroups'

  class Login extends Marionette.LayoutView

    template: Handlebars.compile login

    events:
      'submit [data-event="login"]': '_loginUser'

    regions:
      loginAlertRegion: '[data-region="login-alert-region"]'

    onRender: ->
      $('body').removeClass('authenticated')
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)

      # Select a random user for login
      selected = Math.floor(Math.random() * 5)
      $(@el).find('select > option:eq(' + selected + ')').attr('selected', true)
      return
 
    _loginUser: (e) ->
      e.preventDefault()
      data = _.object(_.map($(e.currentTarget).serializeArray(), _.values)) # An underscore methed to serialize the form into an object
      $(e.currentTarget).find('fieldset').prop 'disabled', true # This needs to be after the data is collected!
      if @xhr and @xhr.readyState > 0 and @xhr.readyState < 4
        @xhr.abort()

      # Temporary login (hitting the users API and grabbing the preferred user)
      user = new User( id: data.id )

      # Set up all the ajax requests to correspond to the latest user id
      $.ajaxPrefilter (options) ->
        if options.url.indexOf('/api/') is 0
          options.beforeSend = (xhr) ->
            xhr.setRequestHeader 'X-Engagement-Platform', 'web'
            xhr.setRequestHeader 'X-Engagement-Browser', navigator.userAgent
            xhr.setRequestHeader 'X-Engagement-User-Id', data.id
            return
        return

      @xhr = user.fetch()

      # Grab the user followers, followings, and group followings (possibly temporary until we add data on the calls that gives us more info about followed users and groups)
      following = new AppUserFollowing([], { userId: data.id })
      followers = new AppUserFollowers([], { userId: data.id })
      groups = new AppUserGroups([], { userId: data.id })

      $.when(@xhr, following.fetch(), followers.fetch(), groups.fetch()).done( =>
        App.user = user
        App.user.following = following
        App.user.followers = followers
        App.user.groups = groups
        App.trigger('athenticate:done',
          callback: @options.callback
          args: @options.args
          name: @options.name
        )
        return
      ).fail((response) =>
        $(e.currentTarget).find('fieldset').prop('disabled', false)
        require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
          text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The system was unable to validate the username. Please try again later.'
          @getRegion('loginAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
            type: 'danger'
            text: text
            dismissable: true
          ))
          return
        return
      )
      return
