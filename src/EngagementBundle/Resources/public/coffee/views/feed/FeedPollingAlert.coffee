define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  feedPollingAlert = require 'text!templates/feed/feedPollingAlert.html'

  require 'bootstrap-alert'

  class FeedPollingAlert extends Marionette.ItemView

    events:
      'click [data-event="feeds"]': '_loadBackgroundPosts'

    template: Handlebars.compile feedPollingAlert

    templateHelpers: ->
      helpers:
        new_posts: do =>
          @collection.models.length
        is_new_posts_plural: do =>
          if @collection.models.length > 1 then true else false
        is_rendered: do =>
          @options.rendered

    initialize: ->
      @listenTo @collection, 'add',  ( -> # Re-render in case the user doesn't refresh and the count goes up
        @options.rendered = true
        @render()
      ), @
      return

    onShow: ->
      $(@el).find('.alert').css('opacity', 0).slideDown( ->
        $(@).animate(opacity: 1)
        # $('html, body').animate(
        #   scrollTop: $(@).offset().top - 80
        # , 1000)
      )
      return

    _loadBackgroundPosts: (e) ->
      e.preventDefault()
      $(@el).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp( =>
          @triggerMethod('loadBackgroundPosts')
          return
        )
      )
      return
