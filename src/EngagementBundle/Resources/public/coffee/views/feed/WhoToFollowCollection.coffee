define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  WhoToFollowItem = require 'cs!coffee/views/feed/WhoToFollowItem'
  whoToFollow = require 'text!templates/feed/whoToFollowCollection.html'

  class WhoToFollow extends Marionette.CompositeView

    template: Handlebars.compile whoToFollow

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    childViewContainer: '.child-view'

    childView: WhoToFollowItem

    childEvents:
      follow: (view) ->
        @collection.remove(@collection.where(id: view.model.get('id')))
        @collection.lazyFetch().done(=>
          if not @collection.models.length
            $(@el).fadeOut()
          return
        )

        return
      dismissUser: (view) -> # Waiting on API call
        @collection.remove(@collection.where(id: view.model.get('id')))
        @collection.lazyFetch().done(=>
          if not @collection.models.length
            $(@el).fadeOut()
          return
        )
        return
