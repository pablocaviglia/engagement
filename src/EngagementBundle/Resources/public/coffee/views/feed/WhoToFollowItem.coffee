define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  #Follow = require 'cs!coffee/models/Follow'
  whoToFollowItem = require 'text!templates/feed/whoToFollowItem.html'

  class WhoToFollowItem extends Marionette.ItemView

    className: 'separator'

    attributes: ->
      'data-id': @model.get 'id'

    template: Handlebars.compile whoToFollowItem

    events:
      'submit [data-event="follow"]': '_follow'
      'click [data-event="dismiss"]': '_dismissUser'
      'click [data-event="user"]': '_showPostsPage'

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _follow: (e) ->
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-default').addClass('btn-warning')
      @model.follow().done( ->
        App.user.following.count()
      )
      $(@el).delay(200).animate(opacity: 0, => # No need to wait for a response, just remove the user
        $(@el).slideUp(=>
          @triggerMethod('follow')
        )
      )
      return

    _dismissUser: ->
      $(@el).animate(opacity: 0, =>
        $(@el).slideUp(=>
          @triggerMethod('dismissUser')
        )
      )
      return

    _showPostsPage: (e) ->
      e.preventDefault()
      e.stopPropagation()
      id = @model.get('id')
      App.appRouter.navigate(id + '/posts')
      require ['cs!coffee/views/user/UserPage', 'views/posts/PostsPage'], (UserPage, PostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: id
          route: 'posts'
          module: new PostsPage( userId: id )
        ))
        return
      return
