define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Posts = require 'cs!coffee/collections/Posts'
  PostsCollection = require 'cs!coffee/views/posts/PostsCollection'
  SharePost = require 'cs!coffee/views/posts/SharePost'
  WhoToFollow = require 'cs!coffee/collections/WhoToFollow'
  WhoToFollowCollection = require 'cs!coffee/views/feed/WhoToFollowCollection'
  feedPage = require 'text!templates/feed/feedPage.html'

  class FeedPage extends Marionette.LayoutView

    template: Handlebars.compile feedPage

    regions:
      feedAlertRegion: '[data-region="feed-alert-region"]'
      feedPollingAlertRegion: '[data-region="feed-polling-alert-region"]'
      feedCollectionRegion: '[data-region="feed-collection-region"]'
      sharePostRegion: '[data-region="share-post-region"]'
      whoToFollowRegion: '[data-region="who-to-follow-region"]'

    initialize: ->
      @collection = new Posts()
      @whotofollow = new WhoToFollow([], { userId: App.user.get('id') })
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('feedAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: 'There are no posts in your feed.'
                dismissable: false
              ))
              return
          @getRegion('feedCollectionRegion').show(new PostsCollection(collection: @collection))

          # Still show the share post view
          @getRegion('sharePostRegion').show(new SharePost(
            open_graph_data: @options.open_graph_data
          ))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The feed could not be loaded at this time.'
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            @getRegion('feedAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      # This doesn't necessarily have to happen so going to go with a separate call instead of throwing it in the main feed call
      $.when(@whotofollow.fetch()).done( =>
        if @whotofollow.models.length
          @getRegion('whoToFollowRegion').show(new WhoToFollowCollection(collection: @whotofollow))
        return
      ).fail( ->
        
      )
      @render_time = (new Date()).getTime()
      @polling = setInterval(@_polling, 60000)
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    childEvents:
      submitPostSuccess: (view) ->
        @collection.add view.post, at: 0
        @getRegion('feedAlertRegion').empty()
        @getRegion('sharePostRegion').show(new SharePost())
        return
      loadBackgroundPosts: (view) ->
        clearInterval(@polling)
        @collection.add(@polling_collection.models, at: 0)
        @getRegion('feedAlertRegion').empty() # Emptying this view in case it tells the user there are no posts
        @getRegion('feedPollingAlertRegion').empty()
        @polling_collection.reset()
        @polling = setInterval(@_polling, 60000)
        return

    onDestroy: ->
      $(window).off('scroll', @_lazyListening)
      clearInterval(@polling)

    _polling: =>
      require ['collections/PollingFeed'], (PollingFeed) => # Require this module later because it is not needed until the first poll
        if not @polling_collection
          @polling_collection = new PollingFeed([], { userId: App.user.get('id') })
        @col_length = @polling_collection.models.length
        @polling_collection.fetch(
          remove: false
          data:
            created_at: @render_time
        ).done( =>
          if @col_length isnt @polling_collection.models.length and not @getRegion('feedPollingAlertRegion').currentView # Only show the alert if posts were returned and only show the alert if there is not already an alert in the region (it will rerender itself if more posts are found)
            require ['cs!coffee/views/feed/FeedPollingAlert'], (FeedPollingAlert) =>
              @getRegion('feedPollingAlertRegion').show new FeedPollingAlert(collection: @polling_collection)
              return
        )
        @render_time = (new Date()).getTime()
        return
      return

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
