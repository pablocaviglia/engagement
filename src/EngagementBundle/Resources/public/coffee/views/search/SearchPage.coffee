define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Posts = require 'cs!coffee/collections/Posts'
  PostsCollection = require 'cs!coffee/views/posts/PostsCollection'
  WhoToFollow = require 'cs!coffee/collections/WhoToFollow'
  WhoToFollowCollection = require 'cs!coffee/views/feed/WhoToFollowCollection'
  searchPage = require 'text!templates/search/searchPage.html'

  class SearchPage extends Marionette.LayoutView

    template: Handlebars.compile searchPage

    regions:
      searchAlertRegion: '[data-region="search-alert-region"]'
      searchPollingAlertRegion: '[data-region="search-polling-alert-region"]'
      searchCollectionRegion: '[data-region="search-collection-region"]'
      whoToFollowRegion: '[data-region="who-to-follow-region"]'

    initialize: ->
      @collection = new Posts([], { q: @options.q })
      @whotofollow = new WhoToFollow([], { userId: App.user.get('id') })
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('searchAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: 'The search did not return any results.'
                dismissable: false
              ))
              return
          @getRegion('searchCollectionRegion').show(new PostsCollection(collection: @collection))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The search results could not be loaded at this time.'
            @getRegion('searchAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      # This doesn't necessarily have to happen so going to go with a separate call instead of throwing it in the main feed call
      $.when(@whotofollow.fetch()).done( =>
        if @whotofollow.models.length
          @getRegion('whoToFollowRegion').show(new WhoToFollowCollection(collection: @whotofollow))
        return
      ).fail( ->
        
      )
      @render_time = (new Date()).getTime()
      @polling = setInterval(@_polling, 60000)
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    childEvents:
      loadBackgroundPosts: (view) ->
        clearInterval(@polling)
        @collection.add(@polling_collection.models, at: 0)
        @getRegion('searchAlertRegion').empty() # Emptying this view in case it tells the user there are no posts
        @getRegion('searchPollingAlertRegion').empty()
        @polling_collection.reset()
        @polling = setInterval(@_polling, 60000)
        return

    onDestroy: ->
      $(window).off('scroll', @_lazyListening)
      clearInterval(@polling)

    _polling: =>
      require ['collections/PollingSearch'], (PollingSearch) => # Require this module later because it is not needed until the first poll
        if not @polling_collection
          @polling_collection = new PollingSearch([], { q: @options.q })
        @col_length = @polling_collection.models.length
        @polling_collection.fetch(
          remove: false
          data:
            created_at: @render_time
        ).done( =>
          if @col_length isnt @polling_collection.models.length and not @getRegion('searchPollingAlertRegion').currentView # Only show the alert if posts were returned and only show the alert if there is not already an alert in the region (it will rerender itself if more posts are found)
            require ['cs!coffee/views/search/SearchPollingAlert'], (SearchPollingAlert) =>
              @getRegion('searchPollingAlertRegion').show new SearchPollingAlert(collection: @polling_collection)
              return
        )
        @render_time = (new Date()).getTime()
        return
      return

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
