define (require) ->

  # $ = require 'jquery'
  _ = require 'underscore'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  admin = require 'text!templates/admin.html'

  require 'bootstrap-transition'
  require 'bootstrap-dropdown'
  require 'bootstrap-modal'

  class Admin extends Marionette.LayoutView

    template: Handlebars.compile admin

    events:
      'click [data-event="home"]': '_preventDefault' # Does nothing for now
      'click [data-event="signout"]': '_signOutUser'
      'click [data-event="users"]': '_showUsersPage'
      'click [data-event="posts"]': '_showPostsPage'
      'click [data-event="feed"]': '_showMainLayout'

    regions:
      pageRegion: '#page'
      modalRegion: '#modal'

    initialize: ->
      @model = App.user
      @listenTo(App.vent, 'modal', (view) ->
        @getRegion('modalRegion').show(view)
        $('#modal').modal()
        return
      , @)

      @listenTo(App.vent, 'navigate:admin:users', (options) ->
        @_showUsersPage()
      , @)

      @listenTo(App.vent, 'navigate:admin:posts', (options) ->
        @_showPostsPage()
      , @)

      return

    onRender: ->
      $('body').addClass('authenticated')
      $(@el).find('[data-nav="' + @options.nav + '"]').addClass('active')
      @getRegion('pageRegion').show @options.module
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _showUsersPage: (e, options) ->
      if e.target
        e.preventDefault()
      else
        options = e
      App.appRouter.navigate('admin/users')
      $(@el).find('[data-nav]').removeClass('active')
      $(@el).find('[data-nav="users"]').addClass('active')
      require ['cs!coffee/views/admin/users/UsersPage'], (UsersPage) =>
        @getRegion('pageRegion').show new UsersPage(options)
        return
      return

    _showPostsPage: (e, options) ->
      if e.target
        e.preventDefault()
      else
        options = e
      App.appRouter.navigate('admin/posts')
      $(@el).find('[data-nav]').removeClass('active')
      $(@el).find('[data-nav="posts"]').addClass('active')
      require ['cs!coffee/views/admin/posts/PostsPage'], (PostsPage) =>
        @getRegion('pageRegion').show new PostsPage(options)
        return
      return

    _showMainLayout: (e, options) ->
      if e.target
        e.preventDefault()
      else
        options = e
      App.appRouter.navigate('/')
      require ['cs!coffee/views/Main', 'views/feed/FeedPage'], (Main, FeedPage) ->
        App.layout.getRegion('appRegion').show new Main(
          module: new FeedPage()
        )
        return
      return

    _preventDefault: (e) ->
      e.preventDefault()
      return

    _signOutUser: (e) ->
      e.preventDefault()
      App.trigger 'unauthenticate'
      return
