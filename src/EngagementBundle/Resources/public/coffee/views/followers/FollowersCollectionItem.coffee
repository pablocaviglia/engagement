define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  FollowersCollectionItemButton = require 'cs!coffee/views/followers/FollowersCollectionItemButton'
  followersCollectionItem = require 'text!templates/followers/followersCollectionItem.html'

  class FollowersCollectionItem extends Marionette.LayoutView

    className: 'col-md-6'

    attributes: ->
      'data-id': @model.get 'id'

    events:
      'click [data-event="user"]': '_showPostsPage'

    regions:
      followersButtonRegion: '[data-region="followers-button-region"]'

    template: Handlebars.compile followersCollectionItem

    onRender: ->
      @getRegion('followersButtonRegion').show(new FollowersCollectionItemButton(
        model: @model
        userId: @options.userId
      ))

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _showPostsPage: (e) -> # Needed for a name click, button view also has the same method
      e.preventDefault()
      e.stopPropagation()
      id = @model.get('id')
      App.appRouter.navigate(id + '/posts')
      require ['cs!coffee/views/user/UserPage', 'views/posts/PostsPage'], (UserPage, PostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: id
          route: 'posts'
          module: new PostsPage( userId: id )
        ))
        return
      return
