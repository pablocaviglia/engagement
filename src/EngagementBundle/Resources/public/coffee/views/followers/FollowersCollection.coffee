define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  FollowersCollectionItem = require 'cs!coffee/views/followers/FollowersCollectionItem'

  class FollowersCollection extends Marionette.CollectionView

    className: 'row'

    childView: FollowersCollectionItem

    childViewOptions: ->
      options =
        userId: @options.userId
