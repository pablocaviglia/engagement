define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  followersCollectionItemButton = require 'text!templates/followers/followersCollectionItemButton.html'

  class FollowersCollectionItemButton extends Marionette.ItemView

    templateHelpers: ->
      helpers:
        app_user_on_own_page: do =>
          App.user.get('id') is @options.userId
        app_user_following: do =>
          App.user.following.get(@model.get('id'))

    events:
      'submit [data-event="unfollow"]': '_unfollow'
      'submit [data-event="follow"]': '_follow'
      'mouseenter [data-event="hoverfollowing"]': '_hoverFollowingButton'
      'mouseleave [data-event="hoverfollowing"]': '_unHoverFollowingButton'
      'click [data-event="user"]': '_showPostsPage'

    template: Handlebars.compile followersCollectionItemButton

    _hoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-warning').addClass('btn-danger').find('span:not(.xhr)').text('Unfollow')

    _unHoverFollowingButton: (e) -> # Use caution here, jQuery in use...
      $(e.currentTarget).removeClass('btn-danger').addClass('btn-warning').find('span:not(.xhr)').text('Following')

    _unfollow: (e) -> # Use caution here, jQuery in use...
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-warning').addClass('btn-danger')
      @model.unfollow().done( =>
        @render()
      )
      return

    _follow: (e) ->
      e.preventDefault()
      $(e.currentTarget).find('fieldset').prop('disabled', true)
      $(e.currentTarget).find('button').removeClass('btn-default').addClass('btn-warning')
      @model.follow().done( =>
        @render()
      )
      return

    _showPostsPage: (e) ->
      e.preventDefault()
      e.stopPropagation()
      id = @options.userId
      App.appRouter.navigate(id + '/posts')
      require ['cs!coffee/views/user/UserPage', 'views/posts/PostsPage'], (UserPage, PostsPage) ->
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: id
          route: 'posts'
          module: new PostsPage( userId: id )
        ))
        return
      return
