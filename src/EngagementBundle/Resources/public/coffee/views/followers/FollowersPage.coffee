define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Followers = require 'cs!coffee/collections/Followers'
  FollowersCollection = require 'cs!coffee/views/followers/FollowersCollection'
  followersPage = require 'text!templates/followers/followersPage.html'
  User = require 'cs!coffee/models/User'

  class FollowersPage extends Marionette.LayoutView

    template: Handlebars.compile followersPage

    regions:
      followersAlertRegion: '[data-region="followers-alert-region"]'
      followersCollectionRegion: '[data-region="followers-collection-region"]'

    initialize: ->
      @collection = new Followers([], { userId: @options.userId })
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('followersAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: do =>
                  if @options.userId is App.user.get('id')
                    return 'You do not have any followers.'
                  else
                    return 'The user does not have any followers.'
                dismissable: false
              ))
              return
          @getRegion('followersCollectionRegion').show(new FollowersCollection(
            collection: @collection
            userId: @options.userId
          ))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The feed could not be loaded at this time.'
            @getRegion('followersAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )
      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    onDestroy: ->
      $(window).off('scroll', @_lazyListening)

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
