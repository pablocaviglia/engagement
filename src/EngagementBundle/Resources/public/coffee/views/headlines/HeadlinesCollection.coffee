define (require) ->

  _ = require 'underscore'
  Marionette = require 'marionette'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  Masonry = require 'masonry'
  HeadlinesCollectionItem = require 'cs!coffee/views/headlines/HeadlinesCollectionItem'

  class HeadlinesCollection extends Marionette.CollectionView

    className: 'masonry'

    childView: HeadlinesCollectionItem

    childEvents:
      onShowChild: ->
        if not @waitingForChildren
          @waitingForChildren = true
          setTimeout( =>
            @masonry = new Masonry(document.querySelector('.masonry'),
              itemSelector: '.masonry-block'
              gridSizer: '.masonry-block'
              percentPosition: true
            )
            @waitingForChildren = false
          , 1050)
        return
