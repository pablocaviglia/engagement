define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  Headlines = require 'cs!coffee/collections/Headlines'
  HeadlinesCollection = require 'cs!coffee/views/headlines/HeadlinesCollection'
  headlinesPage = require 'text!templates/headlines/headlinesPage.html'

  class HeadlinesPage extends Marionette.LayoutView

    template: Handlebars.compile headlinesPage

    regions:
      headlinesAlertRegion: '[data-region="headlines-alert-region"]'
      headlinesCollectionRegion: '[data-region="headlines-collection-region"]'

    initialize: ->
      @collection = new Headlines()
      return

    onRender: ->
      $.when(@collection.fetch()).done( =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          if @collection.models.length is 0
            require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
              @getRegion('headlinesAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
                type: 'warning'
                text: 'The list did not return any results.'
                dismissable: false
              ))
              return
          @getRegion('headlinesCollectionRegion').show(new HeadlinesCollection(collection: @collection))
          setTimeout(=>
            $(window).on('scroll', @_lazyListening)
            return
          , 600)
        )
        return
      ).fail((response) =>
        $(@el).find('[data-loader="initial"]').fadeOut( =>
          require ['cs!coffee/views/app/AppAlert'], (AppAlert) =>
            text = if response and response.responseJSON and response.responseJSON.managed then response.exception.message else 'The list could not be loaded at this time.'
            @getRegion('headlinesAlertRegion').show new AppAlert(scrollTo: false, model: new Backbone.Model(
              type: 'danger'
              text: text
              dismissable: true
            ))
            return
          return
        )
        return
      )

      return

    onShow: ->
      setTimeout( =>
        $(@el).find('.animated').not($(@el).find('[data-region] .animated')).removeClass('animated')
      , 600)
      return

    _lazyListening: =>
      if @collection.state.exhausted
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').slideUp(->
          $(@).fadeOut()
        )
      else if $(window).scrollTop() and ($(window).scrollTop() is $(document).height() - $(window).height())
        $(window).off('scroll', @_lazyListening)
        $(@el).find('[data-loader="lazy"]').fadeIn() # Need to use jQuery fadeIn() / fadeOut() in order to transition between the two states
        setTimeout( =>
          @collection.lazyFetch().done( =>
            $(@el).find('[data-loader="lazy"]').fadeOut()
            $(window).on('scroll', @_lazyListening)
            return
          )
          return
        , 600)
      return
