define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  App = require 'cs!coffee/App'
  Handlebars = require 'handlebars'
  moment = require 'moment'
  headlinesCollectionItem = require 'text!templates/headlines/headlinesCollectionItem.html'

  class HeadlinesCollectionItem extends Marionette.LayoutView

    className: 'invisible masonry-block col-xs-12 col-sm-6 col-md-6 col-lg-4'

    template: Handlebars.compile headlinesCollectionItem

    regions:
      openGraphDataRegion: '.open-graph-data-region'

    events:
      'click [data-event="share"]': '_shareHeadline'

    attributes: ->
      'data-id': @model.get 'id'

    onRender: ->
      ###
      Doing things a bit different than the normal CSS animation
      We use visibility hidden so Masonry can get the heights
      But that means jQuery is needed to fade the item in

      Also, we give the image 1 second to load.
      If it doesn't load in 1 second, a placeholder is rendered
      Then we start fading in the entire collection (or new items)
      ###
      image = new Image()
      image.addEventListener('load', @_replaceImage)
      image.addEventListener('error', =>
        $(@el).css('visibility', 'visible').hide().fadeIn(600)
        @triggerMethod('onShowChild') # Sends a message to the collection view that we are about to render some items
        return
      )
      setTimeout( =>
        image.removeEventListener('load', @_replaceImage)
        $(@el).css('visibility', 'visible').hide().fadeIn(600)
        @triggerMethod('onShowChild')
        return
      , 1000)
      image.src = @model.get('ogImage').url
      return

    _replaceImage: =>
      $(@el).find('.og-placeholder').replaceWith('<img src="' + @model.get('ogImage').url + '" />')
      @triggerMethod('onShowChild')
      return

    _shareHeadline: (e) ->
      e.preventDefault()
      e.stopPropagation()
      id = @model.get('id')
      App.appRouter.navigate('/')
      require ['cs!coffee/views/feed/FeedPage'], (FeedPage) =>
        App.layout.getRegion('appRegion').currentView.showPage(new FeedPage(
          open_graph_data: @model.attributes
        ))
        return
      return
