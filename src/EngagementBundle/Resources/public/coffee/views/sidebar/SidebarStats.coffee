define (require) ->

  _ = require 'underscore'
  # $ = require 'jquery'
  Marionette = require 'marionette'
  Backbone = require 'backbone'
  Handlebars = require 'handlebars'
  App = require 'cs!coffee/App'
  sidebarStats = require 'text!templates/sidebar/sidebarStats.html'

  class SidebarStats extends Marionette.LayoutView

    template: Handlebars.compile sidebarStats

    events:
      'click [data-event="following"]': '_showFollowing'
      'click [data-event="followers"]': '_showFollowers'
      'click [data-event="groupsfollowing"]': '_showGroupsFollowing'

    templateHelpers: ->
      helpers:
        followingCount: do =>
          @options.following.state.totalRecords
        followersCount: do =>
          @options.followers.state.totalRecords
        groupsCount: do =>
          @options.groups.state.totalRecords

    initialize: ->
      @listenTo @options.following, 'count', ->
        @render()
      , @
      @listenTo @options.followers, 'count', ->
        @render()
      , @
      @listenTo @options.groups, 'count', ->
        @render()
      , @

    _showFollowing: (e) -> #TODO Check the route and don't reload user page unless necessary
      e.preventDefault()
      App.appRouter.navigate(@model.get('id') + '/following')
      require ['cs!coffee/views/user/UserPage', 'views/following/FollowingPage'], (UserPage, FollowingPage) =>
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: @model.get('id')
          route: 'following'
          module: new FollowingPage( userId: @model.get('id') )
        ))
        return
      return

    _showFollowers: (e) ->
      e.preventDefault()
      App.appRouter.navigate(@model.get('id') + '/followers')
      require ['cs!coffee/views/user/UserPage', 'views/followers/FollowersPage'], (UserPage, FollowersPage) =>
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: @model.get('id')
          route: 'followers'
          module: new FollowersPage( userId: @model.get('id') )
        ))
        return
      return

    _showGroupsFollowing: (e) ->
      e.preventDefault()
      App.appRouter.navigate(@model.get('id') + '/groups')
      require ['cs!coffee/views/user/UserPage', 'views/groups/GroupsFollowingPage'], (UserPage, GroupsFollowingPage) =>
        App.layout.getRegion('appRegion').currentView.showPage(new UserPage(
          userId: @model.get('id')
          route: 'groupsFollowing'
          module: new GroupsFollowingPage( userId: @model.get('id') )
        ))
        return
      return
