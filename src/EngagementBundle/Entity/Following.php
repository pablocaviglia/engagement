<?php

namespace EngagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Following
 *
 * @ORM\Table(name="following")
 * @ORM\Entity(repositoryClass="EngagementBundle\Repository\FollowingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Following
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="followings")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="followers")
     * @ORM\JoinColumn(name="following_user_id", referencedColumnName="id")
     */
    protected $followingUser;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Following
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param \EngagementBundle\Entity\User $user
     *
     * @return Following
     */
    public function setUser(\EngagementBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \EngagementBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set followingUser
     *
     * @param \EngagementBundle\Entity\User $followingUser
     *
     * @return Following
     */
    public function setFollowingUser(\EngagementBundle\Entity\User $followingUser = null)
    {
        $this->followingUser = $followingUser;

        return $this;
    }

    /**
     * Get followingUser
     *
     * @return \EngagementBundle\Entity\User
     */
    public function getFollowingUser()
    {
        return $this->followingUser;
    }
}
