<?php


namespace EngagementBundle\Entity\Analytic;

use Doctrine\ORM\Query\In;
use Doctrine\ORM\Mapping as ORM;

abstract class AbstractAnalytic
{

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=true)
     */
    protected $createdAt;

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }
}