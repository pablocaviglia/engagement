<?php


namespace EngagementBundle\Entity\Analytic;

use Doctrine\ORM\Query\In;
use Doctrine\ORM\Mapping as ORM;
use EngagementBundle\Entity\User;

/**
 * ContentGroup
 *
 * @ORM\Table(
 *      name="analytic_rest",
 *      indexes={
 *          @ORM\Index(name="platform_idx", columns={"platform"}),
 *          @ORM\Index(name="user_id_idx", columns={"user_id"}),
 *          @ORM\Index(name="error_code_idx", columns={"error_code"}),
 *          @ORM\Index(name="browser_type_idx", columns={"browser_type"})
 *      }
 * )
 * @ORM\Entity(repositoryClass="EngagementBundle\Repository\AnalyticRESTRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class RESTAnalytic extends AbstractAnalytic
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="platform", type="string", length=255, nullable=true)
     */
    private $platform;

    /**
     * @var string
     *
     * @ORM\Column(name="device_model", type="string", length=255, nullable=true)
     */
    private $deviceModel;

    /**
     * @var boolean
     *
     * @ORM\Column(name="device_debug_mode", type="boolean", nullable=true)
     */
    private $deviceDebugMode;

    /**
     * @var string
     *
     * @ORM\Column(name="browser_type", type="string", length=255, nullable=true)
     */
    private $browserType;

    /**
     * @var float
     *
     * @ORM\Column(name="execution_time", type="float", nullable=true)
     */
    private $executionTime;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     */
    private $userId;

    /**
     * @var int
     *
     * @ORM\Column(name="http_error_code", type="integer", nullable=true)
     */
    private $httpErrorCode;

    /**
     * @var int
     *
     * @ORM\Column(name="error_code", type="integer", nullable=true)
     */
    private $errorCode;

    /**
     * @var bool
     *
     * @ORM\Column(name="error_managed", type="boolean", nullable=true)
     */
    private $errorManaged;

    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=1024, nullable=true)
     */
    private $route;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param string $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    /**
     * @return string
     */
    public function getDeviceModel()
    {
        return $this->deviceModel;
    }

    /**
     * @param string $deviceModel
     */
    public function setDeviceModel($deviceModel)
    {
        $this->deviceModel = $deviceModel;
    }

    /**
     * @return boolean
     */
    public function isDeviceDebugMode()
    {
        return $this->deviceDebugMode;
    }

    /**
     * @param boolean $deviceDebugMode
     */
    public function setDeviceDebugMode($deviceDebugMode)
    {
        $this->deviceDebugMode = $deviceDebugMode;
    }

    /**
     * @return string
     */
    public function getBrowserType()
    {
        return $this->browserType;
    }

    /**
     * @param string $browserType
     */
    public function setBrowserType($browserType)
    {
        $this->browserType = $browserType;
    }

    /**
     * @return int
     */
    public function getExecutionTime()
    {
        return $this->executionTime;
    }

    /**
     * @param int $executionTime
     */
    public function setExecutionTime($executionTime)
    {
        $this->executionTime = $executionTime;
    }

    /**
     * @return int
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     */
    public function setUserId($userId)
    {
        $this->userId = $userId;
    }

    /**
     * @return int
     */
    public function getErrorCode()
    {
        return $this->errorCode;
    }

    /**
     * @param int $errorCode
     */
    public function setErrorCode($errorCode)
    {
        $this->errorCode = $errorCode;
    }

    /**
     * @return boolean
     */
    public function isErrorManaged()
    {
        return $this->errorManaged;
    }

    /**
     * @param boolean $errorManaged
     */
    public function setErrorManaged($errorManaged)
    {
        $this->errorManaged = $errorManaged;
    }

    /**
     * @return string
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param string $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return int
     */
    public function getHttpErrorCode()
    {
        return $this->httpErrorCode;
    }

    /**
     * @param int $httpErrorCode
     */
    public function setHttpErrorCode($httpErrorCode)
    {
        $this->httpErrorCode = $httpErrorCode;
    }
}