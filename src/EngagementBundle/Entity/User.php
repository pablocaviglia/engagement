<?php

namespace EngagementBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="EngagementBundle\Repository\UserRepository")
 */
class User
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first", type="string", length=255)
     */
    private $first;

    /**
     * @var string
     *
     * @ORM\Column(name="last", type="string", length=255)
     */
    private $last;

    /**
     * @var string
     *
     * @ORM\Column(name="photo_url", type="string", length=255, nullable=true)
     */
    private $photoUrl;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user")
     */
    protected $posts;

    /**
     * @ORM\OneToMany(targetEntity="Following", mappedBy="user")
     */
    protected $followings;

    /**
     * @ORM\OneToMany(targetEntity="Following", mappedBy="followingUser")
     */
    protected $followers;

    /**
     * @ORM\OneToMany(targetEntity="ContentGroupFollowing", mappedBy="followingContentGroup")
     */
    protected $contentGroups;

    /**
     * @ORM\OneToMany(targetEntity="Impression", mappedBy="user")
     */
    protected $impressions;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set first
     *
     * @param string $first
     *
     * @return User
     */
    public function setFirst($first)
    {
        $this->first = $first;

        return $this;
    }

    /**
     * Get first
     *
     * @return string
     */
    public function getFirst()
    {
        return $this->first;
    }

    /**
     * Set last
     *
     * @param string $last
     *
     * @return User
     */
    public function setLast($last)
    {
        $this->last = $last;

        return $this;
    }

    /**
     * Get last
     *
     * @return string
     */
    public function getLast()
    {
        return $this->last;
    }

    /**
     * Set photoUrl
     *
     * @param string $photoUrl
     *
     * @return User
     */
    public function setPhotoUrl($photoUrl)
    {
        $this->photoUrl = $photoUrl;
        return $this;
    }

    /**
     * Get photoUrl
     *
     * @return string
     */
    public function getPhotoUrl()
    {
        return $this->photoUrl;
    }

    public function __construct()
    {
        $this->posts = new ArrayCollection();
        $this->followings = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->impressions = new ArrayCollection();
    }

    /**
     * Add post
     *
     * @param \EngagementBundle\Entity\Post $post
     *
     * @return User
     */
    public function addPost(\EngagementBundle\Entity\Post $post)
    {
        $this->posts[] = $post;

        return $this;
    }

    /**
     * Remove post
     *
     * @param \EngagementBundle\Entity\Post $post
     */
    public function removePost(\EngagementBundle\Entity\Post $post)
    {
        $this->posts->removeElement($post);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    public function getFullName()
    {
        return $this->first . " " . $this->last;
    }

    /**
     * Add following
     *
     * @param \EngagementBundle\Entity\Following $following
     *
     * @return User
     */
    public function addFollowing(\EngagementBundle\Entity\Following $following)
    {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param \EngagementBundle\Entity\Following $following
     */
    public function removeFollowing(\EngagementBundle\Entity\Following $following)
    {
        $this->followings->removeElement($following);
    }

    /**
     * Get followings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowings()
    {
        return $this->followings;
    }

    /**
     * Get Array of following user ids for a user
     * @return array
     */
    public function getFollowingUserIds()
    {
        $array_of_followings = array();
        $followings = $this->getFollowings();

        foreach ($followings as $following)
        {
            $array_of_followings[] = $following->getFollowingUser()->getId();
        }

        // Add in the user as well
        $array_of_followings[] = $this->getId();

        return $array_of_followings;
    }

    /**
     * Add follower
     *
     * @param \EngagementBundle\Entity\Following $follower
     *
     * @return User
     */
    public function addFollower(\EngagementBundle\Entity\Following $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param \EngagementBundle\Entity\Following $follower
     */
    public function removeFollower(\EngagementBundle\Entity\Following $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @return mixed
     */
    public function getContentGroups()
    {
        return $this->contentGroups;
    }

    /**
     * @param mixed $contentGroups
     */
    public function setContentGroups($contentGroups)
    {
        $this->contentGroups = $contentGroups;
    }

    /**
     * Add contentGroup
     *
     * @param \EngagementBundle\Entity\ContentGroupFollowing $contentGroup
     *
     * @return User
     */
    public function addContentGroup(\EngagementBundle\Entity\ContentGroupFollowing $contentGroup)
    {
        $this->contentGroups[] = $contentGroup;

        return $this;
    }

    /**
     * Remove contentGroup
     *
     * @param \EngagementBundle\Entity\ContentGroupFollowing $contentGroup
     */
    public function removeContentGroup(\EngagementBundle\Entity\ContentGroupFollowing $contentGroup)
    {
        $this->contentGroups->removeElement($contentGroup);
    }

    /**
     * Add impression
     *
     * @param \EngagementBundle\Entity\Impression $impression
     *
     * @return User
     */
    public function addImpression(\EngagementBundle\Entity\Impression $impression)
    {
        $this->impressions[] = $impression;

        return $this;
    }

    /**
     * Remove impression
     *
     * @param \EngagementBundle\Entity\Impression $impression
     */
    public function removeImpression(\EngagementBundle\Entity\Impression $impression)
    {
        $this->impressions->removeElement($impression);
    }

    /**
     * Get impressions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImpressions()
    {
        return $this->impressions;
    }
}
