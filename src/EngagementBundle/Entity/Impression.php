<?php

namespace EngagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Impression
 *
 * @ORM\Table(name="impression")
 * @ORM\Entity(repositoryClass="EngagementBundle\Repository\ImpressionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Impression
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="Post", inversedBy="impressions")
     * @ORM\JoinColumn(name="post_id", referencedColumnName="id")
     */
    private $post;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="impressions")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Impression
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set post
     *
     * @param \EngagementBundle\Entity\Post $post
     *
     * @return Impression
     */
    public function setPost(\EngagementBundle\Entity\Post $post = null)
    {
        $this->post = $post;

        return $this;
    }

    /**
     * Get post
     *
     * @return \EngagementBundle\Entity\Post
     */
    public function getPost()
    {
        return $this->post;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
    }

    /**
     * Set user
     *
     * @param \EngagementBundle\Entity\User $user
     *
     * @return Impression
     */
    public function setUser(\EngagementBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \EngagementBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
