<?php

namespace EngagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Post
 *
 * @ORM\Table(name="post")
 * @ORM\Entity(repositoryClass="EngagementBundle\Repository\PostRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="open_graph_data", type="text", nullable=true)
     */
    private $openGraphData;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="ContentGroup", inversedBy="posts")
     * @ORM\JoinColumn(name="contentgroup_id", referencedColumnName="id")
     */
    protected $contentGroup;

    /**
     * @ORM\OneToMany(targetEntity="Impression", mappedBy="post")
     */
    protected $impressions;

    public function __construct()
    {
        $this->impressions = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Post
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Post
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Post
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Post
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \EngagementBundle\Entity\User $user
     *
     * @return Post
     */
    public function setUser(\EngagementBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \EngagementBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getPostAsJson()
    {
        $post_as_json = array(
            "id" => $this->getId(),
            "title" => $this->getTitle(),
            "content" => $this->getContent(),
            "created_at" => $this->getCreatedAt(),
            "updated_at" => $this->getUpdatedAt(),
            "author" => $this->getUser()->getFullName(),
            "author_id" => $this->getUser()->getId()
        );

        return $post_as_json;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedAtValue()
    {
        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();
    }

    /**
     * Set contentGroup
     *
     * @param \EngagementBundle\Entity\ContentGroup $contentGroup
     *
     * @return Post
     */
    public function setContentGroup(\EngagementBundle\Entity\ContentGroup $contentGroup = null)
    {
        $this->contentGroup = $contentGroup;

        return $this;
    }

    /**
     * Get contentGroup
     *
     * @return \EngagementBundle\Entity\ContentGroup
     */
    public function getContentGroup()
    {
        return $this->contentGroup;
    }

    /**
     * Set openGraphData
     *
     * @param string $openGraphData
     *
     * @return Post
     */
    public function setOpenGraphData($openGraphData)
    {
        $this->openGraphData = $openGraphData;

        return $this;
    }

    /**
     * Get openGraphData
     *
     * @return string
     */
    public function getOpenGraphData()
    {
        return $this->openGraphData;
    }

    /**
     * Add impression
     *
     * @param \EngagementBundle\Entity\Impression $impression
     *
     * @return Post
     */
    public function addImpression(\EngagementBundle\Entity\Impression $impression)
    {
        $this->impressions[] = $impression;

        return $this;
    }

    /**
     * Remove impression
     *
     * @param \EngagementBundle\Entity\Impression $impression
     */
    public function removeImpression(\EngagementBundle\Entity\Impression $impression)
    {
        $this->impressions->removeElement($impression);
    }

    /**
     * Get impressions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getImpressions()
    {
        return $this->impressions;
    }
}
