<?php

namespace EngagementBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ContentGroupFollowing
 *
 * @ORM\Table(name="content_group_following")
 * @ORM\Entity(repositoryClass="EngagementBundle\Repository\ContentGroupFollowingRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class ContentGroupFollowing
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\ManyToOne(targetEntity="ContentGroup")
     * @ORM\JoinColumn(name="content_group_id", referencedColumnName="id")
     */
    protected $followingContentGroup;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getFollowingContentGroup()
    {
        return $this->followingContentGroup;
    }

    /**
     * @param mixed $followingContentGroup
     */
    public function setFollowingContentGroup($followingContentGroup)
    {
        $this->followingContentGroup = $followingContentGroup;
    }
}
